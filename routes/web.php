<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Route::get('fbchatbot', 'Fbbot@verify');
Route::post('fbchatbot', 'Fbbot@message');

Route::any('msgtest', 'Fbbot@msgtest');

//Route::any('fbchatbot', 'Fbbot@index');
Route::get('twchatbot', 'Twbot@verify');
Route::post('twchatbot', 'Twbot@message');

Route::get('twchatbot/reg_webhook', 'Twbot@reg_webhook');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*======Chat controller routing======*/

Route::get('/', 'ChatController@start_chat');
Route::get('/chat/get_persona', 'ChatController@get_persona');
Route::get('/chat/get_url', 'ChatController@get_url');
Route::get('/chat/{industry}/{persona}', 'ChatController@index');
Route::get('/chat/{industry}/{brand}/{persona}', 'ChatController@index');
Route::get('/chat/{industry}/{brand}/{compaign}/{persona}/{test}', 'ChatController@chatBackup');
Route::get('/persona/chat/user_chat', 'ChatController@userChat');
Route::get('/persona/user_chat_detail/{ip}/{email}', 'ChatController@getUserChatDetail');
Route::any('/persona/user_chat_pdf/{personaId}/{ip}', 'ChatController@userChatPdf');
Route::get('/persona/user_chat_detail/{ip}', 'ChatController@getUserChatDetail');
Route::get('/backoffice/persona/check_user_ip', 'ChatController@checkUserIp');
Route::get('/backoffice/persona/update_previous_chat_count', 'ChatController@updatePreviousChatCount');
Route::get('/backoffice/persona/delete_previous_chat', 'ChatController@deletePreviousChat');
Route::get('/backoffice/persona/get_persona_channel', 'PersonaController@getPersonaChannel');

//Route::get('backoffice/', 'DashboardController@index')->middleware('admin');

Route::group(['middleware' => ['admin']], function () {
    Route::get('backoffice/', 'DashboardController@admin');
    Route::get('backoffice/persona', 'PersonaController@index');
    Route::get('backoffice/persona/add_persona', 'PersonaController@add_persona');
    Route::post('backoffice/persona/add_persona', 'PersonaController@save');
    Route::get('backoffice/persona/edit/{id}', 'PersonaController@edit');
    Route::post('backoffice/persona/edit/{id}', 'PersonaController@update');
    Route::delete('backoffice/persona/{id}', 'PersonaController@destroy');
    Route::get('backoffice/persona/industries', 'PersonaController@industries');
    Route::post('backoffice/persona/add_industry', 'PersonaController@save_industry');
    Route::get('backoffice/persona/industries_edit/{id}', 'PersonaController@edit_industries');
    Route::post('backoffice/persona/industries_edit/', 'PersonaController@update_industries');
    Route::delete('backoffice/persona/delete_ind/{id}', 'PersonaController@delete_ind');
    Route::get('backoffice/persona/content/{id}', 'PersonaController@content');
    Route::post('backoffice/persona/save_content/{id}', 'PersonaController@save_content');
    Route::delete('backoffice/persona/delete_cont/{id}', 'PersonaController@delete_cont');
    Route::get('backoffice/users', 'UserController@index');
    Route::get('backoffice/businesslogic/{id}', 'BusinessLogicController@index');
    Route::post('backoffice/businesslogic/save_logic/{id}', 'BusinessLogicController@save_logic');
    Route::get('backoffice/persona/content_configure', 'PersonaController@content_configure');
    Route::get('backoffice/persona/get_configure_content', 'PersonaController@get_configure_content');
    Route::post('backoffice/persona/save_content_configuration', 'PersonaController@save_content_configuration');
    Route::get('backoffice/persona/create_intent_test', 'PersonaController@createIntentTest');
    Route::get('backoffice/persona/detect_intent_texts', 'PersonaController@detect_intent_texts');
    
    Route::post('backoffice/persona/add_form_data', 'FormController@index');
    Route::post('backoffice/persona/delete_func', 'PersonaController@commonDelete');
    Route::get('backoffice/persona/edit_persona_content', 'PersonaController@editPersonaContent');
    Route::get('backoffice/persona/add-duplicate-persona', 'PersonaController@addDuplicatePersona');
    Route::get('backoffice/persona/get_persona_score', 'PersonaController@getPersonaScore');
    Route::post('backoffice/persona/persona_assign_channel', 'PersonaController@PersonaAssignChannel');
    Route::get('backoffice/persona/get_frontend_url', 'PersonaController@getFrontendUrl');
});

Route::get('backoffice/persona/get_chat_response', 'PersonaController@get_chat_response');
Route::get('backoffice/persona/get_chat_backup_response', 'PersonaController@get_chat_backup_response');
Route::get('backoffice/persona/try_chat_response', 'PersonaController@try_chat_response');
Route::get('backoffice/persona/try_chat_backup_response', 'PersonaController@try_chat_backup_response');
Route::get('backoffice/persona/register_chat_user', 'PersonaController@register_chat_user');

Route::post('backoffice/persona/upload_file', 'PersonaController@upload_file');

Route::post('backoffice/persona/chat_backup', 'PersonaController@chat_backup');

/*=====Brand routing====*/

Route::get('backoffice/persona/brands', 'BrandController@index');
Route::post('backoffice/persona/add_brand', 'BrandController@addBrand');
Route::delete('backoffice/persona/delete_brand/{id}', 'BrandController@deleteBrand');
Route::get('backoffice/persona/edit_brand/{id}', 'BrandController@editBrand');
Route::post('backoffice/persona/update_brand', 'BrandController@updateBrand');
Route::post('backoffice/persona/add_compaign', 'BrandController@addCompaign');
Route::post('backoffice/persona/update_compaign', 'BrandController@updateCompaign');
Route::get('backoffice/persona/get_brand_compaign', 'BrandController@getBrandCompaign');
Route::get('backoffice/persona/get_brands_compaign', 'BrandController@getBrandsCompaign');
Route::get('backoffice/persona/brand_compaign/{id}', 'BrandController@brandCompaign');


/*====Manage Admin routing======*/

Route::get('backoffice/persona/manage-admin', 'UserController@index');
Route::post('backoffice/persona/update-profile', 'UserController@updateProfile');
Route::get('backoffice/persona/check_current_password', 'UserController@checkCurrentPwd');

/*======Login routing==========*/

Route::get('login', 'UserController@userLogin');
Route::post('login', 'UserController@userLogin');

/*======Insights routing=====*/

Route::get('persona/persona_chat/{chatId}', 'InsightController@userPersonaChatDetail');
Route::get('backoffice/persona/insights', 'InsightController@index');
Route::get('backoffice/insights/get_brand_list', 'InsightController@getBrandList');
Route::get('backoffice/persona/get_monthly_graph', 'InsightController@getMonthlyGraph');
Route::get('backoffice/persona/get_monthly_par_graph', 'InsightController@getMonthlyParGraph');
Route::get('backoffice/insights/comapign_form', 'InsightController@compaignForm');
Route::get('backoffice/insights/compaign_overview/{id}', 'InsightController@compaignOverview');
Route::get('backoffice/insights/compaign_overview/{id}/{type}', 'InsightController@compaignOverview');
Route::get('backoffice/insights/compaign_participants/{id}', 'InsightController@compaignParticipants');
Route::get('backoffice/insights/compaign_participation/{id}', 'InsightController@compaignParticipation');
Route::get('backoffice/insights/compaign_participation/{id}/{type}', 'InsightController@compaignParticipation');
Route::get('backoffice/insights/get_persona_detail', 'InsightController@getPersonaDetail');
Route::get('backoffice/insights/insight_comparison', 'InsightController@insightComparison');
Route::get('backoffice/insights/get_persona_title', 'InsightController@getPersonaTitle');
Route::get('backoffice/insights/personas_detail', 'InsightController@personasDetail');
Route::post('backoffice/insights/compaign_overview', 'InsightController@getCompaignOverview');
Route::post('backoffice/insights/insight_comparison_list', 'InsightController@getInsightComparisonList');
Route::get('backoffice/insights/insight_comparison/{brand}/{persona1}/{persona2}', 'InsightController@insightComparison');
Route::get('backoffice/insights/insight_comparison/{brand}/{persona1}/{persona2}/{persona3}', 'InsightController@insightComparison');
Route::get('backoffice/insights/insight_comparison/{brand}/{persona1}/{persona2}/{persona3}/{persona4}', 'InsightController@insightComparison');
