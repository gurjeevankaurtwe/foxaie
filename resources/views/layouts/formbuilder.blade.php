<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Admin - FoxAI</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" href="{{asset('admin/img/placeholders/avatars/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/themes.css')}}">
	<script src="{{asset('js/app.js')}}" defer></script>
    <script src="{{asset('admin/js/vendor/modernizr-respond.min.js')}}"></script>
	<script>
	window.Laravel =  <?php echo json_encode([
		'csrfToken' => csrf_token(),
	]); ?>
	</script>
  @stack('styles')
</head>
<body style="background-color:#eaedf1;">
	<div id="app">
		<main class="py-4">
            @yield('content')
        </main>
	</div>
@stack('scripts')
@yield('custom-script')
</body>
</html>