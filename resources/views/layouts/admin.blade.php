<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Admin - Foxaie</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" href="{{asset('chat-img/logo/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{asset('admin/css/plugins.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css">
    <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/themes.css')}}">
	<script src="{{asset('admin/js/vendor/modernizr-respond.min.js')}}"></script>
	<style>
		#form_paciente{
    width: 100% !important;
}
	</style>
	<script>
	window.Laravel =  <?php echo json_encode([
		'csrfToken' => csrf_token(),
	]); ?>
	
	</script>
  @stack('styles')
</head>
<body>
    <div id="page-wrapper">
		<div class="preloader themed-background">
			<h1 class="push-top-bottom text-light text-center"><strong>Foxaie</strong></h1>
			<div class="inner">
				<h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
				<div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
			</div>
		</div>
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
		<div id="sidebar">
			<div id="sidebar-scroll">
				<div class="sidebar-content">
				  <a href="index.php" class="sidebar-brand">
					<!--<span class="sidebar-nav-mini-hide"><strong>Foxaie</strong></span>-->
					<img src="{{asset('/admin/img/FoxAie-logo.png')}}" alt="logo" style="width:70px;">
				  </a>
				  <p class="menu_label">Menu</p>
				  <!--<div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
					<div class="sidebar-user-avatar">
					  <a href="index.php">
						<img src="{{asset('uploads/user-images/'.Auth::user()->profile_image)}}" alt="userimg">
					  </a>
					</div>
					<div class="sidebar-user-name"></div>
					<p>{{ Auth::user()->name }}</p>
				  	</div>-->
				  	<ul class="sidebar-nav">
						<li>
						  <a href="{{url('backoffice')}}" class="{{ request()->is('backoffice') ? 'active' : '' }}"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
						</li>
						<li>
					  		<a href="javascript:void(0)" class="sidebar-nav-menu @if (strpos($_SERVER['REQUEST_URI'], 'persona') !== false) open @endif">
					  			<i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
					  			<i class="fa fa-lock sidebar-nav-icon"></i>
					  			<span class="sidebar-nav-mini-hide">Configuration</span>
					  		</a>
						  	<ul @if(strpos($_SERVER['REQUEST_URI'], 'persona') !== false) style=display:block  @endif >
								<li>
								  <a class="{{ request()->is('backoffice/persona') ? 'active' : '' }}" href="{{url('backoffice/persona')}}">Personas</a>
								</li>
								<li>
								  <a class="{{ request()->is('backoffice/persona/industries') ? 'active' : '' }}" href="{{url('backoffice/persona/industries')}}">Manage Industries</a>
								</li>
								<li>
								  <a class="{{ request()->is('backoffice/persona/brands') ? 'active' : '' }}" href="{{url('backoffice/persona/brands')}}">Manage Brands</a>
								</li>
								<li>
								  <a class="{{ request()->is('backoffice/persona/user_chat') ? 'active' : '' }}" href="{{url('/persona/chat/user_chat')}}">User Chats</a>
								</li>
								<li>
								  <a class="{{ request()->is('backoffice/persona/manage-admin') ? 'active' : '' }}" href="{{url('backoffice/persona/manage-admin')}}">Manage Admin</a>
								</li>
						  	</ul>
						</li>
						<li>
						  	<a href="{{url('backoffice/persona/insights')}}" class="{{ request()->is('backoffice/persona/insights') ? 'active' : '' }}">
						  		<i class="gi gi-stopwatch sidebar-nav-icon"></i>
						  		<span class="sidebar-nav-mini-hide">Insights</span>
						  	</a>
					  		
						</li>
				  	</ul>
				</div>
			</div>
		</div>
		<div id="main-container">
			<header class="navbar navbar-default">
			  <ul class="nav navbar-nav-custom">
				<li>
				  <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
				  <i class="fa fa-bars fa-fw"></i>
				  </a>
				</li>
			  </ul>
			  <ul class="nav navbar-nav-custom pull-right">
				<li class="dropdown">
				  <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
				  <img src="{{asset('uploads/user-images/'.Auth::user()->profile_image)}}" alt="avatar"> <i class="fa fa-angle-down"></i>
				  </a>
				  <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
					<li class="dropdown-header text-center">Account</li>
					<li class="divider"></li>
					<li>
					  <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
					  <i class="fa fa-sign-out fa-fw pull-right"></i>
					  Logout
					  </a>
					  	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
					  <a href="#modal-user-settings" data-toggle="modal">
					  <i class="fa fa-cog fa-fw pull-right"></i>
					  Settings
					  </a>
					</li>
					<li class="divider"></li>
				  </ul>
				</li>
			  </ul>
			</header>
			<div id="page-content">
				
				<div class="clearfix"></div>
				

			 @yield('content')
			 
			</div>
			<footer class="clearfix">
			  <div class="pull-left">
				2019 - 2020 &copy; <a href="javascript:void();" target="_blank">Foxaie</a>
			  </div>
			</footer>
		</div>
	</div>
</div>
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<!-- Modal -->
<div id="configure-content" class="modal fade in" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Configure</h4>
		</div>
		<div class="modal-body">
			test sds
			kjdff
		</div>
    </div>

  </div>
</div>

<!---- Delete Popup ---->

<div class="modal fade in" id="deleteModal" role="dialog">
    <div class="modal-dialog">
    	<div class="modal-content">
	        <div class="modal-header">
	          <h4 class="delete-modal-title"></h4>
	        </div>
	        <form method="post" action="{{url('/backoffice/persona/delete_func')}}">
		        <div class="modal-body">
		          	<p class="delete-modal-text"></p>
		          	@csrf
		          	<input type="hidden" name="id" class="delete-id">
		          	<input type="hidden" name="deleteStatus" class="delete-status">
		          	<input type="hidden" name="table_name" class="table-name">
		        </div>
		        <div class="modal-footer">
		          <button class="btn btn-primary" type="submit">Yes</button>
		          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		        </div>
		    </form>
      	</div>
    </div>
</div>

<!---- Assign Channel Modal ---->

<div class="modal fade" id="assign_channel_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Assign Channel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      	<form method="POST" action="{{url('backoffice/persona/persona_assign_channel')}}">
      		@csrf
	      	<div class="modal-body">
	        	<input type="hidden" class="channelPerId" name="persona_id">
				<div class="form-group">
		            <select class="form-control selectedChannels" name="channels[]" multiple>
		            	
		            </select>
	          	</div>
		   	</div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Submit</button>
	      </div>
	    </form>
    </div>
  </div>
</div>




<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>
<script src="{{asset('admin/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/plugins.js')}}"></script>
<script src="{{asset('admin/js/app.js')}}"></script>
<script src="//maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{asset('admin/js/helpers/gmaps.min.js')}}"></script>
<!-- <script src="{{asset('admin/js/pages/tablesDatatables.js')}}"></script>
<script>$(function(){ TablesDatatables.init(); });</script> -->
<script src="http://malsup.github.com/jquery.form.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
<script src="{{asset('admin/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('admin/js/helpers/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('admin/js/pages/index.js')}}"></script>
<script>$(function(){ Index.init(); });</script> 

@stack('scripts')
@yield('custom-script')

<script>

	

	jQuery(document).on('click','.show-frontend-url',function(){
		jQuery('#frontend_url_modal').modal('show');
		var personaId = jQuery(this).attr('data-attr');
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_frontend_url')}}",
			data: { personaId:personaId},
			dataType:"json",
			success:function(data) 
			{
				var html = '';
				var html1 = '';
				var html2 = '';
				var html3 = '';
				var html4 = '';
				if(data.facebook_url)
				{
					var html = '<h4>Facebook Url : <a href="'+data.facebook_url+'" target="_blank"><input type="button" class="btn btn-primary" value="Facebook Url"></a></h4>';
				}
				if(data.twitter_url)
				{
					var html1 = '<h4>Twitter Url : <a href="'+data.twitter_url+'" target="_blank"><input type="button" class="btn btn-primary"  value="Twitter Url"></a></h4>';
				}
				if(data.linkdin_url)
				{
					var html2 = '<h4>Linkdin Url : <a href="'+data.linkdin_url+'" target="_blank"><input type="button" class="btn btn-primary"  value="Linkdin Url"></a></h4>';
				}
				if(data.youtube_url)
				{
					var html3 = '<h4>Youtube Url : <a href="'+data.youtube_url+'" target="_blank"><input type="button" class="btn btn-primary"  value="Youtube Url"></a></h4>';
				}
				var html4 = '<h4>Website Url : <a href="'+data.url+'/website" target="_blank"><input type="button" class="btn btn-primary"  value="Website Url"></a></h4>';

				jQuery('.url-modal-body').html(html+' '+html1+' '+html2+' '+html3+' '+html4);
				
			}
		});
	});

	jQuery(document).on('change','.graphType',function(){
		if(jQuery(this).val() == 'Bar')
		{
			jQuery('#monthly_bar_container').css('display','block');
			jQuery('#monthly_container').css('display','none');
		}
		if(jQuery(this).val() == 'Line')
		{
			jQuery('#monthly_container').css('display','block');
			jQuery('#monthly_bar_container').css('display','none');
		}
	});

/*=========Show assign channel to persona modal========*/

	jQuery(document).on('click','.assign-persona-channel',function(){
		jQuery('#assign_channel_modal').modal('show');
		var perId = jQuery(this).attr('data-attr');
		jQuery('.channelPerId').val(perId);
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_persona_channel')}}",
			data: { perId:perId},
			dataType:"html",
			success:function(data) 
			{
				
				jQuery('.selectedChannels').html(data);
			}
		});
	});

	$('.chosen-select').chosen({}).change( function(obj, result) {
    console.debug("changed: %o", arguments);
    
    console.log("selected: " + result.selected);
});
	
/*==========start get monthly Overview graph according to date==========*/

	$("#from_date").datepicker({
        maxDate: 0,
        onSelect: function (date) {
            var dt2 = $('#to_date');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            dt2.datepicker('setDate', minDate);
            startDate.setDate(startDate.getDate() + 30);
            //sets dt2 maxDate to the last day of 30 days window
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
            $(this).datepicker('option', 'minDate', minDate);
        }
    });
	$( "#to_date" ).datepicker({
		maxDate: 0,
	});

	jQuery(document).on('change','.getMonthlyGraph',function(){
		var startDate = jQuery('#from_date').val();
		var endDate = jQuery('#to_date').val();
		var personaId = jQuery('.personaId').val();
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_monthly_graph')}}",
			data: { startDate:startDate,endDate:endDate,personaId:personaId},
			dataType:"html",
			success:function(data) 
			{
				jQuery('.monthly_graph').html(data);
				
			}
		});
	})


/*==end get monthly graph according to date==*/


/*==========start get monthly Participation graph according to date==========*/

	$("#par_from_date").datepicker({
        maxDate: 0,
        onSelect: function (date) {
            var dt2 = $('#par_to_date');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            dt2.datepicker('setDate', minDate);
            startDate.setDate(startDate.getDate() + 30);
            //sets dt2 maxDate to the last day of 30 days window
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
            $(this).datepicker('option', 'minDate', minDate);
        }
    });
	$( "#par_to_date" ).datepicker({
		maxDate: 0,
	});

	jQuery(document).on('change','.getMonthlyParGraph',function(){
		var startDate = jQuery('#par_from_date').val();
		var endDate = jQuery('#par_to_date').val();
		var personaId = jQuery('.personaId').val();
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_monthly_par_graph')}}",
			data: { startDate:startDate,endDate:endDate,personaId:personaId},
			dataType:"html",
			success:function(data) 
			{
				jQuery('.monthly_graph').html(data);
				
			}
		});
	})


/*==end get monthly graph according to date==*/



	var APP_URL = {!! json_encode(url('/')) !!}
	
	$(document).ready(function() {
	    $('#example').DataTable( {
	    }  );
	} );


	function readURL(input) {
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    
	    reader.onload = function(e) {
	      $('.check-image').val('');
	      $('#blah').attr('src', e.target.result);
	    }
	    
	    reader.readAsDataURL(input.files[0]);
	  }
	}

	$(".upload-persona-image").change(function() {
	  readURL(this);
	});
	function profileImage(input) {

	    if (input.files && input.files[0]) {
	      var reader = new FileReader();
	      
	      reader.onload = function(e) {
	        $('.check-profile-image').val('');
	        $('#show_profile_image_div').css('display', 'block');
	        $('#show_profile_image').attr('src', e.target.result);
	      }
	      
	      reader.readAsDataURL(input.files[0]);
	    }
  }

  	function bannerImage(input) {

	    if (input.files && input.files[0]) {
	      var reader = new FileReader();
	      
	      reader.onload = function(e) {
	        $('.check-banner-image').val('');
	        $('#show_banner_image_div').css('display', 'block');
	        $('#show_banner_image').attr('src', e.target.result);
	      }
	      
	      reader.readAsDataURL(input.files[0]);
	    }
  	}

  	$("#profile_image").change(function() {
    	profileImage(this);
  	});

  	$("#banner_image").change(function() {
	    bannerImage(this);
  	});

  jQuery(document).on('click','.edit-persona-content',function(){
  		var contentId = jQuery(this).attr('data-attr');
  		var personaChannel = jQuery(this).attr('data-target');
  		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/edit_persona_content')}}",
			data: { contentId:contentId,personaChannel:personaChannel},
			dataType:"json",
			success:function(data) 
			{
				var attachment = 'attachment';
				var quick_replies = 'quick_replies';
				var quick_reply = 'quick_reply';
				if(data.channel == 'Facebook')
				{
					if(attachment in data.content['message'])
					{
					    var type = 'File';
					    var response = data.content['message']['attachment']['payload']['url'];
					} 
					else if(quick_replies in data.content['message'])
					{
						var type =  'Radio';
						var response = data.content['message']['text'];
					}
					else
					{
						var type = 'Text';
						var response = data.content['message']['text'];
					}
				}
				else if(data.channel == 'Twitter')
				{
					if(quick_reply in data.content['event']['message_create']['message_data'])
					{
						var type =  'Radio';
						var response = data.content['event']['message_create']['message_data']['text'];
					}
					else
					{
						var type = 'Text';
						var response = data.content['event']['message_create']['message_data']['text'];
					}
				}
				else
				{
					var type = data.content.type;
					var response = data.content.query;
				}
				
				$("html, body").animate({ scrollTop: 0 }, "slow");
				jQuery('.edit-content-id').val(data.content_id);
				jQuery('#content_cust_id').val(data.content_cust_id);
				jQuery('#response-type').val(type);
				if(type == 'Text')
				{
					if(data.content.email)
					{
						var sel = 'checked';
					}
					else
					{
						var sel = '';
					}
					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" value="'+data.score+'"><label for="Query">Response:</label><input type="text" class="form-control" required name="query" value="'+response+'"><label for="Query">Email:</label><input type="checkbox" class="form-control" name="email" value="email" '+sel+'></div></div>');
					$('#content-btn-sub').css('display','block');
				}
				if(type == 'Button')
				{
					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" value="'+data.score+'"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer" value="'+data.content.answer+'"><label for="Query">Button Label:</label><input type="text" class="form-control" required name="query" value="'+response+'"></div></div>');
					$('#content-btn-sub').css('display','block');
				}
				if(type == 'Embed')
				{
					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" "'+data.content.score+'"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer" value="'+data.content.answer+'"><label for="Query">Button Label:</label><input type="text" class="form-control" required name="btn-label" value="'+data.content.btnlabel+'"><label for="Query">Response:</label><textarea id="editor1" class="form-control"  name="query">'+data.content.query+'</textarea></div></div>');
					$('#content-btn-sub').css('display','block');
					tinymce.init({
						selector: "#editor1",
						 plugins: "media",
						  menubar: "insert",
						  toolbar: "media"
					});
				}
				if(type == 'Textarea')
				{
					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" "'+data.content.score+'"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer" value="'+data.content.answer+'"><label for="Query">Response:</label><textarea id="editor2" class="form-control"  name="query">'+data.content.query+'</textarea></div></div>');
					$('#content-btn-sub').css('display','block');
					tinymce.init({
						selector: "#editor2",
						plugins: "link image",
						inline_styles: false,
					});
				}
				if(type == 'Radio')
				{
					var option = [];
					if(data.channel == 'Facebook')
					{
						$.each(data.content['message']['quick_replies'] , function( index, value ) 
						{
							option += '<input type="text" class="form-control" placeholder="Option" required name="ques[]" value="'+value['title']+'"><br>';
						});
					}
					else if(data.channel == 'Twitter')
					{
						$.each(data.content['event']['message_create']['message_data']['quick_reply']['options'] , function( index, value ) 
						{
							option += '<input type="text" class="form-control" placeholder="Option" required name="ques[]" value="'+value['label']+'"><br>';
						});
					}
					else
					{
						$.each(data.content.answer, function( index, value ) 
						{
							option += '<input type="text" class="form-control" placeholder="Option" required name="ques[]" value="'+value+'"><br>';
						});
					}
					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" value="'+data.score+'"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer" value="'+response+'"></div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Options:</label>'+option+'<a href="JavaScript:Void(0);" onclick="addmore();" id="add-radio-option">Add More</a></div></div>');
					$('#content-btn-sub').css('display','block');
				}
				if(type == 'Link')
				{
					if(data.content.answer)
					{
						answer = '<input type="text" class="form-control" required name="answer" value="'+data.content.answer+'">';
					}
					else
					{
						answer = '<input type="text" class="form-control" required name="answer">';
					}
					anw = '<label for="Query">Query:</label>'+answer;

					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" value="'+data.score+'">'+anw+'<label for="Query">Link:</label><input type="text" value="'+data.content.query+'" class="form-control" name="link-input"><br></div></div>');
					$('#content-btn-sub').css('display','block');
				}
				if(type == 'File')
				{
					anw = '';
					if(data.channel == 'Website')
					{
						if(data.content.answer)
						{
							answer = '<input type="text" class="form-control" required name="answer" value="'+data.content.answer+'">';
						}
						else
						{
							answer = '<input type="text" class="form-control" required name="answer">';
						}
						anw = '<label for="Query">Query:</label>'+answer+'<label for="Query">Button Label:</label><input type="text" class="form-control" required name="btn-label" value="'+data.content.btnlabel+'">';
					}
					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" value="'+data.score+'">'+anw+'</div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Select:</label><input type="file" class="form-control upload_persona_content_image" name="file-input"><br><input type="hidden" class="upload_persona_image" name="check_image" value="'+response+'"></div></div><div id="edit-image"><img id="contentImage" src="'+APP_URL+'/'+response+'"></div>');
					$('#content-btn-sub').css('display','block');
				}
				if(type == 'Docs')
				{
					anw = '';
					if(data.channel == 'Website')
					{
						if(data.content.answer)
						{
							answer = '<input type="text" class="form-control" required name="answer" value="'+data.content.answer+'">';
						}
						else
						{
							answer = '<input type="text" class="form-control" required name="answer">';
						}
						anw = '<label for="Query">Query:</label>'+answer+'<label for="Query">Button Label:</label><input type="text" class="form-control" required name="btn-label" value="'+data.content.btnlabel+'">';
					}
					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" value="'+data.score+'">'+anw+'</div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Select:</label><input type="file" class="form-control upload_persona_content_image" name="file-input"><br><input type="hidden" class="upload_persona_image" name="check_image" value="'+response+'"></div></div><div id="edit-docs">'+APP_URL+'/'+response+'</div>');
					$('#content-btn-sub').css('display','block');
				}
				if(type == 'Checkbox')
				{
					var checkbox = [];
					$.each(data.content.query, function( index, value ) 
					{
						checkbox += '<input type="text" class="form-control" placeholder="Option" required name="ques[]" value="'+value+'"><br>';
					});
					$('#content-field').children('div').remove();
					$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0" value="'+data.score+'"><label for="Answer">Query:</label><input type="text" class="form-control" required name="answer" value="'+data.content.answer+'"></div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Options:</label>'+checkbox+'<a href="JavaScript:Void(0);" onclick="addmore();" id="add-radio-option">Add More</a></div></div>');
					$('#content-btn-sub').css('display','block');
				}
				/*if(data.)
						
					
					case 'Checkbox':
						$('#content-field').children('div').remove();
						$('#content-field').append('<div class="row"><div class="form-group col-md-6"><label for="Answer">Query:</label><input type="text" class="form-control" required name="answer"></div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Options:</label><input type="text" class="form-control" placeholder="Option" required name="ques[]"><br><input type="text" class="form-control" placeholder="Option" name="ques[]"><a href="JavaScript:Void(0);" onclick="addmore();" id="add-radio-option">Add More</a></div></div>');
						$('#content-btn-sub').css('display','block');
						break;
					
					
					case 'Form':
						$('#content-field').children('div').remove();
						var per_id = $('#persona-id').val();
						$('#content-field').append('<div class="row"><iframe class="form-iframe" src="http://67.209.127.45/chatbot/public/backoffice/form-builder/forms/create/'+per_id+'" frameborder="0" height="850" style="position:relative; top:0; left:0; bottom:0; right:0; width:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;" ></iframe></div>');
						$('#content-btn-sub').css('display','none');
						break;
				}*/
			}
		});
	});

	function contentImage(input) {
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    
	    reader.onload = function(e) {
	      $('.upload_persona_image').val('');
	      $('#contentImage').attr('src', e.target.result);
	    }
	    
	    reader.readAsDataURL(input.files[0]);
	  }
	}

	jQuery(document).on('change','.upload_persona_content_image',function(){
		contentImage(this);
	});

	

/*==============Change password functionality for manage-admin===========*/

  	jQuery(document).on('change','#current_pwd',function(){
  		var currentPwd = jQuery(this).val();
	  	if(currentPwd != '')
	  	{
			$.ajax({
				method: "GET",
				url:"{{url('backoffice/persona/check_current_password')}}",
				data: { currentPwd:currentPwd},
				dataType:"json",
				success:function(data) 
				{
					if(data.success == true)
					{
						jQuery('#new_pwd').prop('required',true);
						jQuery('#new_pwd').attr('name','new_password');
	    				jQuery('#confirm_pwd').prop('required',true);
	    				jQuery('#confirm_pwd').attr('name','confirm_password');
	    				jQuery('.current-pwd-msg').html('');
					}
					else
					{
						jQuery('.current-pwd-msg').html('Password not match');
					}
					
				}
			});
	  	}
	  	else
	  	{
	  		jQuery('#new_pwd').prop('required',false);
			jQuery('#new_pwd').attr('name','');
			jQuery('#confirm_pwd').prop('required',false);
			jQuery('#confirm_pwd').attr('name','');
			jQuery('.current-pwd-msg').html('');
	  	}
   	});

   	jQuery(document).on('change','#new_pwd',function(){
   		confirmPwd()
   	});
   	jQuery(document).on('change','#confirm_pwd',function(){
   		confirmPwd()
	});

   	function confirmPwd()
   	{
   		var newPwd = jQuery('#new_pwd').val();
	  	var confirmPwd = jQuery('#confirm_pwd').val();
	  	if(confirmPwd == '')
	  	{
	  		jQuery('.conf-pwd-msg').html('');
	  	}
	  	else if(newPwd != confirmPwd)
	  	{
	  		jQuery('.conf-pwd-msg').html('Password not match');
	  	}
	  	
   	}

 /*============Duplicate persona=========*/

 	jQuery(document).on('click','.duplicate-persona',function(){
 		var personaId = jQuery(this).attr('data-attr');
 		jQuery(this).html('Processing...');
 		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/add-duplicate-persona')}}",
			data: { personaId:personaId},
			dataType:"json",
			success:function(data) 
			{
				if(data.url != '')
				{
					window.location.href = data.url;
				}
				else
				{
					jQuery(this).html('Duplicate');
					jQuery('.current-pwd-msg').html('Password not match');
				}
				
			}
		});

 	});


/*=============Delete functionality============*/

	jQuery(document).on('click','.deleteFun',function(){
		jQuery('#deleteModal').modal();
		var id = jQuery(this).attr('data-target');
		var deleteStatus = jQuery(this).attr('data-value');
		var tableName = jQuery(this).attr('data-table');
		var modalMsg = jQuery(this).attr('data-attr');
		var modalTitle = jQuery(this).attr('data-href');
		jQuery('.delete-modal-text').html(modalMsg);
		jQuery('.delete-status').val(deleteStatus);
		jQuery('.delete-modal-title').html(modalTitle);
		jQuery('.delete-id').val(id);
		jQuery('.table-name').val(tableName);
	});

/*==========Edit brand==========*/

	$(document).on("click", ".edit-brand", function () {
		var ind_ID = $(this).data('id');
	    $(".modal-body #brand-id").val( ind_ID );
		var nombre = $(this).data('iname');
	    $(".modal-body #brand-name").val( nombre );
		var posicion = $(this).data('istatus');
	    $('.modal-body #brand-status').find("option[value='"+posicion+"']").attr('selected', 'selected');
	});

/*=======Edit compaign=======*/

	$(document).on("click", ".edit-compaign", function () {
		var ind_ID = $(this).data('id');
	    $(".modal-body #compaign-id").val( ind_ID );
		var nombre = $(this).data('iname');
	    $(".modal-body #compaign-name").val( nombre );
		var posicion = $(this).data('istatus');
	    $('.modal-body #compaign-status').find("option[value='"+posicion+"']").attr('selected', 'selected');
	}); 

/*=============On change Persona industry set url=========*/
			
	jQuery('#persona_industries').change(function(event){
		chatUrl();
	});

/*===========On change brand compaigns set url========*/

	jQuery('#persona_compaigns').change(function(event){
		chatUrl();
	});

/*=============On change Persona brand set url=========*/
			
	jQuery('#persona_brands').change(function(event){
		var brandId = $(this).val();
		jQuery('.brand').val(brandId);
		jQuery.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_brand_compaign')}}",
			data: { brandId:brandId},
			dataType:"html",
			success:function(data) 
			{
				jQuery('#persona_compaigns').html(data);
				jQuery('.persona_compaign').html(data);
				jQuery('.brand-compaigns').css('display','block');
			}
		});
        chatUrl();
	});

/*===========On change Prsona title set url=============*/

	jQuery('#persona_title').blur(function(event){
		chatUrl();
	});

/*======function to change chat url when add and edit persona========*/

	function chatUrl()
	{
		var url_val=  "{{url('/')}}/chat"; 
		var industryVal = $('option:selected', '#persona_industries').attr('data-attr');
		var brandVal = $('option:selected', '#persona_brands').attr('data-attr');
       	var titleVal = $('#persona_title').val();
       	var compVal = $('option:selected', '#persona_compaigns').attr('data-attr');
       	if(brandVal != '' && industryVal != '' && titleVal != '' && compVal != '')
		{
			var industryAttr = $('#persona_industries').val();
			var brandAttr = $('#persona_brands').val();
	       	var compAttr = $('#persona_compaigns').val();
	       	var url = url_val+'/'+industryAttr+'/'+brandAttr+'/'+compAttr+'/'+titleVal ;
	       	jQuery('#set_persona_url').val(url);
			
			var brandTemp = brandVal.toLowerCase().replace(/ +/g,'-').replace(/[0-9]/g,'').replace(/[^a-z0-9-_]/g,'').trim();
			var industrialTemp = industryVal.toLowerCase().replace(/ +/g,'-').replace(/[0-9]/g,'').replace(/[^a-z0-9-_]/g,'').trim();
			var titleTemp = titleVal.toLowerCase().replace(/ +/g,'-').replace(/[0-9]/g,'').replace(/[^a-z0-9-_]/g,'').trim();
			var compTemp = compVal.toLowerCase().replace(/ +/g,'-').replace(/[0-9]/g,'').replace(/[^a-z0-9-_]/g,'').trim();
			var newURL = url_val+'/'+industrialTemp+'/'+brandTemp+'/'+compTemp+'/'+titleTemp ;
			$('#persona_url').val(newURL);
		}
	}

	$(document).on("click", ".edit-btn", function () {
     var ind_ID = $(this).data('id');
     $(".modal-body #industry-id").val( ind_ID );
	 var nombre = $(this).data('iname');
     $(".modal-body #industry-name").val( nombre );
	 var posicion = $(this).data('istatus');
     //$(".modal-body #industry-status").val( posicion );
	 $('.modal-body #industry-status').find("option[value='"+posicion+"']").attr('selected', 'selected');
});
	
</script>

<script>

/*=========On change score check min score greater than zero=========*/
	
	jQuery(document).on('keyup','#checkMinScore',function(){
		var score = jQuery(this).val();
		if(score == 0)
		{
			jQuery(this).val('');
		}
	});

/*================On change response from persona content get query or input========*/

	$("#response-type").change(function()
	{
    	var newValue = $(this).children("option:selected").val();
		switch (newValue) 
		{
			case 'Embed':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer"><label for="Query">Button Label:</label><input type="text" class="form-control" required name="btn-label"><label for="Query">Response:</label><textarea id="editor1" class="form-control"  name="query"></textarea></div></div>');
				$('#content-btn-sub').css('display','block');
				tinymce.init({
					selector: "#editor1",
				 	plugins: "media",
				  	menubar: "insert",
				  	toolbar: "media",
				  	media_scripts: [
					   {filter: 'http://media1.tinymce.com'},
					   {filter: 'http://media2.tinymce.com', width: 350, height: 200}
					 ]
				});
				break;
			case 'Text':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Query">Response:</label><input type="text" class="form-control" required name="query"><label for="Query">Email:</label><input type="checkbox" class="form-control"  name="email" value="email"></div></div>');
				$('#content-btn-sub').css('display','block');
				break;
			case 'Textarea':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer"><label for="Query">Response:</label><textarea id="editor2" class="form-control"  name="query"></textarea></div></div>');
				$('#content-btn-sub').css('display','block');
				tinymce.init({
					selector: "#editor2",
					plugins: "link image",
					inline_styles: false,
				});
				break;
			case 'Button':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer"><label for="Query">Button Label:</label><input type="text" class="form-control" required name="query"></div></div>');
				$('#content-btn-sub').css('display','block');
				break;
			case 'Radio':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer"></div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Options:</label><input type="text" class="form-control" placeholder="Option" required name="ques[]"><br><input type="text" class="form-control" placeholder="Option" name="ques[]"><a href="JavaScript:Void(0);" onclick="addmore();" id="add-radio-option">Add More</a></div></div>');
				$('#content-btn-sub').css('display','block');
				break;
			case 'Checkbox':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Answer">Query:</label><input type="text" class="form-control" required name="answer"></div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Options:</label><input type="text" class="form-control" placeholder="Option" required name="ques[]"><br><input type="text" class="form-control" placeholder="Option" name="ques[]"><a href="JavaScript:Void(0);" onclick="addmore();" id="add-radio-option">Add More</a></div></div>');
				$('#content-btn-sub').css('display','block');
				break;
			case 'File':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer"><label for="Query">Button Label:</label><input type="text" class="form-control" required name="btn-label"></div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Select:</label><input type="file" class="form-control" name="file-input"><br></div></div>');
				$('#content-btn-sub').css('display','block');
				break;
			case 'Docs':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer"><label for="Query">Button Label:</label><input type="text" class="form-control" required name="btn-label"></div></div><div class="row"><div class="form-group col-md-6"><label for="Query">Select:</label><input type="file" class="form-control" name="file-input"><br></div></div>');
				$('#content-btn-sub').css('display','block');
				break;
			case 'Link':
				$('#content-field').children('div').remove();
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"><label for="Query">Query:</label><input type="text" class="form-control" required name="answer"><label for="Query">Link:</label><input type="text" value="http://" class="form-control" name="link-input"><br></div></div>');
				$('#content-btn-sub').css('display','block');
				break;
			case 'Form':
				$('#content-field').children('div').remove();
				var per_id = $('#persona-id').val();
				var url = "{{url('/backoffice/form-builder/forms/create/')}}";
				$('#content-field').append('<div class="row"><div class="form-group col-md-6"><input type="number" placeholder="Score" class="form-control persona_score" required name="score" min="0"></div><iframe class="form-iframe" src="'+url+'/'+per_id+'" frameborder="0" height="850" style="position:relative; top:0; left:0; bottom:0; right:0; width:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;" ></iframe></div>');
				$('#content-btn-sub').css('display','none');
				break;
			
		}
	});

/*=========On change person score from content check score==========*/

	jQuery(document).on('change keyup','.persona_score',function(){
		var score = jQuery(this).val();
		var personaId = jQuery('#persona-id').val();
		jQuery('.content-btn').prop('disabled',true);
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_persona_score')}}",
			data: { score:score, personaId:personaId },
			dataType: "json",
			success:function(data) {
				if(data.score_sum)
				{
					totalScoreSum = parseInt(data.score_sum)+parseInt(score);
					if(totalScoreSum > parseInt(data.score))
					{
						jQuery('.persona_score').val('');
					}
				}
				else
				{
					if(parseInt(score) > parseInt(data.score))
					{
						jQuery('.persona_score').val('');
					}
				}
				jQuery('.content-btn').prop('disabled',false);
			}
		});
	});




function addmore(){
	//alert('test');
	$('#add-radio-option').parent().append('<input type="text" class="form-control" placeholder="Option" name="ques[]">');
}

function configure_content(content_id, persona_id){
	//$('#configure-content').modal('show');
	$.ajax( {
		method: "GET",
		url:"{{url('backoffice/persona/content_configure')}}",
		data: { cont_id:content_id, per_id:persona_id },
		success:function(data) {
			$('#configure-content div.modal-body').html(data);
			$('#configure-content').modal('show');
		}
	});
}

var rowNum = 0; 

function chatTry(){
	rowNum++;
	var last_msg_det = $("#chat-messege li").last().attr("id");
	var lastmsgtype = last_msg_det.split('_');
	if(lastmsgtype[2] == "Text"){
		var messege = $('#try-chat-inputbox').val();
		var personaId = $('#persona-id').val();
		var dt = new Date();
		var cr_time = dt.getHours() + ":" + dt.getMinutes();
		
		//alert(last_msg_det);
		//return false ;
		var messageHTML = '<li id="msg-text-'+rowNum+'"><div class="chat_box chat_box_left"><div class="avatar"><img class="img-circle" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="txt"><p>'+messege+'</p><p><small>'+cr_time+'</small></p></div></div></li>';
		$('#chat-messege').append(messageHTML);
	}else if(lastmsgtype[2] == "Radio"){
		var messege = $("input[name='"+lastmsgtype[3]+"']:checked").val();
		var personaId = $('#persona-id').val();
		var dt = new Date();
		var cr_time = dt.getHours() + ":" + dt.getMinutes();
		
		//alert(last_msg_det);
		//return false ;
		var messageHTML = '<li id="msg-text-'+rowNum+'"><div class="chat_box chat_box_left"><div class="avatar"><img class="img-circle" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="txt"><p>'+messege+'</p><p><small>'+cr_time+'</small></p></div></div></li>';
		$('#chat-messege').append(messageHTML);
	}
	
	$.ajax( {
		method: "GET",
		url:"{{url('backoffice/persona/try_chat_response')}}",
		data: { user_msg:messege, msg_pos:rowNum, persona_id:personaId, msg_det:last_msg_det},
		success:function(data) {
			$('#chat-messege').append(data);
			$('#try-chat-inputbox').val('');
		}
	});
}

function start_try_chat(){
	var personaId = $('#persona-id').val();
	$.ajax({
		method: "GET",
		url:"{{url('backoffice/persona/get_chat_response')}}",
		data: {persona_id:personaId},
		success:function(data){
			$('#chat-messege').append(data);
			$('#try-chat-btn').css('display', 'none');
			$('#try-chat-input').css('display', 'block');
		}
	});
}


$('#try-chat-inputbox').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    chatTry();
                }
                //Stop the event from propogation to other handlers
                //If this line will be removed, then keypress event handler attached
                //at document level will also be triggered
                event.stopPropagation();
            });

/*=Start insight js=*/

/*===========On select brand,industry and comapign type get personas==============*/

	jQuery(document).on('change','.get-persona',function(){
		var brandId = jQuery('#persona_brands').val();
		var industryId = jQuery('#persona_industries').val();
		var compaignId = jQuery('#persona_compaigns').val();
		if(brandId != '' && industryId !='' && compaignId != '')
		{
			jQuery.ajax({
				method: "GET",
				url:"{{url('backoffice/insights/get_persona_title')}}",
				data: {brandId:brandId,industryId:industryId,compaignId:compaignId},
				success:function(data)
				{
					$('#select_persona').html(data);
				}
			});
		}
	});

	jQuery(document).on('change','.get-persona2',function(){
		var brandId = jQuery('#persona_brands').val();
		var industryId = jQuery('#persona_industries2').val();
		var compaignId = jQuery('#persona_compaigns2').val();
		if(brandId != '' && industryId !='' && compaignId != '')
		{
			jQuery.ajax({
				method: "GET",
				url:"{{url('backoffice/insights/get_persona_title')}}",
				data: {brandId:brandId,industryId:industryId,compaignId:compaignId},
				success:function(data)
				{
					$('#select_persona2').html(data);
				}
			});
		}
	});

	jQuery(document).on('change','.get-persona3',function(){
		var brandId = jQuery('#persona_brands').val();
		var industryId = jQuery('#persona_industries3').val();
		var compaignId = jQuery('#persona_compaigns3').val();
		if(brandId != '' && industryId !='' && compaignId != '')
		{
			jQuery.ajax({
				method: "GET",
				url:"{{url('backoffice/insights/get_persona_title')}}",
				data: {brandId:brandId,industryId:industryId,compaignId:compaignId},
				success:function(data)
				{
					$('#select_persona3').html(data);
				}
			});
		}
	});

	jQuery(document).on('change','.get-persona4',function(){
		var brandId = jQuery('#persona_brands').val();
		var industryId = jQuery('#persona_industries4').val();
		var compaignId = jQuery('#persona_compaigns4').val();
		if(brandId != '' && industryId !='' && compaignId != '')
		{
			jQuery.ajax({
				method: "GET",
				url:"{{url('backoffice/insights/get_persona_title')}}",
				data: {brandId:brandId,industryId:industryId,compaignId:compaignId},
				success:function(data)
				{
					$('#select_persona4').html(data);
				}
			});
		}
	});

	jQuery(document).on('click','#compare_persona',function()
	{
		if(jQuery('.persona1').val() == '' && jQuery('.persona2').val() == '')
		{
			alert('Choose atleast two persona for comparison');
		}
		if(jQuery('.persona1').val() != '' && jQuery('.persona2').val() == '')
		{
			alert('Choose atleasrt two persona for comparison');
		}
		if(jQuery('.persona1').val() != '' && jQuery('.persona2').val() != '')
		{
			console.log('fdsa');
			persona1 = jQuery('.persona1').val();
			persona2 = jQuery('.persona2').val();
			persona3 = jQuery('.persona3').val();
			persona4 = jQuery('.persona4').val();
			brand = jQuery('.brand').val();
			var url = "{{url('backoffice/insights/insight_comparison')}}"+'/'+brand+'/'+persona1+'/'+persona2;
			if(persona3 != '')
			{
				var url = "{{url('backoffice/insights/insight_comparison')}}"+'/'+brand+'/'+persona1+'/'+persona2+'/'+persona3;
			}
			if(persona4 != '')
			{
				var url = "{{url('backoffice/insights/insight_comparison')}}"+'/'+brand+'/'+persona1+'/'+persona2+'/'+persona3+'/'+persona4;
			}
			jQuery(this).attr('href',url);
		}
	});

	jQuery(document).on('change','.persona_one_title',function()
	{
		var personaId = jQuery(this).val();
		if(personaId != '')
		{
			jQuery('.persona1').val(personaId);
			jQuery('#persona_industries2').prop('disabled',false);
			jQuery('#persona_compaigns2').prop('disabled',false);
			jQuery('#select_persona2').prop('disabled',false);
		}
	});

	jQuery(document).on('click','.persona_two_title',function()
	{
		var personaId = jQuery(this).val();
		if(personaId != '')
		{
			jQuery('.persona2').val(personaId);
			jQuery('#persona_industries3').prop('disabled',false);
			jQuery('#persona_compaigns3').prop('disabled',false);
			jQuery('#select_persona3').prop('disabled',false);
		}
	});

	jQuery(document).on('click','.persona_three_title',function()
	{
		var personaId = jQuery(this).val();
		if(personaId != '')
		{
			jQuery('.persona3').val(personaId);
			jQuery('#persona_industries4').prop('disabled',false);
			jQuery('#persona_compaigns4').prop('disabled',false);
			jQuery('#select_persona4').prop('disabled',false);
		}
	});

	jQuery(document).on('click','.persona_four_title',function()
	{
		var personaId = jQuery(this).val();
		if(personaId != '')
		{
			jQuery('.persona4').val(personaId);
		}
	});

	jQuery(document).on('click','.choose_brand',function(){
		var brandId = jQuery(this).attr('data-attr');
		jQuery('.selectedBrand').html(jQuery(this).attr('data-target'));
		jQuery('.persona_brands').val(brandId);
		jQuery('.select_brand_wrapper').css('display','none');
		jQuery('.select_industry_wrapper').css('display','block');
		jQuery.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_brands_compaign')}}",
			data: { brandId:brandId},
			dataType:"html",
			success:function(data) 
			{
				jQuery('.personaCompType').html(data);
			}
		});
	});

	jQuery(document).on('click','.choose_industry',function(){
		var industryId = jQuery(this).attr('data-attr');
		jQuery('.selectedIndustry').html(jQuery(this).attr('data-target'));
		jQuery('.persona_industries').val(industryId);
		jQuery('.select_industry_wrapper').css('display','none');
		jQuery('.select_camptype_wrapper').css('display','block');
	});

	jQuery(document).on('click','.industryBack',function(){
		jQuery('.select_industry_wrapper').css('display','none');
		jQuery('.select_brand_wrapper').css('display','block');
	});

	jQuery(document).on('click','.compTypeBack',function(){
		jQuery('.select_camptype_wrapper').css('display','none');
		jQuery('.select_industry_wrapper').css('display','block');
	});

	jQuery(document).on('click','.compListBack',function(){
		$('.select_camplist_wrapper').css('display','none');
		jQuery('.select_camptype_wrapper').css('display','block');
	});

	jQuery(document).on('click','.brandsComp',function(){
		var compaignId = jQuery(this).attr('data-attr');
		var industryId = jQuery('.persona_industries').val();
		var brandId = jQuery('.persona_brands').val();
		jQuery('.selectedCompType').html(jQuery(this).attr('data-target'));
		jQuery('.persona_compaigns').val(compaignId);
		jQuery.ajax({
			method: "GET",
			url:"{{url('backoffice/insights/personas_detail')}}",
			data: {brandId:brandId,industryId:industryId,compaignId:compaignId},
			success:function(data)
			{
				$('.personasTitle').html(data);
				$('.select_camptype_wrapper').css('display','none');
				$('.select_camplist_wrapper').css('display','block');
			}
		});
	});

	jQuery(document).on('click','.personasDetails',function(){
		var personaId = jQuery(this).attr('data-attr');
		jQuery('.select_persona').val(personaId);
		jQuery('#insight_form').submit();
		jQuery('.selectedCompList').html(jQuery(this).attr('data-target'));
	});

	jQuery(document).on('change','.search_brand_list',function(){
		var keyword = jQuery(this).val();
		if(keyword.length >= 3 || keyword == '')
		{
			jQuery.ajax({
				method: "GET",
				url:"{{url('backoffice/insights/get_brand_list')}}",
				data: {keyword:keyword},
				success:function(data)
				{
					jQuery('.brandList').html(data);
				}
			});
		}
	})

</script>
</body>
</html>
