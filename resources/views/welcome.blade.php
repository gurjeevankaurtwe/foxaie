<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Chat Bot</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="{{asset('admin/img/placeholders/avatars/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/themes.css')}}">
	<!--<script src="{{ asset('js/app.js') }}" defer></script>-->
    <script src="{{asset('admin/js/vendor/modernizr-respond.min.js')}}"></script>
	
        <!-- Styles -->
        <style>					
			.chat_box_wrapper{				
				width: 40% !important;				
				margin: 30px auto !important;				
				max-height: 450px;				
				overflow-y: scroll;				
				padding: 18px;				
				background: #fff;				
				border-radius: 5px;			
			}						
			.chat_box_wrapper::-webkit-scrollbar-track{				
				-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);				
				border-radius: 10px;				
				background-color: #F5F5F5;			
			}			
			.chat_box_wrapper::-webkit-scrollbar {				
				width: 12px;				
				background-color: #F5F5F5;			
			}			
			.chat_box_wrapper::-webkit-scrollbar-thumb	{				
				border-radius: 10px;				
				-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);				
				background-color: #555;			
			}
			.ul_chat_module_new{
				width: 100% !important;				
				border: none;				
				position: relative;				
				padding: 0px;				
				margin-bottom: 0px;				
				overflow-y: hidden;
			}
			.chat_footer_new{
				position: relative !important;

				bottom: unset;

				width: 40% !important;

				margin: 0px auto !important;
				background: #b4b4b4;
			}
			#try-chat-input{
				width: 100%;

			 padding: 20px;
			}
        </style>
    </head>
    <body>

            <div class="content">
			
				<form name="start_chat" action="" method="post">
					<div class="row">
						<div class="col-md-4"></div>
						<div class="form-group col-md-4">
							<label for="Industry">Select Industry:</label>
							<select class="form-control" name="industry" id="persona_industries" required>
								<option value="">Select</option>
								 @foreach($industries as $industry)	
								<option value="{{$industry->industry}}">{{$industry->industry}}</option>
								 @endforeach
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"></div>
						<div class="form-group col-md-4">
							<label for="Industry">Select Persona:</label>
							<select class="form-control" name="persona" id="persona_sel" required>
								<option value="">Select</option>
								
							</select>
						</div>
					</div> 
					<div class="row">
						<div class="col-md-4"></div>
						<div class="form-group col-md-4">
							<button type="button" id="subfrmch" class="btn btn-success">Submit</button>
						</div>
					</div>
				</form>
                    <!--<div class="chat_module" style="min-height:650px !important;height:auto !important;">					
						<div class="chat_box_wrapper">
							<ul id="chat-messege" class="ul_chat_module ul_chat_module_new"></ul>
						</div>
						<div class="chat_footer chat_footer_new">
							<p><a href="JavaScript:void(0);" id="try-chat-btn" class="btn btn-warning" onclick="start_try_chat();">Start Chat</a></p>
							<div id="try-chat-input" style="display:none">
								<div class="chat_input" style="float: left;">                        
									<div class="chat_input_box" style="background:whitesmoke !important">
										<input class="mytext" name="try-chat-inputbox" id="try-chat-inputbox" placeholder="Type a message" />
									</div> 
								</div>
								<div class="chat_btn" onclick="chatTry();">
									<span class="fa fa-share" style="margin-left: 10px;"></span>
								</div>
							</div>
						</div>
					</div>-->
					
				
            </div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>
		<script src="{{asset('admin/js/vendor/bootstrap.min.js')}}"></script>
		<script src="{{asset('admin/js/plugins.js')}}"></script>
		<script src="//maps.google.com/maps/api/js?sensor=true"></script>
		<script src="{{asset('admin/js/helpers/gmaps.min.js')}}"></script>
		<script src="{{asset('admin/js/helpers/ckeditor/ckeditor.js')}}"></script>
		<script>
		/************************************/
		$("#persona_industries").change(function(event){
			
			var newValue = $(this).children("option:selected").val();
			$.ajax({
				method: "GET",
				url:"{{url('chat/get_persona')}}",
				data: {industry:newValue},
				success:function(data){
					$('#persona_sel').html(data);
					
				}
			});
		});
		
		/***********************************/
		
		var rowNum = 0; 
function chatTry(){
	rowNum++;
	var last_msg_det = $("#chat-messege li").last().attr("id");
	var lastmsgtype = last_msg_det.split('_');
	if(lastmsgtype[2] == "Text"){
		var messege = $('#try-chat-inputbox').val();
		var personaId = '5ce8dd34923dac42c1282da2';
		var dt = new Date();
		var cr_time = dt.getHours() + ":" + dt.getMinutes();
		
		//alert(last_msg_det);
		//return false ;
		var messageHTML = '<li id="msg-text-'+rowNum+'"><div class="chat_box chat_box_left"><div class="avatar"><img class="img-circle" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="txt"><p>'+messege+'</p><p><small>'+cr_time+'</small></p></div></div></li>';
		$('#chat-messege').append(messageHTML);
	}else if(lastmsgtype[2] == "Radio"){
		var messege = $("input[name='"+lastmsgtype[3]+"']:checked").val();
		var personaId = '5ce8dd34923dac42c1282da2';
		var dt = new Date();
		var cr_time = dt.getHours() + ":" + dt.getMinutes();
		
		//alert(last_msg_det);
		//return false ;
		var messageHTML = '<li id="msg-text-'+rowNum+'"><div class="chat_box chat_box_left"><div class="avatar"><img class="img-circle" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="txt"><p>'+messege+'</p><p><small>'+cr_time+'</small></p></div></div></li>';
		$('#chat-messege').append(messageHTML);
	}else if(lastmsgtype[2] == "Form"){
		var messege = $("#form_"+lastmsgtype[3]).serialize();;
		var personaId = '5ce8dd34923dac42c1282da2';
		var dt = new Date();
		var cr_time = dt.getHours() + ":" + dt.getMinutes();
		
		//alert(last_msg_det);
		//return false ;
		var messageHTML = '<li id="msg-text-'+rowNum+'"><div class="chat_box chat_box_left"><div class="avatar"><img class="img-circle" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="txt"><p>Sumit successfully</p><p><small>'+cr_time+'</small></p></div></div></li>';
		$('#chat-messege').append(messageHTML);
	}else{
		var messege = $('#try-chat-inputbox').val();
		var personaId = '5ce8dd34923dac42c1282da2';
		var dt = new Date();
		var cr_time = dt.getHours() + ":" + dt.getMinutes();
		
		//alert(last_msg_det);
		//return false ;
		var messageHTML = '<li id="msg-text-'+rowNum+'"><div class="chat_box chat_box_left"><div class="avatar"><img class="img-circle" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="txt"><p>'+messege+'</p><p><small>'+cr_time+'</small></p></div></div></li>';
		$('#chat-messege').append(messageHTML);
	}
	
	$.ajax( {
		method: "GET",
		url:"{{url('backoffice/persona/try_chat_response')}}",
		data: { user_msg:messege, msg_pos:rowNum, persona_id:personaId, msg_det:last_msg_det},
		success:function(data) {
			$('#chat-messege').append(data);
			$('#try-chat-inputbox').val('');
			$('.chat_box_wrapper').animate({scrollTop: $('.chat_box_wrapper').prop("scrollHeight")}, 500);
		}
	});
}

function start_try_chat(){
	var personaId = '5ce8dd34923dac42c1282da2';
	$.ajax({
		method: "GET",
		url:"{{url('backoffice/persona/get_chat_response')}}",
		data: {persona_id:personaId},
		success:function(data){
			$('#chat-messege').append(data);
			$('#try-chat-btn').css('display', 'none');
			$('#try-chat-input').css('display', 'block');
		}
	});
}


$('#try-chat-inputbox').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    chatTry();
                }
                //Stop the event from propogation to other handlers
                //If this line will be removed, then keypress event handler attached
                //at document level will also be triggered
                event.stopPropagation();
            });
			
$('#subfrmch').click(function(event){
			var selpersona = $('#persona_sel').children("option:selected").val();
			$.ajax({
				method: "GET",
				url:"{{url('chat/get_url')}}",
				data: {pers:selpersona},
				success:function(data){
					window.location.href = data;
					
				}
			});
		});
		</script>
    </body>
</html>
