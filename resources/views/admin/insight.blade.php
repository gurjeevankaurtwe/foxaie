@extends('layouts.admin')

@section('content')

  	<h2 class="text-center">Insights Category</h2>
	
	<div class="row" style="margin-top:60px;">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="col-sm-12 col-lg-6">
					<a href="{{url('backoffice/insights/comapign_form')}}" class="widget widget-hover-effect1 insight_widget"> 
						<div class="widget-simple">
							<div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
								<i class="fa fa-file-text"></i>
							</div>
							<h3 class="widget-content text-right animation-pullDown">
								<strong>Build Campaigns</strong>
							</h3>
						</div>
					</a>
				</div>
				<div class="col-sm-12 col-lg-6">
					<a href="javascript:void();" class="widget widget-hover-effect1 insight_widget">
						<div class="widget-simple">
							<div class="widget-icon pull-left themed-background-spring animation-fadeIn">
								<i class="fa fa-file-text"></i>
							</div>
							<h3 class="widget-content text-right animation-pullDown">
								<strong>View Insights</strong>
							</h3>
						</div>
					</a>
				</div>				
			</div>
		</div>
	</div>	

@endsection