@extends('layouts.admin')

@section('content')

<?php



/*===========Yearly record=========*/

	/*$yearly_record = array();
	foreach($yearlyRecord as $key=>$yearly)
	{
		$yearly_record[$key] =  $yearly->sum('clicks');
	}
	ksort($yearly_record);
	$yearlyPointStart = key($yearly_record);
	$years = implode("','", array_map(
	    function ($v, $k)
	    { 
	    	return sprintf("%s", $k, $v); 
	    },
	    $yearly_record,
	    array_keys($yearly_record)
	));*/
	
	

/*===========Monthly Record==========*/

	$monthly_record = array();
	$monRecords = array();
	foreach($monthlyRecord as $key=>$val)
	{
		foreach($val as $month)
		{
			$monthly_record[date('Y-m-d',strtotime($month->created_at))][] = $month->clicks;
		}
	}

	foreach($monthly_record as $key=>$monRec)
	{
		$monRecords[$key] = array_sum($monRec);
	}
	ksort($monRecords);
	
	$currentMonthsDate = implode("','", array_map(
	    function ($v, $k)
	    { 
	    	return sprintf("%s", $k, $v); 
	    },
	    $monRecords,
	    array_keys($monRecords)
	));

	

/*===========Weekly Record========*/

	/*$weekly_record = array();

	foreach($weeklyRecord as $key=>$val)
	{
		$weekly_record[$key] = $val->sum('clicks');
	}
	ksort($weekly_record);
	$currentWeek = implode("','", array_map(
	    function ($v, $k)
	    { 
	    	return sprintf("%s", $k, $v); 
	    },
	    $weekly_record,
	    array_keys($weekly_record)
	));*/
?>

<div class="select_camplists_wrapper" style="display:block;">

	<input type="hidden" class="personaId" value="{{$persona->_id}}">
	
	<div class="text-left outer_title">
		<h2>Campaigns details</h2>
		<ul>
			<li>{{ucfirst($brand->brand)}}</li>
			<li>{{ucfirst($industry->industry)}}</li>
			<li>{{ucfirst($compaignType->compaign)}}</li>
			<li>{{ucfirst($persona->persona_title)}}</li>
		</ul>
	</div>

	<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id)}}">Compaign Overview</a>
	<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id)}}">Participation</a>
	<a href="{{url('backoffice/insights/compaign_participants/'.$persona->_id)}}">Participants</a>
	<a href="{{url('backoffice/insights/insight_comparison')}}">Comparison</a>
	
	<div class="social_list">
		<ul class="nav nav-pills nav-justified">
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id.'/facebook')}}" class="nav-link">Facebook</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id.'/twitter')}}" class="nav-link">Twitter</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id.'/linkdin')}}" class="nav-link">Linkedin</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id.'/instagram')}}" class="nav-link">Instagram</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id.'/google')}}" class="nav-link">Google</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id.'/youtube')}}" class="nav-link">Youtube</a>
			</li>
		</ul>
	</div>
	
	<div class="row" style="margin-top:20px;">
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-clock-o"></i>
				</div>
				<div class="content">
					<p>Lifetime Intractions</p>
					<h2>{{$lifetimeVisitors->sum('clicks')}}</h2>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-calendar" style="color:#886BFF;"></i>
				</div>
				<div class="content">
					<p>Month Wise Intractions</p>
					<h2>{{$monthlyVisitors->sum('clicks')}}</h2>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-calendar" style="color:#FABC01;"></i>
				</div>
				<div class="content">
					<p>Week Wise Intractions</p>
					<h2>{{$weeklyVisitors->sum('clicks')}}</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-sm-12">
		<div class="camp_chart">
			<div class="camp_head">
				<div class="form-group">
					<input type="text" class="form-control getMonthlyParGraph" placeholder="From Date" id="par_from_date" autocomplete="off"> 
				</div>
				<div class="form-group">
					<input type="text" class="form-control getMonthlyParGraph" placeholder="To Date" id="par_to_date" autocomplete="off"> 
				</div>
				<div class="export_btn">
					<!-- <a href="javascript:void();">Export</a> -->
					<select class="form-control graphType">
						<option value="">Select graph type...</option>
						<option value="Line">Line Graph</option>
						<option value="Bar">Bar Graph</option>
					</select>
				</div>
				
			</div>
			<div class="monthly_graph"> 
				<div id="monthly_container"></div> 
				<div id="monthly_bar_container" style="display:none"></div> 
			</div>
		</div>
	</div>
	
</div>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>

/*=============Lifetime visitors============*/

	/*Highcharts.chart('container', {

   	 	xAxis: {
               categories: [<?php //echo "'".$years."'";?>],
            },

	    series: [{
	        name: 'Lifetime Interactions',
	        data: [<?php //echo implode(",",$yearly_record); ?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});*/

/*=============Monthly visitors============*/

	Highcharts.chart('monthly_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            
	        }
	    },
	    chart: {
	        type: 'area'
	    },
	    title: {
	        text: 'Monthly Interactions'
	    },
	    xAxis: {
               categories: [<?php echo "'".$currentMonthsDate."'";?>],
            },

	    series: [{
	        name: 'Monthly Interactions',
	        data: [<?php echo implode(",",$monRecords);?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});

	Highcharts.chart('monthly_bar_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            
	        }
	    },
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Monthly Interactions'
	    },
	    xAxis: {
               categories: [<?php echo "'".$currentMonthsDate."'";?>],
            },

	    series: [{
	        name: 'Monthly Interactions',
	        data: [<?php echo implode(",",$monRecords);?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});

/*=============Weekly interactions============*/

	/*Highcharts.chart('weekly_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            
	        }
	    },
	    xAxis: {
               categories: [<?php //echo "'".$currentWeek."'";?>],
            },

	    series: [{
	        name: 'Weekly Interactions',
	        data: [<?php //echo implode(",",$weekly_record);?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});*/
</script>

@endsection

