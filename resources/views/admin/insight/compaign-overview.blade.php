@extends('layouts.admin')

@section('content')

<?php 
	
	/*===========Yearly record=========*/

	/*$yearly_record = array();
	foreach($yearlyRecord as $key=>$yearly)
	{
		$yearly_record[$key] =  count($yearly);
	}
	ksort($yearly_record);
	$yearlyPointStart = key($yearly_record);*/

	/*===========Monthly Record==========*/

	$monthly_record = array();
	$monRecords = array();
	foreach($monthlyRecord as $key=>$val)
	{
		foreach($val as $month)
		{

			$monthly_record[date('Y-m-d',strtotime($month->created_at))][] = $month->_id;
		}
	}
	foreach($monthly_record as $key=>$monRec)
	{
		$monRecords[$key] = count($monRec);
	}
	ksort($monRecords);
	$currentMonthsDate = implode("','", array_map(
	    function ($v, $k)
	    { 
	    	return sprintf("%s", $k, $v); 
	    },
	    $monRecords,
	    array_keys($monRecords)
	));

/*===========Weekly Record========*/

	/*$weekly_record = array();

	foreach($weeklyRecord as $key=>$val)
	{
		$weekly_record[$key] = count($val);
	}
	ksort($weekly_record);
	$currentWeek = implode("','", array_map(
	    function ($v, $k)
	    { 
	    	return sprintf("%s", $k, $v); 
	    },
	    $weekly_record,
	    array_keys($weekly_record)
	));*/
	
	
?>

<div class="select_camplists_wrapper" style="display:block;">

	<input type="hidden" class="personaId" value="{{$persona->_id}}">
	
	<div class="text-left outer_title">
		<h2>Campaigns details</h2>
		<ul>
			<li>{{ucfirst($brand->brand)}}</li>
			<li>{{ucfirst($industry->industry)}}</li>
			<li>{{ucfirst($compaignType->compaign)}}</li>
			<li>{{ucfirst($persona->persona_title)}}</li>
		</ul>
	</div>

	<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id)}}">Compaign Overview</a>
	<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id)}}">Participation</a>
	<a href="{{url('backoffice/insights/compaign_participants/'.$persona->_id)}}">Participants</a>
	<a href="{{url('backoffice/insights/insight_comparison')}}">Comparison</a>
	
	<div class="social_list">
		<ul class="nav nav-pills nav-justified">
			<li class="nav-item {{ request()->is('backoffice/insights/compaign_overview/'.$persona->_id.'/facebook') ? 'active' : '' }}">
				<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id.'/facebook')}}" class="nav-link">Facebook</a>
			</li>
			<li class="nav-item {{ request()->is('backoffice/insights/compaign_overview/'.$persona->_id.'/twitter') ? 'active' : '' }}">
				<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id.'/twitter')}}" class="nav-link">Twitter</a>
			</li>
			<li class="nav-item {{ request()->is('backoffice/insights/compaign_overview/'.$persona->_id.'/linkdin') ? 'active' : '' }}">
				<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id.'/linkdin')}}" class="nav-link">Linkedin</a>
			</li>
			<li class="nav-item {{ request()->is('backoffice/insights/compaign_overview/'.$persona->_id.'/instagram') ? 'active' : '' }}">
				<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id.'/instagram')}}" class="nav-link">Instagram</a>
			</li>
			<li class="nav-item {{ request()->is('backoffice/insights/compaign_overview/'.$persona->_id.'/google') ? 'active' : '' }}">
				<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id.'/google')}}" class="nav-link">Google</a>
			</li>
			<li class="nav-item {{ request()->is('backoffice/insights/compaign_overview/'.$persona->_id.'/youtube') ? 'active' : '' }}">
				<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id.'/youtube')}}" class="nav-link">Youtube</a>
			</li>
		</ul>
	</div>
	
	<div class="row" style="margin-top:20px;">
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-clock-o"></i>
				</div>
				<div class="content">
					<p>Lifetime Visitors</p>
					<h2>{{$lifetimeVisitors}}</h2>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-calendar" style="color:#886BFF;"></i>
				</div>
				<div class="content">
					<p>Month Wise Visitors</p>
					<h2>{{$monthlyVisitors}}</h2>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-calendar" style="color:#FABC01;"></i>
				</div>
				<div class="content">
					<p>Week Wise Visitors</p>
					<h2>{{$weeklyVisitors}}</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-sm-12">
		<div class="camp_chart">
			<div class="camp_head">
				<div class="form-group">
					<input type="text" class="form-control getMonthlyGraph" placeholder="From Date" id="from_date" autocomplete="off"> 
				</div>
				<div class="form-group">
					<input type="text" class="form-control getMonthlyGraph" placeholder="To Date" id="to_date" autocomplete="off"> 
				</div>
				<div class="export_btn">
					<!-- <a href="javascript:void();">Export</a> -->
					<select class="form-control graphType">
						<option value="">Select graph type...</option>
						<option value="Line">Line Graph</option>
						<option value="Bar">Bar Graph</option>
					</select>
				</div>
				
			</div>
			<div class="monthly_graph"> 
				<div id="monthly_container"></div> 
				<div id="monthly_bar_container" style="display:none"></div> 
			</div>
		</div>
	</div>
	
</div>

 <!-- <div class="text-left outer_title">
	<h2>Campaign Dashboard</h2>
</div>

<div class="row">
	<div class="col-lg-3 col-md-4">
		<ul class="nav insight_tabs">
			<li class="nav-item active">
				<a href="{{url('backoffice/insights/compaign_overview/'.$pId)}}" class="nav-link">Overview</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_participation/'.$pId)}}" class="nav-link">Participation</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_participants/'.$pId)}}" class="nav-link">Participants</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/insight_comparison')}}" class="nav-link">Comparison</a>
			</li>
		</ul>	
		<ul class="nav social_tabs">
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_overview/'.$pId)}}" class="nav-link" style="background:#AC4AB8;"> All</a>
			</li>
			<li class="nav-item">
				<a href="{{url('backoffice/insights/compaign_overview/'.$pId.'?type=web')}}" class="nav-link" style="background:#D54316;"> Web</a>
			</li>
			<li class="nav-item">
				<a href="javascript:void();" class="nav-link" style="background:#3949AA;"><i class="fa fa-facebook"></i> Facebook</a>
			</li>
			<li class="nav-item">
				<a href="javascript:void();" class="nav-link" style="background:#43A3F9;"><i class="fa fa-twitter"></i> Twitter</a>
			</li>
						
		</ul>
	</div>
	<div class="col-lg-9 col-md-8">
		<div class="insight_body">
			<h3 class="title">Category : {{ucfirst($brand->brand)}}</h3>
			 <div class="filter">
				<select>
					<option value="0">Data Type</option>
					<option value="month">Month Wise</option>
					<option value="week">Week Wise</option>
				</select>
			</div> 	
			<div class="insignt_visiter">
				<div class="row">
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="widget">
							<div class="widget-advanced">
								<div class="widget-header text-center themed-background-dark">
									<h3 class="widget-content-light">
										<a href="javascript:void();" class="themed-color">Lifetime visitors</a>
										<small>{{$lifetimeVisitors}}</small>
									</h3>
								</div>
								<div class="widget-main">
									<a href="javascript:void();" class="widget-image-container animation-hatch">
										<img src="{{asset('/admin/img/visitor.png')}}" class="widget-image img-circle">
									</a>
									<h3>Trend Chart</h3>
									<div id="container"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="widget">
							<div class="widget-advanced">
								<div class="widget-header text-center themed-background-dark">
									<h3 class="widget-content-light">
										<a href="javascript:void();" class="themed-color">Monthly visitors</a>
										<small>{{$monthlyVisitors}}</small>
									</h3>
								</div>
								<div class="widget-main">
									<a href="javascript:void();" class="widget-image-container animation-hatch">
										<img src="{{asset('/admin/img/visitor.png')}}" class="widget-image img-circle">
									</a>
									<h3>Trend Chart</h3>
									<div id="monthly_container"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="widget">
							<div class="widget-advanced">
								<div class="widget-header text-center themed-background-dark">
									<h3 class="widget-content-light">
										<a href="javascript:void();" class="themed-color">Weekly visitors</a>
										<small>{{$weeklyVisitors}}</small>
									</h3>
								</div>
								<div class="widget-main">
									<a href="javascript:void();" class="widget-image-container animation-hatch">
										<img src="{{asset('/admin/img/visitor.png')}}" class="widget-image img-circle">
									</a>
									<h3>Trend Chart</h3>
									<div id="weekly_container"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>  -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>



/*=============Lifetime visitors============*/

	/*Highcharts.chart('container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            pointStart: <?php //echo $yearlyPointStart;?>
	        }
	    },

	    series: [{
	        name: 'Lifetime Visitors',
	        data: [<?php //echo implode(",",$yearly_record); ?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});*/

/*=============Monthly visitors============*/

	Highcharts.chart('monthly_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            
	        }
	    },
	    chart: {
	        type: 'area'
	    },
	    title: {
	        text: 'Monthly Visitors'
	    },
	    xAxis: {
               categories: [<?php echo "'".$currentMonthsDate."'";?>],
            },

	    series: [{
	        name: 'Monthly Visitors',
	        data: [<?php echo implode(",",$monRecords);?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});

	Highcharts.chart('monthly_bar_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            
	        }
	    },
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Monthly Visitors'
	    },
	    xAxis: {
               categories: [<?php echo "'".$currentMonthsDate."'";?>],
            },

	    series: [{
	        name: 'Monthly Visitors',
	        data: [<?php echo implode(",",$monRecords);?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});

/*=============Weekly visitors============*/

	/*Highcharts.chart('weekly_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            pointStart: <?php //echo $yearlyPointStart;?>
	        }
	    },

	    series: [{
	        name: 'Weekly Visitors',
	        data: [<?php //echo $yearlyPointStart; ?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});
*/
/*=============Weekly interactions============*/

	/*Highcharts.chart('weekly_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            
	        }
	    },
	    xAxis: {
               categories: [<?php //echo "'".$currentWeek."'";?>],
            },

	    series: [{
	        name: 'Weekly Visitors',
	        data: [<?php //echo implode(",",$weekly_record);?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});*/

</script>

@endsection

