@extends('layouts.admin')

@section('content')

<div class="text-left outer_title">
	<h2>Campaign details</h2>
</div>

<div class="row">

	<a href="{{url('backoffice/insights/compaign_overview/'.$persona->_id)}}">Compaign Overview</a>
	<a href="{{url('backoffice/insights/compaign_participation/'.$persona->_id)}}">Participation</a>
	<a href="{{url('backoffice/insights/compaign_participants/'.$persona->_id)}}">Participants</a>
	<a href="{{url('backoffice/insights/insight_comparison')}}">Comparison</a>
	
	<div class="col-lg-9 col-md-8">
		<div class="insight_body">
			<div class="participant_info">
				<h4>Lead Generation Report</h4>
				<p><b>Campaign Category:</b>{{ucfirst($compaign->compaign)}}</p>
				<p><b>Campaign id:</b>{{ucfirst($persona->persona_title)}}</p>
			</div>
			<div class="block full"> 
				<div class="block-title">
					<h2><strong>Client : {{ucfirst($brand->brand)}}</strong></h2>
				</div> 
				<div class="table-responsive">
					<table class="table table-vcenter table-condensed table-bordered table_design">
						<thead>
							<tr>
							   <th class="text-center">S.No</th>               
							   <th class="text-center">Email</th>
							   <th class="text-center">Mobile</th>
							   <th class="text-center">Appointment</th>
							   <th class="text-center">Channel</th>
							   <th class="text-center">FB/Twitter<br>Handle</th>
							   <th class="text-center">Last Login</th>
							   <th class="text-center">Login Time</th>
							   <th class="text-center">Replies</th>
							   <th class="text-center">Conversation History</th>
							   <th class="text-center">Score</th>
							   <th class="text-center">Clicks</th>
							</tr>
						</thead>
						<tbody>
							@php $i = 1; @endphp
							@foreach($personaChat as $val)
								<tr>
									<td class="text-center">{{$i}}</td>
									<td class="text-center">
										@if($val->email != '')
											{{$val->email}}
										@else
											{{'NA'}}
										@endif
									</td>
									<td class="text-center">{{'NA'}}</td>
									<td class="text-center">Appointment</td>
									<td class="text-center">{{$val->personaDetail->channel}}</td>
									<td class="text-center">FB/Twitter<br>Handle</td>
									<td class="text-center">{{date('m-d-Y',strtotime($val->chatMsg[0]->created_at))}}</td>
									<td class="text-center">{{date('H:i:s',strtotime($val->chatMsg[0]->created_at))}}</td>
									<td class="text-center">{{count($val->chatMsg)}}</td>
									<td class="text-center"><a href="{{url('persona/persona_chat/'.$val->_id)}}">Conversation History</a></td>
									<td class="text-center">{{$val->userChatMsg->sum('score')}}</td>
									<td class="text-center">{{$val->clicks}}</td>
								</tr>
								@php $i++ @endphp
							@endforeach
						</tbody>	
					</table>
			</div>
			</div>				
		</div>
	</div>
</div>

@endsection