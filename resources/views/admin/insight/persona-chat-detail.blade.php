@extends('layouts.admin')

@section('content')
<?php use App\FormSubmissions; ?>

	<div class="container">

    <h2>Chat</h2>

    <br/>

    @if (\Session::has('success'))

      <div class="alert alert-success">

        <p>{{ \Session::get('success') }}</p>

      </div><br />

    @endif
	<div class="block full">
		<div class="block-title">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
			<?php
				echo '<li class="nav-item active">
						<a href="#chat1" class="nav-link active" data-toggle="tab">'.$chatDetail->personaDetail['persona_title'].'</a>
					</li>';
			?>
			</ul>
		</div>
		<div class="tab-content" id="myTabContent">
			<?php
				
					
					
						echo '<div class="tab-pane fade active in" id="chat1">
							<div class="chat_tabbox">';

						foreach($chatDetail->userChatMsg as $val)
						{
							
							
							if($val['response'] != '')
				    		{
				    			$personaContent = json_decode($val['response']);
				    		}
	    		
				    		$userContent = json_decode($val['user_msg']);

				    		if($userContent != '')
				    		{
				    			echo '<div id="msg-text-1" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="media-body"><small>'.date('H:i',strtotime($val['updated_at'])).'</small><div class="chat_txt bg_brown">'.$userContent->msg.'</div></div></div></div>';
				    		}

				    		if($val->response != '')
				    		{
				    			/*====If type radio===*/

				    			if($personaContent->type == 'Radio')
					    		{
					    			$answers = json_decode($personaContent->answer);

									$retStr = '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$chatDetail->personaDetail['persona_image']).'"></div><div class="media-body"><small>'.date('H:i',strtotime($val->updated_at)).'</small><div class="chat_txt bg_light">'.$personaContent->msg.'</div>';
					    			$retStr .= '<div class="chat_btn">';
									foreach($answers as $options)
									{

										if(isset($personaContent->selected))
										{
											if($options == $personaContent->selected)
											{
												$sel = "checked='checked'";
											}
											else
											{
												$sel = '';
											}
										}
										else
										{
											$sel = '';
										}
										$retStr .= '<input type="radio" class="bg_orange" name="'.$personaContent->content_id.'" onclick="chatBackupTry()" value="'.$options.'" '.$sel.' disabled>'.$options.'<br>';
									}
									$retStr .= '</div></div></div></div>';
									echo $retStr;
		    					}

		    					/*====If type file====*/

					    		else if($personaContent->type == 'File')
					    		{
					    			$imagePath = asset($personaContent->msg);
									echo '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$chatDetail->personaDetail['persona_image']).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$personaContent->answer.'</div><div class="attachment">
									<img src="'.$imagePath.'" style="hieght:150px;width:150px" class="file_img" /></div></div></div></div>';
									

					    		}
					    		else if($personaContent->type == 'Form')
								{

									$formData = FormSubmissions::where('contentId',$personaContent->content_id)->first();
									$formItems = json_decode($personaContent->answer);
									$htmlstr = '';
										    		
									$htmlstr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$personaContent->content_id.'">';
									
									foreach($formItems as $formItem)
									{
										$getVal = '';
										$readonly = '';
										$disabled = '';
										if($formData != '')
										{
											$decodeFormVal = json_decode($formData->formData);
											$formVal = (array)$decodeFormVal;
										}
															
										if($formItem->type =='textarea')
										{ 
											if($formData != '')
											{
												$getVal =  $formVal[$formItem->name];
												$readonly = 'readonly';
											}
											$htmlstr.= '<div class="col-lg-6">
												<textarea name="'.$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" '.$readonly.'>'.$getVal.'</textarea>
												</div>';
										}
										else if($formItem->type =='checkbox-group')
										{
											if($formData != '')
											{
												$getVal =  $formVal[$formItem->name];
												$disabled = 'disabled';
											} 
											$htmlstr.= '<div class="col-lg-6">';
											foreach($formItem->values as $val)
											{
												
												if($val->value == $getVal)
												{
													$sel = "selected='selected'";
												}
												else
												{
													$sel ='';
												}
												
												$htmlstr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'" '.$sel.'>'.$val->value;
											}
											$htmlstr.= '</div>';
										}
										else if($formItem->type =='radio-group')
										{ 
											if($formData != '')
											{
												$getVal =  $formVal[$formItem->name];
												$disabled = 'disabled';
											}
											$htmlstr.= '<div class="col-lg-6">';
											foreach($formItem->values as $val)
											{
												if($val->value == $getVal[0])
												{
													$sel = "checked='checked'";
												}
												else
												{
													$sel ='';
												}
												$htmlstr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'" '.$sel.'>'.$val->value;
											}
											$htmlstr .= '</div>';
										}
										else if($formItem->type =='select')
										{ 
											if($formData != '')
											{
												$getVal =  $formVal[$formItem->name];
												$disabled = 'disabled';
											}
											$htmlstr.= '<div class="col-lg-6">
												<select class="form-control mr-3 '.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required '.$disabled.'>';
											foreach($formItem->values as $slectOptions)
											{
												if($slectOptions->value == $getVal)
												{
													$sel = "selected='selected'";
												}
												else
												{
													$sel ='';
												}
												
												$htmlstr.= '<option value="'.$slectOptions->value .'" '.$sel.'>'.$slectOptions->label .'</option>';
											}
											$htmlstr.='</select></div>';
										}
										else if($formItem->type =='text')
										{
											if($formData != '')
											{
												$getVal =  $formVal[$formItem->name];
												$readonly = 'readonly';
											} 
											$htmlstr.= '<div class="col-lg-6">
												<input type="text" name="' .$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" value="'.$getVal.'" '.$readonly.' placeholder="Text">
											</div>';
											
										}
										else if($formItem->type =='number')
										{ 
											if($formData != '')
											{
												$getVal =  $formVal[$formItem->name];
												$readonly = 'readonly';
											}
											$htmlstr.= '<div class="col-lg-6">
												<input type="number" name="'. $formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" value="'.$getVal.'" '.$readonly.'>
											</div>';
										}
										else if($formItem->type =='date')
										{
											if($formData != '')
											{
												$getVal =  $formVal[$formItem->name];
												$readonly = 'readonly';
											}
											$htmlstr.= '<div class="col-lg-6">
												<input type="date" name="'.$formItem->name .'" id="'.$formItem->name.'" class="'. $formItem->className .'" value="'.$getVal.'" '.$readonly.'>
											</div>';
										}
										else if($formItem->type =='file')
										{
											$htmlstr.= '<div class="col-lg-12"><div class="file_upload"><span id="result_name"></span> <i class="fa fa-cloud-upload"></i>
												<input type="file" id="upload_input" name="'.$formItem->name .'[]" id="'.$formItem->name.'" class="'.$formItem->className .'" multiple>
											</div>';

											if($formData != '')
											{

												$htmlstr .= '<div class="attachment">';
													foreach($formVal['upload-'.$formItem->name] as $val)
													{
														$htmlstr .= '<img src="'.asset('uploads/formFiles/'.$val).'" class="file_img">';
													}
												'</div>';
												
											}
											$htmlstr .= '</div></div>';
										}
										else if($formItem->type =='button')
										{ 
											if($formData != '')
											{
												$disabled = 'disabled';
											}
											$formId = 'form_'.$personaContent->content_id;
											$htmlstr.= '<div class="chat_btn mt-2 text-right">
												<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="bg_orange '. $formItem->className .'" onclick="submitForm('.$formId.')" '.$disabled.'>
												</div>';
												
										}
										
									}
									echo '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><img class="fox_icon" src="'.asset('uploads/user-images/'.$chatDetail->personaDetail['persona_image']).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light"></div><div class="form mt-2"><div class="row">'.$htmlstr.'</div></div></div></div>';
								}

					    		else
					    		{

					    			echo '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$chatDetail->personaDetail['persona_image']).'"></div><div class="media-body"><small>'.date('H:i',strtotime($val['updated_at'])).'</small><div class="chat_txt bg_lights">'.$personaContent->msg.'</div></div></div></div>';

					    		}
	    					}
	    					
	    				}
	    				echo '</div>
							</div>';
					
						
				
			?>
		</div>
	</div>
	<table class="table table-striped" style="display:none;">
		<thead>
			<tr>
		        <th>User Message</th>
				<th>Persona Message</th>
      		</tr>
		</thead>
		<tbody>
			
		</tbody>
	</table>
</div>

@endsection

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>







