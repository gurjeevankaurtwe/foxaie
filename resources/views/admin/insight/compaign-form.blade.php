@extends('layouts.admin')

@section('content')

<div class="row" style="display:none;">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="block">
			<div class="block-title">
				<h2><strong>Build Campaigns Form</strong></h2>
			</div>
			<form action="{{url('backoffice/insights/compaign_overview')}}" method="post" class="form-horizontal form-bordered" id="insight_form">
				@csrf

				<input type="hidden" class="persona_brands" name="brandId">
				<input type="hidden" class="persona_industries" name="personaId">
				<input type="hidden" class="persona_compaigns" name="compaignId">
				<input type="hidden" class="select_persona" name="personaId">
			</form>
		</div>
	</div>
</div>

<div class="select_brand_wrapper" style="display:block;">
	
	<div class="text-left outer_title">
		<h2>Brands</h2>
		<ul>
			<li class="selectedBrand"></li>
		</ul>
	</div>

	<div class="block select_brand">
		<div class="block-title">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="form-group">
					<input type="text" class="form-control search_brand_list" placeholder="Search Brand">
					<button class="search_btn"><i class="fa fa-search"></i></button>	
				</div>
			</div>		
		</div>
		<div class="row brandList">
			@foreach($brands as $val)
				<div class="col-sm-3">
					<a href="javascript:void();" data-attr="{{$val->_id}}" data-target="{{ucfirst($val->brand)}}" class="brand_link choose_brand">
						<div class="brand_box">
							<div class="brand_logo">
								<img src="{{asset('/uploads/user-images/'.$val->brand_image)}}" alt="logo">
							</div>
							<div class="brand_txt">
								<p>{{ucfirst($val->brand)}}</p>
							</div>
						</div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
</div>

<div class="select_industry_wrapper" style="display:none;">
	
	<div class="text-left outer_title">
		<h2>Industry</h2>
		<ul>
			<a href="javscript:void(0)"><li class="industryBack">Back</li></a>
			<li class="selectedBrand"></li>
			<li class="selectedIndustry"></li>
		</ul>
	</div>
	
	<div class="block select_brand">		
		<div class="row">
			@foreach($industry as $ind)
				<div class="col-sm-3">
					<a href="javascript:void();" data-attr="{{$ind->_id}}" data-target="{{ucfirst($ind->industry)}}" class="brand_link choose_industry">
						<div class="brand_box">
							<div class="brand_logo">
								<img src="{{asset('/admin/img/insignt/marketing.png')}}" style="width:60px;" alt="logo">
							</div>
							<div class="brand_txt">
								<p>{{ucfirst($ind->industry)}}</p>
							</div>
						</div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
</div>

<div class="select_camptype_wrapper" style="display:none;">
	
	<div class="text-left outer_title">
		<h2>Campaigns Type</h2>
		<ul>
			<a href="javscript:void(0)"><li class="compTypeBack">Back</li></a>
			<li class="selectedBrand"></li>
			<li class="selectedIndustry"></li>
			<li class="selectedCompType"></li>
		</ul>
	</div>
	
	<div class="row personaCompType"></div>
	
</div>

<div class="select_camplist_wrapper" style="display:none;">
	
	<div class="text-left outer_title">
		<h2>Campaigns list</h2>
		<ul>
			<a href="javscript:void(0)"><li class="compListBack">Back</li></a>
			<li class="selectedBrand"></li>
			<li class="selectedIndustry"></li>
			<li class="selectedCompType"></li>
			<li class="selectedCompList"></li>
		</ul>
	</div>
	
	<div class="row personasTitle"></div>
</div>

<div class="select_camplists_wrapper" style="display:none;">
	
	<div class="text-left outer_title">
		<h2>Campaigns details</h2>
		<ul>
			<li>Nokia</li>
			<li>Marketing</li>
			<li>Awareness</li>
		</ul>
	</div>
	
	<div class="social_list">
		<ul class="nav nav-pills nav-justified">
			<li class="nav-item">
				<a href="javascript:void();" class="nav-link">Facebook</a>
			</li>
			<li class="nav-item">
				<a href="javascript:void();" class="nav-link">Twitter</a>
			</li>
			<li class="nav-item">
				<a href="javascript:void();" class="nav-link">Linkedin</a>
			</li>
			<li class="nav-item">
				<a href="javascript:void();" class="nav-link">Instagram</a>
			</li>
			<li class="nav-item">
				<a href="javascript:void();" class="nav-link">Google</a>
			</li>
			<li class="nav-item">
				<a href="javascript:void();" class="nav-link">Youtube</a>
			</li>
		</ul>
	</div>
	
	<div class="row" style="margin-top:20px;">
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-clock-o"></i>
				</div>
				<div class="content">
					<p>Lifetime Visitors</p>
					<h2>200K</h2>
					<span>111.20%</span>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-calendar" style="color:#886BFF;"></i>
				</div>
				<div class="content">
					<p>Month Wise Visitors</p>
					<h2>50K</h2>
					<span style="color:red;">80.20%</span>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="camp_widgetbox">
				<div class="icon">
					<i class="fa fa-calendar" style="color:#FABC01;"></i>
				</div>
				<div class="content">
					<p>Week Wise Visitors</p>
					<h2>10K</h2>
					<span>111.20%</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-sm-12">
		<div class="camp_chart">
			<div class="camp_head">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="From Date"> 
				</div>
				<div class="form-group">
					<input type="text" class="form-control" placeholder="To Date"> 
				</div>
				<div class="export_btn">
					<a href="javascript:void();">Export</a> 
				</div>
			</div>
		</div>
	</div>
	
</div>

@endsection
