<?php 
	
	/*===========Yearly record=========*/

	/*$yearly_record = array();
	foreach($yearlyRecord as $key=>$yearly)
	{
		$yearly_record[$key] =  count($yearly);
	}
	ksort($yearly_record);
	$yearlyPointStart = key($yearly_record);*/

	/*===========Monthly Record==========*/

	$monthly_record = array();
	$monRecords = array();
	foreach($monthlyRecord as $key=>$val)
	{
		$monthly_record[date('Y-m-d',strtotime($val->created_at))][] = $val->_id;
	}
	foreach($monthly_record as $key=>$monRec)
	{
		$monRecords[$key] = count($monRec);
	}
	ksort($monRecords);
	$currentMonthsDate = implode("','", array_map(
	    function ($v, $k)
	    { 
	    	return sprintf("%s", $k, $v); 
	    },
	    $monRecords,
	    array_keys($monRecords)
	));

/*===========Weekly Record========*/

	/*$weekly_record = array();

	foreach($weeklyRecord as $key=>$val)
	{
		$weekly_record[$key] = count($val);
	}
	ksort($weekly_record);
	$currentWeek = implode("','", array_map(
	    function ($v, $k)
	    { 
	    	return sprintf("%s", $k, $v); 
	    },
	    $weekly_record,
	    array_keys($weekly_record)
	));*/
	
	
?>


	
	<div id="monthly_container"></div> 
	
		

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>


/*=============Monthly visitors============*/

	Highcharts.chart('monthly_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            
	        }
	    },
	    xAxis: {
               categories: [<?php echo "'".$currentMonthsDate."'";?>],
            },

	    series: [{
	        name: 'Monthly Visitors',
	        data: [<?php echo implode(",",$monRecords);?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});


</script>


