@extends('layouts.admin')

@section('content')


	<div class="text-left outer_title">
		<h2>Brands</h2>
	</div>
	
	<div class="insight_bnr">
		<img src="{{asset('uploads/user-images/'.Auth::user()->banner_image)}}" alt="banner"> 
	</div>
	
	<div class="row" style="margin-top:20px;">
		<div class="row">
			<div class="col-sm-12 col-lg-6">
				<a href="javascript:void();" class="widget widget-hover-effect1 insight_widget" data-toggle="modal" data-target="#add-compaign">
					<div class="widget-simple">
						<div class="widget-icon themed-background-autumn animation-fadeIn">
							<i class="fa fa-file-text"></i>
						</div>
						<div class="widget-content animation-pullDown">
							<h3><strong>Build Campaigns</strong></h3>
						</div>						
					</div>
				</a>
			</div>
			<div class="col-sm-12 col-lg-6">
				<a href="{{url('backoffice/insights/comapign_form')}}" class="widget widget-hover-effect1 insight_widget">
					<div class="widget-simple">
						<div class="widget-icon themed-background-spring animation-fadeIn">
							<i class="fa fa-file-text"></i>
						</div>
						<div class="widget-content animation-pullDown">
							<h3><strong>View Insights</strong></h3>
						</div>						
					</div>
				</a>
			</div>				
		</div>
	</div>	

@endsection

<!-- Add Compaign Modal -->

<div id="add-compaign" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Add Brand Compaign</h4>

      </div>

      <div class="modal-body">

        <form method="post" action="{{url('backoffice/persona/add_compaign')}}">

			  @csrf


			<div class="row">

  				<div class="col-md-4"></div>

  				<div class="form-group col-md-4">

  					<label for="status">Brand:</label>

  					 <select class="form-control" name="brandId" required>

  						<option value="">Select Brand...</option>

  						@foreach($brands as $val)
  							<option value="{{$val->_id}}">{{$val->brand}}</option>
  						@endforeach

  					</select>

  				</div>

  			</div>

        
  			<div class="row">

  			  <div class="col-md-4"></div>

  			  <div class="form-group col-md-4">

  				<label for="industry">Compaign:</label>

  				<input type="text" class="form-control" required name="compaign">

  			  </div>

  			</div>

  			<div class="row">

  				<div class="col-md-4"></div>

  				<div class="form-group col-md-4">

  					<label for="status">Status:</label>

  					 <select class="form-control" name="status" required>

  						<option value="A">Active</option>

  						<option value="I">In-Active</option>

  					</select>

  				</div>

  			</div>

  			<div class="row">

  			  <div class="col-md-4"></div>

  			  <div class="form-group col-md-4">

  				<button type="submit" class="btn btn-success">Submit</button>

  			  </div>

  			</div>

		  </form>

      </div>

    </div>
  
  </div>

</div>
