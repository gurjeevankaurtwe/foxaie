@extends('layouts.admin')

@section('content')

<?php 
	
	/*===========Persona 1 Monthly Record==========*/

	if($persona1Detail != '')
	{
		$per1monthly_record = array();
		$per1monRecords = array();
		$per1currentMonth = date('m');
		$per1currentMonthDate = '1';
		
		foreach($per1MonthlyRecord as $key=>$val)
		{
			if($key == $per1currentMonth)
			{
				foreach($val as $per1month)
				{

					$per1monthly_record[date('Y-m-d',strtotime($per1month->created_at))][] = $per1month->_id;
				}
			}
		}
		foreach($per1monthly_record as $key=>$per1monRec)
		{
			$per1monRecords[$key] = count($per1monRec);
		}
		
		$per1Date1 = date('01-m-Y'); 
		$per1Date2 = date('t-m-Y');
		$per1array = array(); 
	  
		$per1Variable1 = strtotime($per1Date1); 
		$per1Variable2 = strtotime($per1Date2); 
	  
		for ($currentDate = $per1Variable1; $currentDate <= $per1Variable2;  $currentDate += (86400)) 
		{ 
			foreach($per1monRecords as $key=>$val)
			{
				if($key == date('Y-m-d', $currentDate))
				{
					$per1array[date('Y-m-d', $currentDate)] = $val;
				}
				else
				{
					$per1array[date('Y-m-d', $currentDate)] = "''";
				}
			}
		} 
		$per1currentMonthsDate = implode("','", array_map(
		    function ($v, $k)
		    { 
		    	return sprintf("%s", $k, $v); 
		    },
		    $per1array,
		    array_keys($per1array)
		));
	}

		

	/*===========Persona 2 Monthly Record==========*/

	if($persona2Detail != '')
	{

		$per2monthly_record = array();
		$per2monRecords = array();
		$per2currentMonth = date('m');
		$per2currentMonthDate = '1';
		
		foreach($per2MonthlyRecord as $key=>$val)
		{
			if($key == $per2currentMonth)
			{
				foreach($val as $per2month)
				{

					$per2monthly_record[date('Y-m-d',strtotime($per2month->created_at))][] = $per2month->_id;
				}
			}
		}
		foreach($per2monthly_record as $key=>$per2monRec)
		{
			$per2monRecords[$key] = count($per2monRec);
		}
		
		$per2Date1 = date('01-m-Y'); 
		$per2Date2 = date('t-m-Y');
		$per2array = array(); 
	  
		$per2Variable1 = strtotime($per2Date1); 
		$per2Variable2 = strtotime($per2Date2); 
	  
		for ($currentDate = $per2Variable1; $currentDate <= $per2Variable2;  $currentDate += (86400)) 
		{ 
			foreach($per2monRecords as $key=>$val)
			{
				if($key == date('Y-m-d', $currentDate))
				{
					$per2array[date('Y-m-d', $currentDate)] = $val;
				}
				else
				{
					$per2array[date('Y-m-d', $currentDate)] = "''";
				}
			}
		} 
	}

	/*===========Persona 3 Monthly Record==========*/

	if($persona1Detail != '')
	{
		$per3monthly_record = array();
		$per3monRecords = array();
		$per3currentMonth = date('m');
		$per3currentMonthDate = '1';
		
		foreach($per3MonthlyRecord as $key=>$val)
		{
			if($key == $per3currentMonth)
			{
				foreach($val as $per3month)
				{

					$per3monthly_record[date('Y-m-d',strtotime($per3month->created_at))][] = $per3month->_id;
				}
			}
		}
		foreach($per3monthly_record as $key=>$per3monRec)
		{
			$per3monRecords[$key] = count($per3monRec);
		}
		
		$per3Date1 = date('01-m-Y'); 
		$per3Date2 = date('t-m-Y');
		$per3array = array(); 
	  
		$per3Variable1 = strtotime($per3Date1); 
		$per3Variable2 = strtotime($per3Date2); 
	  
		for ($currentDate = $per3Variable1; $currentDate <= $per3Variable2;  $currentDate += (86400)) 
		{ 
			foreach($per3monRecords as $key=>$val)
			{
				if($key == date('Y-m-d', $currentDate))
				{
					$per3array[date('Y-m-d', $currentDate)] = $val;
				}
				else
				{
					$per3array[date('Y-m-d', $currentDate)] = "''";
				}
			}
		} 
	}

	

	/*===========Persona 4 Monthly Record==========*/

	if($persona4Detail != '')
	{

		$per4monthly_record = array();
		$per4monRecords = array();
		$per4currentMonth = date('m');
		$per4currentMonthDate = '1';
		
		foreach($per4MonthlyRecord as $key=>$val)
		{
			if($key == $per4currentMonth)
			{
				foreach($val as $per4month)
				{

					$per4monthly_record[date('Y-m-d',strtotime($per4month->created_at))][] = $per4month->_id;
				}
			}
		}
		foreach($per4monthly_record as $key=>$per4monRec)
		{
			$per4monRecords[$key] = count($per4monRec);
		}
		
		$per4Date1 = date('01-m-Y'); 
		$per4Date2 = date('t-m-Y');
		$per4array = array(); 
	  
		$per4Variable1 = strtotime($per4Date1); 
		$per4Variable2 = strtotime($per4Date2); 
	  
		for ($currentDate = $per4Variable1; $currentDate <= $per4Variable2;  $currentDate += (86400)) 
		{ 
			foreach($per4monRecords as $key=>$val)
			{
				if($key == date('Y-m-d', $currentDate))
				{
					$per4array[date('Y-m-d', $currentDate)] = $val;
				}
				else
				{
					$per4array[date('Y-m-d', $currentDate)] = "''";
				}
			}
		}
	} 
	

?>

<div class="container-fluid">
	
	<div class="text-left outer_title">
		<h2>Compare Persona</h2>
	</div>

	<form method="POST"  action="{{url('backoffice/insights/insight_comparison_list')}}">

		@csrf

		<input type="hidden" class="persona1" <?php if($persona1Detail != ''){echo 'value="'.$persona1Detail->_id.'"'; } ?>>
		<input type="hidden" class="persona2" <?php if($persona2Detail != ''){echo 'value="'.$persona2Detail->_id.'"'; } ?>>
		<input type="hidden" class="persona3" <?php if($persona3Detail != ''){echo 'value="'.$persona3Detail->_id.'"'; } ?>>
		<input type="hidden" class="persona4" <?php if($persona4Detail != ''){echo 'value="'.$persona4Detail->_id.'"'; } ?>>
		<input type="hidden" class="brand" <?php if($brands != ''){echo 'value="'.$brandId.'"'; } ?>>
	
		<div class="row">
			<div class="col-lg-4 col-sm-6">
				<div class="form-group comp_brand">
					<select class="form-control get-persona" name="brandId" id="persona_brands" required>
						<option value="">Select Brand</option>
						@foreach($brands as $val)
							@if($brandId != '')
								@if($val->_id == $brandId)
									@php $sel = "selected='selected'"; @endphp
								@else
									@php $sel = ''; @endphp
								@endif
							@else
								@php $sel = ''; @endphp
							@endif
							<option value="{{$val->_id}}" {{$sel}}>{{ucfirst($val->brand)}}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
	
		<div class="row">
			<div class="col-lg-3 col-sm-6">
				<div class="compare_box">
					<div class="addCricle">
						<img src="{{asset('admin/img/plus.png')}}">
					</div>
					<div class="selectBox">
						<div class="form-group">
							<select class="form-control get-persona" name="industry" id="persona_industries" required>
								<option value="">Select Industry</option>
								@foreach($industry as $ind)
									@if($persona1Detail != '')
										@if($persona1Detail->industry == $ind->_id)
											@php $sel = "selected='selected'"; @endphp
										@else
											@php $sel = ''; @endphp
										@endif
									@else
										@php $sel = ''; @endphp
									@endif

									<option value="{{$ind->_id}}" {{$sel}}>{{ucfirst($ind->industry)}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<select class="form-control get-persona persona_compaign" name="compaign" id="persona_compaigns" required>
								<option value="">Campaign Category</option>
								@if(count($compaign) > 0)
									@foreach($compaign as $comp)
										@if($comp->_id == $persona1Detail->compaign)
											@php $sel = "selected='selected'"; @endphp
										@else
											@php $sel = ''; @endphp
										@endif
										<option value="{{$comp->_id}}" {{$sel}}>{{ucfirst($comp->compaign)}}</option>
									@endforeach
								@endif
								
							</select>
						</div>
						<div class="form-group">
							<select class="form-control persona_one_title" id="select_persona" name="persona_id" required>
								<option value="">Persona Name</option>
								@if(count($personas1) > 0)
									@foreach($personas1 as $per)
										@if($persona1Detail != '')
											@if($per->_id == $persona1Detail->_id)
												@php $sel = "selected='selected'"; @endphp
											@else
												@php $sel = ''; @endphp
											@endif
										@else
											@php $sel = ''; @endphp
										@endif
										<option value="{{$per->_id}}" {{$sel}}>{{ucfirst($per->persona_title)}}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6">
				<div class="compare_box">
					<div class="addCricle">
						<img src="{{asset('admin/img/plus.png')}}">
					</div>
					<div class="selectBox">
						<div class="form-group">
							<select class="form-control get-persona2" name="industry2" id="persona_industries2" required <?php if($persona1Detail == ''){echo 'disabled'; } ?>>
								<option value="">Select Industry</option>
								@foreach($industry as $ind)
									@if($persona2Detail != '')
										@if($persona2Detail->industry == $ind->_id)
											@php $sel = "selected='selected'"; @endphp
										@else
											@php $sel = ''; @endphp
										@endif
									@else
										@php $sel = ''; @endphp
									@endif
									<option value="{{$ind->_id}}" {{$sel}}>{{ucfirst($ind->industry)}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<select class="form-control get-persona2 persona_compaign" name="compaign2" id="persona_compaigns2" required <?php if($persona1Detail == ''){echo 'disabled'; } ?>>
								<option value="">Campaign Category</option>
								@if(count($compaign) > 0)
									@foreach($compaign as $comp)
										@if($persona2Detail != '')
											@if($comp->_id == $persona2Detail->compaign)
												@php $sel = "selected='selected'"; @endphp
											@else
												@php $sel = ''; @endphp
											@endif
										@else
											@php $sel = ''; @endphp
										@endif
										
										<option value="{{$comp->_id}}" {{$sel}}>{{ucfirst($comp->compaign)}}</option>
									@endforeach
								@endif
							</select>
						</div>
						<div class="form-group">
							<select class="form-control persona_two_title" id="select_persona2" name="persona_id2" required <?php if($persona1Detail == ''){echo 'disabled'; } ?>>
								<option value="">Persona Name</option>
								@if(count($personas2) > 0)
									@foreach($personas2 as $per)
										@if($per->_id == $persona2Detail->_id)
											@php $sel = "selected='selected'"; @endphp
										@else
											@php $sel = ''; @endphp
										@endif
										<option value="{{$per->_id}}" {{$sel}}>{{ucfirst($per->persona_title)}}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 persona-three">
				<div class="compare_box">
					<div class="addCricle">
						<img src="{{asset('admin/img/plus.png')}}">
					</div>
					<div class="selectBox">
						<div class="form-group">
							<select class="form-control get-persona3" name="industry3" id="persona_industries3" required <?php if($persona2Detail == ''){echo 'disabled'; } ?>>
								<option value="">Select Industry</option>
								@foreach($industry as $ind)
									@if($persona3Detail != '')
										@if($persona3Detail->industry == $ind->_id)
											@php $sel = "selected='selected'"; @endphp
										@else
											@php $sel = ''; @endphp
										@endif
									@else
										@php $sel = ''; @endphp
									@endif
									<option value="{{$ind->_id}}" {{$sel}}>{{ucfirst($ind->industry)}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<select class="form-control get-persona3 persona_compaign" name="compaign3" id="persona_compaigns3" required <?php if($persona2Detail == ''){echo 'disabled'; } ?>>
								<option value="">Campaign Category</option>
								@if(count($compaign) > 0)
									@foreach($compaign as $comp)
										@if($persona3Detail != '')
											@if($comp->_id == $persona3Detail->compaign)
												@php $sel = "selected='selected'"; @endphp
											@else
												@php $sel = ''; @endphp
											@endif
										@else
											@php $sel = '';@endphp
										@endif
										<option value="{{$comp->_id}}" {{$sel}}>{{ucfirst($comp->compaign)}}</option>
									@endforeach
								@endif
							</select>
						</div>
						<div class="form-group">
							<select class="form-control persona_three_title" id="select_persona3" name="persona_id3" required <?php if($persona2Detail == ''){echo 'disabled'; } ?>>
								<option value="">Persona Name</option>
								@if(count($personas3) > 0)
									@foreach($personas3 as $per)
										@if($per->_id == $persona3Detail->_id)
											@php $sel = "selected='selected'"; @endphp
										@else
											@php $sel = ''; @endphp
										@endif
										<option value="{{$per->_id}}" {{$sel}}>{{ucfirst($per->persona_title)}}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 persona-four">
				<div class="compare_box">
					<div class="addCricle">
						<img src="{{asset('admin/img/plus.png')}}">
					</div>
					<div class="selectBox">
						<div class="form-group">
							<select class="form-control get-persona4" name="industry4" id="persona_industries4" required <?php if($persona3Detail == ''){echo 'disabled'; } ?>>
								<option value="">Select Industry</option>
								@foreach($industry as $ind)
									@if($persona4Detail != '')
										@if($persona4Detail->industry == $ind->_id)
											@php $sel = "selected='selected'"; @endphp
										@else
											@php $sel = ''; @endphp
										@endif
									@else
										@php $sel = ''; @endphp
									@endif
									<option value="{{$ind->_id}}" {{$sel}}>{{ucfirst($ind->industry)}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<select class="form-control get-persona4 persona_compaign" name="compaign4" id="persona_compaigns4" required <?php if($persona3Detail == ''){echo 'disabled'; } ?>>
								<option value="">Campaign Category</option>
								@if(count($compaign) > 0)
									@foreach($compaign as $comp)
										@if($persona4Detail != '')
											@if($comp->_id == $persona4Detail->compaign)
											@php $sel = "selected='selected'"; @endphp
											@else
												@php $sel = ''; @endphp
											@endif
										@else
											@php $sel = ''; @endphp
										@endif
										
										<option value="{{$comp->_id}}" {{$sel}}>{{ucfirst($comp->compaign)}}</option>
									@endforeach
								@endif
							</select>
						</div>
						<div class="form-group">
							<select class="form-control persona_four_title" id="select_persona4" name="persona_id4" required <?php if($persona3Detail == ''){echo 'disabled'; } ?>>
								<option value="">Persona Name</option>
								@if(count($personas4) > 0)
									@foreach($personas4 as $per)
										@if($per->_id == $persona4Detail->_id)
											@php $sel = "selected='selected'"; @endphp
										@else
											@php $sel = ''; @endphp
										@endif
										<option value="{{$per->_id}}" {{$sel}}>{{ucfirst($per->persona_title)}}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 text-center" style="margin-top:30px;margin-bottom:30px;">
				<a id="compare_persona" href="javascipt:void(0)">
					<input type="button"  class="btn btn-lg btn-primary" value="Compare Persona">
				</a>
			</div>			
		</div>
	</form>
	
	<div class="compare_info" <?php if($persona1Detail == '' && $persona2Detail == ''){ echo 'style="display:none"';}?>>
		
		<h3 class="heading">
			<?php 
				
				if($persona1Detail != '' && $persona2Detail != '' && $persona3Detail != '' && $persona4Detail != '')
				{
					echo ucfirst($persona1Detail->persona_title).' '.'Vs'.' '.ucfirst($persona2Detail->persona_title).' '.'Vs'.' '.ucfirst($persona3Detail->persona_title).' '.'Vs'.ucfirst($persona4Detail->persona_title);
				}
				else if($persona1Detail != '' && $persona2Detail != '' && $persona3Detail != '')
				{
					echo ucfirst($persona1Detail->persona_title).' '.'Vs'.' '.ucfirst($persona2Detail->persona_title).' '.'Vs'.' '.ucfirst($persona3Detail->persona_title);
				}
				else if($persona1Detail != '' && $persona2Detail != '')
				{
					echo ucfirst($persona1Detail->persona_title).' '.'Vs'.' '.ucfirst($persona2Detail->persona_title);
				}
				

			?>
		</h3>	
		<p class="des">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
		
		<div class="compare_table">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Overview</th>
							<?php
								if($persona1Detail != '')
								{
									echo '<th>'.ucfirst($persona1Detail->persona_title).'</th>';
								} 	
								if($persona2Detail != '')
								{
									echo '<th>'.ucfirst($persona2Detail->persona_title).'</th>';
								} 
								if($persona3Detail != '')
								{
									echo '<th>'.ucfirst($persona3Detail->persona_title).'</th>';
								}
								if($persona4Detail != '')
								{
									echo '<th>'.ucfirst($persona4Detail->persona_title).'</th>';
								}

							?>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Lifetime Visitors</td>
							<?php
								if($persona1Detail != '')
								{
									echo '<td>'.$per1LifetimeVisitors.'</td>';
								} 	
								if($persona2Detail != '')
								{
									echo '<td>'.$per2LifetimeVisitors.'</td>';
								} 
								if($persona3Detail != '')
								{
									echo '<td>'.$per3LifetimeVisitors.'</td>';
								}
								if($persona4Detail != '')
								{
									echo '<td>'.$per4LifetimeVisitors.'</td>';
								}

							?>
						</tr>
						<tr>
							<td>Monthly Visitors</td>
							<?php
								if($persona1Detail != '')
								{
									echo '<td>'.$per1MonthlyVisitors.'</td>';
								} 	
								if($persona2Detail != '')
								{
									echo '<td>'.$per2MonthlyVisitors.'</td>';
								} 
								if($persona3Detail != '')
								{
									echo '<td>'.$per3MonthlyVisitors.'</td>';
								}
								if($persona4Detail != '')
								{
									echo '<td>'.$per4MonthlyVisitors.'</td>';
								}

							?>
						</tr>
						<tr>
							<td>Weekly Visitors</td>
							<?php
								if($persona1Detail != '')
								{
									echo '<td>'.$per1WeeklyVisitors.'</td>';
								} 	
								if($persona2Detail != '')
								{
									echo '<td>'.$per2WeeklyVisitors.'</td>';
								} 
								if($persona3Detail != '')
								{
									echo '<td>'.$per3WeeklyVisitors.'</td>';
								}
								if($persona4Detail != '')
								{
									echo '<td>'.$per4WeeklyVisitors.'</td>';
								}

							?>
						</tr>
						<tr>
							<td>Facebook</td>
							<td>20,00000</td>
							<td>10,00000</td>
							<td>5,00000</td>
							<td>8,00000</td>
						</tr>
						<tr>
							<td>Twitter</td>
							<td>20,00000</td>
							<td>10,00000</td>
							<td>5,00000</td>
							<td>8,00000</td>
						</tr>
						<tr>
							<td>Instagram</td>
							<td>20,00000</td>
							<td>10,00000</td>
							<td>5,00000</td>
							<td>8,00000</td>
						</tr>
						<tr>
							<td>Youtube</td>
							<td>20,00000</td>
							<td>10,00000</td>
							<td>5,00000</td>
							<td>8,00000</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>

	<?php if($persona1Detail != ''){ ?>

		<div id="container"></div>
	<?php } ?>
	
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>

	<?php 
	
		/*===========Persona 1 Yearly record=========*/

		/*$per1yearly_record = array();
		foreach($per1YearlyRecord as $key=>$per1yearly)
		{
			$per1yearly_record[$key] =  count($per1yearly);
		}
		ksort($per1yearly_record);
		$per1yearlyPointStart = key($per1yearly_record);*/

		/*===========Persona 2 Yearly record=========*/

		/*$per2yearly_record = array();
		foreach($per2YearlyRecord as $key=>$per2yearly)
		{
			$per2yearly_record[$key] =  count($per2yearly);
		}
		ksort($per2yearly_record);
		$per2yearlyPointStart = key($per2yearly_record);*/

		/*===========Persona 3 Yearly record=========*/

		/*$per3yearly_record = array();
		foreach($per3YearlyRecord as $key=>$per3yearly)
		{
			$per3yearly_record[$key] =  count($per3yearly);
		}
		ksort($per3yearly_record);
		$per3yearlyPointStart = key($per3yearly_record);*/

		/*===========Persona 4 Yearly record=========*/

		/*$per4yearly_record = array();
		foreach($per4YearlyRecord as $key=>$per4yearly)
		{
			$per4yearly_record[$key] =  count($per4yearly);
		}
		ksort($per4yearly_record);
		$per4yearlyPointStart = key($per4yearly_record);*/

		
		
		
	?>

	Highcharts.chart('container', {

    title: {
        text: 'Monthly Comparison'
    },

   	
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    xAxis: {
       	categories: [<?php if($persona1Detail != ''){ echo "'".$per1currentMonthsDate."'";}?>],
	},

	series: [{
		<?php if($persona1Detail != ''){ ?>
	        name: <?php echo "'".ucfirst($persona1Detail->persona_title)."'";?>,
	        data: [<?php echo implode(",",$per1array);?>]
	    <?php } ?>
    }<?php if($persona2Detail != ''){?>, {
        name: <?php echo "'".ucfirst($persona2Detail->persona_title)."'";?>,
        data: [<?php echo implode(",",$per2array);?>]
    }<?php } if($persona3Detail != ''){?>, {
	        name: <?php echo "'".ucfirst($persona3Detail->persona_title)."'";?>,
	        data: [<?php echo implode(",",$per3array);?>]
    }<?php } if($persona4Detail != ''){?>, {
	        name: <?php echo "'".ucfirst($persona4Detail->persona_title)."'";?>,
	        data: [<?php echo implode(",",$per4array);?>]
    }<?php } ?>],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});


</script>

@endsection

