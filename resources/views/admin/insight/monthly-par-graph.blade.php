<?php 
	
	$monthly_record = array();
	$monRecords = array();
	foreach($monthlyRecord as $key=>$val)
	{
		$monthly_record[date('Y-m-d',strtotime($val->created_at))][] = $val->clicks;
	}

	foreach($monthly_record as $key=>$monRec)
	{
		$monRecords[$key] = array_sum($monRec);
	}
	ksort($monRecords);
	
	$currentMonthsDate = implode("','", array_map(
	    function ($v, $k)
	    { 
	    	return sprintf("%s", $k, $v); 
	    },
	    $monRecords,
	    array_keys($monRecords)
	));

?>


	
	<div id="monthly_container"></div> 
	
		

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>


/*=============Monthly visitors============*/

	Highcharts.chart('monthly_container', {

   	 	plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            
	        }
	    },
	    xAxis: {
               categories: [<?php echo "'".$currentMonthsDate."'";?>],
            },

	    series: [{
	        name: 'Monthly Interactions',
	        data: [<?php echo implode(",",$monRecords);?>]
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }
	});


</script>


