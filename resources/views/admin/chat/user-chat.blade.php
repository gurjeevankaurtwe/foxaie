@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Users List</h2>
</div>

<!-- For Alert -->
<br>
@if (\Session::has('success'))
  <div class="alert alert-success">
	<p>{{ \Session::get('success') }}</p>
  </div><br>
@endif
<!-- End For Alert -->

<div class="block">

	<div class="block-title">
		<h2><strong>Users List</strong></h2>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table_design" style="margin-bottom:0px;">
			<thead>
				<tr>
					<th>ID</th>
					<th>IP</th>
					<th>Email</th>
					<th>Action</th>			 
				</tr>
			</thead>
			<tbody>
			  @php $a=1; @endphp 
			  @foreach($chat as $userChat)
				<tr>
					<td>{{$a}}</td>
					<!-- <td>{{$userChat['user_ip_address']}}</td> -->
					<td>{{$userChat['user_ip_address']}}</td>
					<td>{{$userChat['email']}}</td>
					<td>
						@if($userChat['email'] != '')
							<a href="{{url('/persona/user_chat_detail/'.$userChat['uniqueId'].'/'.Crypt::encryptString($userChat['email']))}}" class="btn btn-info btn-sm edit-btn" >Chat</a>
						@else
							<a href="{{url('/persona/user_chat_detail/'.$userChat['uniqueId'])}}" class="btn btn-info btn-sm edit-btn" >Chat</a>
						@endif
					</td>
				</tr>			
				@php $a++; @endphp
				@endforeach
			</tbody>
	  </table>
	</div>
</div>

@endsection


