<!DOCTYPE html>

<html class="no-js">

  <head>

    <meta charset="utf-8">

    <title>Fox AI || Login</title>

    <meta name="description" content="">

    <meta name="author" content="pixelcave">

    <meta name="robots" content="noindex, nofollow">

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

	<link rel="shortcut icon" href="{{asset('admin/img/placeholders/avatars/favicon.png')}}">

    <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('admin/css/plugins.css')}}">

    <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">

    <link rel="stylesheet" href="{{asset('admin/css/themes.css')}}">

    <script src="{{asset('admin/js/vendor/modernizr-respond.min.js')}}"></script>

  </head>

  <body>

    <img src="img/placeholders/backgrounds/login_bg.png" alt="Login Full Background" class="full-bg animation-pulseSlow">

    <div id="login-container" class="animation-fadeIn">

      <div class="login-title text-center">

        <h1><strong>Chat Bot</strong><br><small>Please <strong>Login</strong></small></h1>

      </div>

      <div class="block push-bit">

        <form action="{{url('login')}}" method="post" id="form-login" class="form-horizontal form-bordered form-control-borderless">

          @csrf

          <div class="form-group">

            <div class="col-xs-12">

              <div class="input-group">

                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>

                <input type="text" id="login-email" name="login-email" class="form-control input-lg" placeholder="Email" required>

              </div>

            </div>

          </div>

          <div class="form-group">

            <div class="col-xs-12">

              <div class="input-group">

                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>

                <input type="password" id="login-password" name="login-password" class="form-control input-lg" placeholder="Password" required>

              </div>

            </div>

          </div>

          <div class="form-group form-actions">

            <div class="col-xs-4">

              <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">

              <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>

              <span></span>

              </label>

            </div>

            <div class="col-xs-8 text-right">

              <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>

            </div>

          </div>

          <div class="form-group">

            <div class="col-xs-12 text-center">

              <a href="javascript:void(0)" id="link-reminder-login"><small>Forgot password?</small></a> 

            </div>

          </div>

        </form>

        <form action="login_full.php#reminder" method="post" id="form-reminder" class="form-horizontal form-bordered form-control-borderless display-none">

          <div class="form-group">

            <div class="col-xs-12">

              <div class="input-group">

                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>

                <input type="text" id="reminder-email" name="reminder-email" class="form-control input-lg" placeholder="Email">

              </div>

            </div>

          </div>

          <div class="form-group form-actions">

            <div class="col-xs-12 text-right">

              <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Reset Password</button>

            </div>

          </div>

          <div class="form-group">

            <div class="col-xs-12 text-center">

              <small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>

            </div>

          </div>

        </form>

        <form action="login_full.php#register" method="post" id="form-register" class="form-horizontal form-bordered form-control-borderless display-none">

          <div class="form-group">

            <div class="col-xs-12">

              <div class="input-group">

                <span class="input-group-addon"><i class="gi gi-user"></i></span>

                <input type="text" id="register-firstname" name="register-firstname" class="form-control input-lg" placeholder="Full Name">

              </div>

            </div>            

          </div>

          <div class="form-group">

            <div class="col-xs-12">

              <div class="input-group">

                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>

                <input type="text" id="register-email" name="register-email" class="form-control input-lg" placeholder="Email">

              </div>

            </div>

          </div>

          <div class="form-group">

            <div class="col-xs-12">

              <div class="input-group">

                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>

                <input type="password" id="register-password" name="register-password" class="form-control input-lg" placeholder="Password">

              </div>

            </div>

          </div>

          <div class="form-group">

            <div class="col-xs-12">

              <div class="input-group">

                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>

                <input type="password" id="register-password-verify" name="register-password-verify" class="form-control input-lg" placeholder="Verify Password">

              </div>

            </div>

          </div>

          <div class="form-group form-actions">

            <div class="col-xs-6">

              <a href="#" data-toggle="modal" class="register-terms">Terms</a>

              <label class="switch switch-primary" data-toggle="tooltip" title="Agree to the terms">

              <input type="checkbox" id="register-terms" name="register-terms">

              <span></span>

              </label>

            </div>

            <div class="col-xs-6 text-right">

              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Register Account</button>

            </div>

          </div>

          <div class="form-group">

            <div class="col-xs-12 text-center">

              <small>Do you have an account?</small> <a href="javascript:void(0)" id="link-register"><small>Login</small></a>

            </div>

          </div>

        </form>

      </div>

    </div>



    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>

    <script src="{{asset('admin/js/vendor/bootstrap.min.js')}}"></script>

    <script src="{{asset('admin/js/plugins.js')}}"></script>

    <script src="{{asset('admin/js/app.js')}}"></script>

    <script src="{{asset('admin/js/pages/login.js')}}"></script>

    <script>$(function(){ Login.init(); });</script>

  </body>

</html>