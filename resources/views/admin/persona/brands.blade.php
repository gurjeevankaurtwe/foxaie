@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Brand List</h2>
	<a href="javascript:void();" class="btn btn-info" data-toggle="modal" data-target="#add-brand" style="float:right;">Add New Brand</a>
</div>

<!-- For Alert -->
<br>
@if (\Session::has('success'))
<div class="alert alert-success">
	<p>{{ \Session::get('success') }}</p>
</div>
<br>
@endif
<!-- End For Alert -->

<div class="block">
	
	<div class="block-title">
		<h2><strong>Brand List</strong></h2>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table_design" style="margin-bottom:0px;">
    
			<thead>
			  <tr>
				<th>ID</th>
				<th>Brand</th>
				<th>Status</th>
				<th>Action</th>
			  </tr>
			</thead>

			<tbody>
			  @php $i = 1; @endphp
			  @foreach($brands as $val)
				<tr>
				  <td>{{$i}}</td>
				  <td>{{ucfirst($val->brand)}}</td>
				  <td>
					@if($val->status == 'I')
						{{'In-active'}}
					@else
					  {{'Active'}}
					@endif
				  </td>
				  <td>
					<a href="{{action('PersonaController@edit', $val->id)}}" class="btn btn-info btn-sm edit-brand" data-toggle="modal" data-target="#edit-brand" data-id="{{$val->_id}}" data-iname="{{$val->brand}}" data-istatus="{{$val->status}}">Edit</a>
					<a href="{{url('backoffice/persona/brand_compaign/'.$val->id)}}" class="btn btn-info btn-sm">Compaign</a>
					@if($val->status == 'I')
						<button class="btn btn-info btn-sm deleteFun" data-value="A" data-table="Brands" data-target="{{$val->id}}" data-attr="Are you sure you want to Active this Brand?" data-href="Active Brand" type="button">In-active</button>
					@else
					  <button class="btn btn-info btn-sm deleteFun" data-value="I" data-table="Brands" data-target="{{$val->id}}" data-attr="Are you sure you want to In-active this Brand?" data-href="In-active Brand" type="button">Active</button>
					@endif
				  </td>				 
				</tr>
				@php $i++; @endphp
			  @endforeach
			</tbody>
			
		</table>
	</div>
	
</div>

@endsection

<!-- Add Brand Modal -->
<div class="modal fade modal_design" id="add-brand">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Brand</h4>
			</div>
			<div class="modal-body">
				<div class="modal_box">					
					<form method="post" action="{{url('backoffice/persona/add_brand')}}" enctype="multipart/form-data">
						@csrf	
						<div class="form-group">
							<label class="control-label">Brand</label>
							<input type="text" class="form-control" placeholder="Enter Brand" required name="brand">
						</div>
						<div class="form-group">
							<label class="control-label">Brand Image</label>
							<input type="file" class="form-control" required name="brand_image">
						</div>
						<div class="form-group">
							<label class="control-label">Status</label>
							<select class="form-control" name="status" required>
								<option value="A">Active</option>
								<option value="I">In-Active</option>
							</select>
						</div>
						<div class="text-center" style="margin-top:30px;">
							<button type="submit" class="btn btn-info btn-lg">Submit</button>
						</div>	
					</form>
				</div>										
			</div>
		</div>
	</div>
</div>

<!-- Edit Brand Modal -->
<div class="modal fade" id="edit-brand">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Brand</h4>
			</div>
			<div class="modal-body">
				<div class="modal_box">
					<form method="post" action="{{url('backoffice/persona/update_brand/')}}">
						@csrf
						<input type="hidden" id="brand-id" required name="brand-id">
						<div class="form-group">
							<label class="control-label">Brand</label>
							<input type="text" class="form-control" id="brand-name" required name="brand">
						</div>
						<div class="form-group">
							<label class="control-label">Brand Image</label>
							<input type="file" class="form-control" required name="brand_image">
						</div>
						<div class="form-group">
							<label class="control-label">Status</label>
							<select class="form-control" name="status" id="brand-status" required>
								<option value="A">Active</option>
								<option value="I">In-Active</option>
							</select>
						</div>			
						<div class="text-center" style="margin-top:30px;">
							<button type="submit" class="btn btn-info btn-lg">Submit</button>
						</div>					
					  </form>
				</div>		
			</div>
		</div>
	</div>
</div>