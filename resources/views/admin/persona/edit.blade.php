@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Edit Persona</h2>
</div>	 

<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="block">
			<div class="block-title">
				<h2><strong>Edit Persona</strong></h2>
			</div>
			<form class="form-horizontal form-bordered" method="post" action="{{action('PersonaController@update', $id)}}" enctype="multipart/form-data">
			@csrf
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Persona Industry</label>
				<div class="col-sm-9">
					<select class="form-control" name="industry" id="persona_industries" required>
						<option value="">Select</option>
						@foreach($industries as $industry)
							@if($industry->id == $persona->industry)		 
								@php $sel = 'selected="selected"'; @endphp
							@else
								@php $sel = ''; @endphp
							@endif
							<option value="{{$industry->id}}" data-attr="{{$industry->industry}}" {{$sel}}>{{$industry->industry}}</option>	
						@endforeach
					</select>
				</div>				
			 </div>
			 
			<div class="form-group">
				<label class="col-sm-3 control-label">Persona Brand</label>
				<div class="col-sm-9">
					<select class="form-control" name="brand" id="persona_brands" required>
					  	<option value="">Select</option>
						@foreach($brands as $brand)
							  @if($brand->id == $persona->brand)		 
								@php $sel = 'selected="selected"'; @endphp
							  @else
								@php $sel = ''; @endphp
							  @endif
							  <option value="{{$brand->id}}" data-attr="{{$brand->brand}}" {{$sel}}>{{$brand->brand}}</option>
						@endforeach
					</select>
				</div>
			</div>
			 
			<div class="form-group brand-compaigns">
				<label class="col-sm-3 control-label">Persona Brand Compaign</label>
				<div class="col-sm-9">
					<select class="form-control" name="compaign" id="persona_compaigns" required>
					  	<option value="">Select</option>
						@foreach($compaign as $comp)
						  @if($comp->_id == $persona->compaign)	  
							<?php $sel = 'selected="selected"'; ?>
						  @else
							<?php $sel = '';?>
						  @endif
						  <option value="{{$comp->id}}" data-attr="{{$comp->compaign}}" {{$sel}}>{{$comp->compaign}}</option>
						@endforeach
					</select>
				</div>
			</div>	

			<div class="form-group">
				<label class="col-sm-3 control-label">Channel</label>
				<div class="col-sm-9">
					<select class="form-control" name="channel">
						<option value="">Select Channel...</option>
						<option value="Facebook" <?php if($persona->channel =='Facebook'){ echo "selected='selected'";}?>>Facebook</option>
						<option value="Twitter" <?php if($persona->channel =='Twitter'){ echo "selected='selected'";}?>>Twitter</option>
						<option value="Website" <?php if($persona->channel =='Website'){ echo "selected='selected'";}?>>Website</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">URL</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="url" id="persona_url" value="{{$persona->url}}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Persona</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="persona_title" value="{{$persona->persona_title}}" id="persona_title">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Tags</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="tags" value="{{$persona->tags}}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Score</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="checkMinScore" placeholder="Score" name="score" min="1" value="{{$persona->score}}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Login Require</label>
				<div class="col-sm-9">
					<input name='require_login' id="require_login" type='checkbox' value='1' <?php if($persona->require_login == 1){ echo "checked='checked'";}?> >
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Image</label>
				<div class="col-sm-6">
					<input type="hidden" class="check-image" name="check_image" value="{{$persona->persona_image}}">
					<input type="file" class="form-control upload-persona-image" name="persona_image" required>
				</div>
				<div class="col-sm-3">
					<div id="image">
						<img id="blah" src="{{asset('uploads/user-images/'.$persona->persona_image)}}" width="50" height="50">
					  </div>
				</div>
			</div>
			
			<div class="text-center" style="margin-bottom:20px;">
				<button type="submit" class="btn btn-success btn-md">Update Persona</button>
			</div>
			
			</form>
		</div>
	</div>
</div>

@endsection