@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Content</h2>
</div>

<div class="block" style="margin-top:20px;">
	<form name="content-add" id="content-add" method="post" enctype="multipart/form-data" action="{{action('PersonaController@save_content', $id)}}">
		@csrf 
		<?php 
			if(count($content) == 0){
				$coid = count($content)+1;
			}else{
				$carray = json_decode(json_encode($content), true);
				$coid = max(array_column($carray, 'content_cust_id'))+1;
			}			
		?>
		<input type="hidden" name="content_id" class="edit-content-id">
		<div class="row">
			<div class="form-group col-md-6">
				<label for="type">Content ID</label>
				<input class="form-control" name='content_cust_id' id="content_cust_id" type='text' readonly value='{{$coid}}'>
			</div>
			 <div class="form-group col-md-6">
				<label for="type">Select Resonse Type</label>
				<select class="form-control" class="edit-response-type" id="response-type" name="type" required>
					<option value="">Select</option>
					@if($persona->channel == 'Facebook' || $persona->channel == 'Twitter')
						<option value="Text">Text</option>
						<option value="File">File</option>
						<option value="Radio">Quick Reply</option>
					@else
						<option value="Text">Text</option>
						<option value="Textarea">Textarea</option>
						<option value="Button">Button</option>
						<option value="Radio">Radio</option>
						<option value="File">Image</option>
						<option value="Docs">File</option>
						<option value="Link">Link</option>
						<option value="Form">Form</option>
						<option value="Checkbox">Checkbox</option>
						<option value="Embed">Embed</option>
					@endif
				</select>
			</div>
		</div>
		<div class="row"></div>

		<input name='persona-id' id="persona-id" type='hidden' value='{{$id}}' />
		<input name='persona-id-test' id="persona-id" type='hidden' value='{{$id}}' />
		<input name='cusperconid' id="cusperconid" type='hidden' value='{{$coid}}' />

		<div id="content-field"></div>
		<div id="content-btn-sub" class="row">
			<div class="form-group col-md-4">
				<button type="submit" class="btn btn-success content-btn">Submit</button>
			</div>
		</div>
	</form>
</div>
<div class="clearfix"></div>

<div class="block">
	<div>
		<table class="table table-striped table_design">
			<thead>
			  <tr>
				<th>ID</th>
				<th>Type</th>
				<th>Content</th>
				<th>Response</th>
				<th>Score</th>
				<th>Action</th>
			  </tr>
			</thead>
			<tbody>  
			  @foreach($content as $cont)
			  <tr>
				<td>{{$cont->content_cust_id}}</td>
				<td>
					<?php
						if($persona->channel == 'Facebook')
						{
							$data = $cont->content;
							if(array_key_exists('attachment',$data['message']))
							{
								echo 'File';
							}
							else if(array_key_exists('quick_replies',$data['message']))
							{
								echo 'Quick Reply';
							}
							else
							{
								echo ucfirst(key($data['message']));
							}
						}
						else if($persona->channel == 'Twitter')
						{
							$data = $cont->content;
							if(array_key_exists('quick_reply',$data['event']['message_create']['message_data']))
							{
								echo 'Quick Reply';
							}
							else
							{
								echo ucfirst(key($data['event']['message_create']['message_data']));
							}
						}
						else
						{
							$data = json_decode($cont->content);
							echo  $data->type;
						}
					?>
				</td>
				<td>
					<?php
						if($persona->channel == 'Facebook')
						{
							$data = $cont->content;
							$gettype = ucfirst(key($data['message']));
							if(array_key_exists('attachment',$data['message']))
							{
								$type =  'File';
							}
							else if(array_key_exists('quick_reply',$data['message']))
							{
								$type =  'Radio';
							}
							else
							{
								$type =  ucfirst(key($data['message']));
							}
							if($type == 'Text')
							{
								$response = $data['message']['text'];
							}
							else if($type == 'File')
							{
								$path = $data['message']['attachment']['payload']['url'];
								$imgSrc = asset($path);
							}
							else if($type == 'Radio')
							{
								$response = $data['message']['text'];
							}
						}
						else if($persona->channel == 'Twitter')
						{
							if(array_key_exists('quick_reply',$data['event']['message_create']['message_data']))
							{
								$type =  'Radio';
							}
							else
							{
								$type = ucfirst(key($data['event']['message_create']['message_data']));
							}
							if($type == 'Text')
							{
								$response = $data['event']['message_create']['message_data']['text'];
							}
							else if($type == 'Radio')
							{
								$response = $data['event']['message_create']['message_data']['text'];
							}
						}
						else
						{
							$data = json_decode($cont->content);
							$data_input = json_decode($cont->response);
							$type =  $data->type;
							$response = $data->query;
							$imgSrc = asset($data->query);
						}
						if($type == "Text"){ ?>
							
							<div class="fields_show">
								<p><strong>Response: </strong> {{$response}}</p>
								<?php if(isset($data->email)){?>
									<p><strong>Type: </strong> {{$data->email}}</p>
								<?php } ?>
							</div>
						<?php } else if($type == "Button"){ ?>
							
							<div class="fields_show">
								<p><strong>Query: </strong>{{ucfirst($data->answer)}}</p>
								<p><strong>Button Label: </strong> {{$response}}</p>
							</div>
						
						<?php }else if($type == "Textarea"){ ?>
							
							<div class="fields_show">
								@php
									if(isset($data->answer)) 
									{
										echo '<p><strong>Query: </strong>'.ucfirst($data->answer).'</p>';
									} 
								@endphp
								<p><strong>Response: </strong> {!!$response!!}</p>
							</div>

						<?php }else if($type == "Embed"){ ?>
							
							<div class="fields_show">
								@php
									if(isset($data->answer)) 
									{
										echo '<p><strong>Query: </strong>'.ucfirst($data->answer).'</p>';
									} 
								@endphp
								<p><strong>Button Label:</strong> @if(isset($data->btnlabel)){{$data->btnlabel}} @endif</p>
								<p><strong>Response: </strong> {!!$response!!}</p>
							</div>

						<?php }else if($type == "Radio"){ ?>
							
							<div class="fields_show">
								<p><strong>Query:</strong> {{$data->query}}</p>
								<p><strong>Options: </strong></p>
								<ul>
									<?php $oparr =$data->answer ; ?>
									@foreach($oparr as $opts)
										<li>{{$opts}}</li>
									@endforeach
								</ul>
							</div>
						
						<?php }else if($type == "Checkbox"){ ?>
							
							<div class="fields_show">
								<p><strong>Query:</strong> {{$data->query}}</p>
								<p><strong>Options: </strong></p>
								<ul>
									<?php $oparr =$data->answer ; ?>
									@foreach($oparr as $opts)
									<li>{{$opts}}</li>
									@endforeach
								</ul>
							</div>
						<?php } else if($type == "File"){ ?>
							<div class="fields_show">
								<?php
									
									if(isset($data->answer)) 
									{
										echo $data->answer;
									} 
								?>
								<p><strong>Query:</strong> @if(isset($data->label)){{$data->label}} @endif</p>
								<p><strong>Button Label:</strong> @if(isset($data->btnlabel)){{$data->btnlabel}} @endif</p>
								<p><strong>Image:</strong> <img src="<?php echo $imgSrc; ?>" class="img-rounded" style="height:40px;width:40px;"></p>								
							</div>
						<?php } else if($type == "Docs"){ ?>
							<div class="fields_show">
								<?php
									
									if(isset($data->answer)) 
									{
										echo $data->answer;
									} 
								?>
								<p><strong>Query:</strong> @if(isset($data->label)){{$data->label}} @endif</p>
								<p><strong>Button Label:</strong> @if(isset($data->btnlabel)){{$data->btnlabel}} @endif</p>
								<p><strong>File:</strong> <?php echo $imgSrc; ?></p>								
							</div>
						<?php } else if($type == "Link"){ ?>
							
							<div class="fields_show">
								@if(isset($data->answer))
									<p><strong>Query:</strong> {{$data->answer}}</p>
								@endif
								<p><strong>Link:</strong> {{$data->query}}</p>			
							</div>
						
						<?php }else if($type == "Form"){ ?>
								<div class="fields_show">
									<p><strong>Form:</strong> {{ $data->query }} </p>
									<?php
										$oparr =$data->formcont ;
										$formItems = json_decode($oparr);
										
										foreach($formItems as $formItem){
											if($formItem->type =='textarea'){ ?>
											<div class="form-group">
												<label for="type">{{$formItem->label }}</label>
												<textarea name="{{$formItem->name }}" class="{{$formItem->className }}"> </textarea>
											</div>
											<?php }else if($formItem->type =='select'){ ?>
											<div class="form-group">
												<label for="type">{{$formItem->label }}</label>
												<select class="{{$formItem->className }}" id="response-type" name="{{$formItem->name }}" required>
													@foreach($formItem->values as $slectOptions)
													<option value="{{$slectOptions->value}}">{{$slectOptions->label}}</option>
													@endforeach
												</select>
											</div>
											<?php }else if($formItem->type =='text'){ ?>
											<div class="form-group">
												<label for="type">{{$formItem->label }}</label>
												<input type="text" name="{{$formItem->name }}" class="{{$formItem->className }}" >
											</div>
											<?php }else if($formItem->type =='number'){ ?>
											<div class="form-group">
												<label for="type">{{$formItem->label }}</label>
												<input type="number" name="{{$formItem->name }}" class="{{$formItem->className }}" >
											</div>
											<?php }else if($formItem->type =='date'){ ?>
											<div class="form-group">
												<label for="type">{{$formItem->label }}</label>
												<input type="date" name="{{$formItem->name }}" class="{{$formItem->className }}" >
											</div>
											<?php }else if($formItem->type =='file'){ ?>
											<div class="form-group">
												<label for="type">{{$formItem->label }}</label>
												<input type="file" name="{{$formItem->name }}" class="{{$formItem->className }}" >
											</div>
											<?php }else if($formItem->type =='checkbox-group'){ ?>
											<div class="form-group">
												<label for="type">{{$formItem->label }}</label>
												<ul>
													@foreach($formItem->values as $slectOptions)
														<li><input type="checkbox" name="{{$formItem->name }}" value="{{$slectOptions->value}}">{{$slectOptions->label}} </li> 
													@endforeach
												</ul>
											</div>
											<?php }else if($formItem->type =='radio-group'){ ?>
											<div class="form-group">
												<label for="type">{{$formItem->label }}</label>
												<ul>
													@foreach($formItem->values as $slectOptions)
														<li><input type="radio" name="{{$formItem->name }}" value="{{$slectOptions->value}}">{{$slectOptions->label}} </li>
													@endforeach
												</ul>
											</div>
											<?php } else if($formItem->type =='button'){ ?>
											<div class="form-group">
												<input type="button" name="{{$formItem->name }}" value="{{$formItem->label }}" class="{{$formItem->className }}" >
											</div>								
											<?php
											}
										} 
									?>
								</div>
						
			   			<?php } ?>
				</td>
				<td>
				<?php
					foreach($reponse_content as $res_cont)
					{
						if($res_cont->content_id == $cont->_id)
						{
							$res_data = json_decode($res_cont->response_content);
							foreach($content as $cont_r)
							{
								if($cont_r->_id == $res_data->response_cont_id)
								{

									
									if($persona->channel == 'Facebook')
									{
										$data = $cont_r->content;
										if(array_key_exists('attachment',$data['message']))
										{
											$type =  'File';
											$response = $data['message']['attachment']['payload']['url'];
										}
										else if(array_key_exists('quick_replies',$data['message']))
										{
											$type =  'Radio';
											$response = $data['message']['text'];
										}
										else
										{
											$type =  ucfirst(key($data['message']));
											$response = $data['message']['text'];
										}
									}
									else if($persona->channel == 'Twitter')
									{
										$data = $cont_r->content;
										if(array_key_exists('quick_reply',$data['event']['message_create']['message_data']))
										{
											$type =  'Radio';
											$response = $data['event']['message_create']['message_data']['text'];
										}
										else
										{
											$type =  ucfirst(key($data['event']['message_create']['message_data']));
											$response = $data['event']['message_create']['message_data']['text'];
										}
									}
									else
									{
										$res_data1 = json_decode($cont_r->content); 
										$response = $res_data1->query;
									}
									echo $response;
								}	
							}
							
						}
					}
				?>
				</td>
				<td>{{$cont->score}}</td>
				<td>
					<a href="JavaScript:void(0);" class="btn btn-info btn-sm edit-persona-content" data-target="{{$persona->channel}}" data-attr="{{$cont->id}}" title="Edit"><i class="fa fa-pencil"></i></a>
					<a href="JavaScript:void(0);" class="btn btn-info btn-sm" onclick="configure_content('{{$cont->_id}}', '{{$id}}');" title="Configure"><i class="fa fa-cog"></i></a>
					<form action="{{action('PersonaController@delete_cont', $cont->id)}}" method='post' onsubmit="return confirm('Are you want to delete content!')">
						@csrf
						<input name='_method' type='hidden' value='DELETE'>
						<input name='cont-id' type='hidden' value='{{$id}}'>
						<button class="btn btn-info btn-sm" type="submit" title="Delete"><i class="fa fa-trash"></i></button>
					</form>
				</td>
			  </tr>
			  @endforeach
			</tbody>
		</table>
	</div>
</div>
	
@endsection


