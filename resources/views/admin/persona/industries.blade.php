@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Industries List</h2>
	<a href="javascript:void();" class="btn btn-info" data-toggle="modal" data-target="#add-industry" style="float:right;">Add New Industries</a>
</div>

<!-- For Alert -->
<br>
@if (\Session::has('success'))
<div class="alert alert-success">
	<p>{{ \Session::get('success') }}</p>
</div>
<br>
@endif
<!-- End For Alert -->

<div class="block">
  
    <div class="block-title">
		<h2><strong>Industries List</strong></h2>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table_design">
			<thead>
				<tr>
					<th>ID</th>
					<th>Industry</th>
					<th>Status</th>
					<th>Action</th>				  
				</tr>
			</thead>
			<tbody>
				@php $i = 1; @endphp
				@foreach($industries as $industry)
				  <tr>
					<td>{{$i}}</td>
					<td>{{ucfirst($industry->industry)}}</td>
					<td>
					  @if($industry->status == 'I')
						{{'In-active'}}
					  @else
						{{'Active'}}
					  @endif
					</td>
					<td>
					  <a href="{{action('PersonaController@edit', $industry->id)}}" class="btn btn-info btn-sm edit-btn" data-toggle="modal" data-target="#edit-industry" data-id="{{$industry->_id}}" data-iname="{{$industry->industry}}" data-istatus="{{$industry->status}}">Edit</a>
					  @if($industry->status == 'I')
						<button class="btn btn-info btn-sm deleteFun" data-value="A" data-table="Industries" data-target="{{$industry->id}}" data-attr="Are you sure you want to Active this Industry?" data-href="Active Industry" type="button">In-active</button>
						@else
						<button class="btn btn-info btn-sm deleteFun" data-value="I" data-table="Industries" data-target="{{$industry->id}}" data-attr="Are you sure you want to In-active this Industry?" data-href="In-active Industry" type="button">Active</button>
						@endif
					</td>					
				  </tr>
				  @php $i++; @endphp
				@endforeach
			</tbody>
		</table>
	</div>
	
</div>

@endsection

<!-- Add industry Modal -->
<div class="modal fade" id="add-industry">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Industry</h4>
			</div>
			<div class="modal-body">
				<div class="modal_box">
					<form method="post" action="{{url('backoffice/persona/add_industry')}}">
						@csrf						
						<div class="form-group">
							<label class="control-label">Industry</label>
							<input type="text" class="form-control" placeholder="Enter Industry" required name="industry">
						</div>						
						<div class="form-group">
							<label class="control-label">Status</label>
							<select class="form-control" name="status" required>
								<option value="A">Active</option>
								<option value="I">In-Active</option>
							</select>
						</div>
						<div class="text-center" style="margin-top:30px;">
							<button type="submit" class="btn btn-info btn-lg">Submit</button> 
						</div>							
					</form>
				</div>				
			</div>
		</div>
	</div>
</div>

<!-- Edit Industry Modal -->

<div class="modal fade" id="edit-industry">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Industry</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="{{url('backoffice/persona/industries_edit/')}}">
					@csrf
					<input type="hidden" id="industry-id" required name="industry-id">				
					<div class="form-group">
						<label class="control-label">Industry</label>
						<input type="text" class="form-control" id="industry-name" required name="industry">
					</div>
					<div class="form-group">
						<label class="control-label">Status</label>
						<select class="form-control" name="status" id="industry-status" required>
							<option value="A">Active</option>
							<option value="I">In-Active</option>
						</select>
					</div>
					<div class="text-center" style="margin-top:30px;">
						<button type="submit" class="btn btn-info btn-lg">Submit</button> 
					</div>				
				</form>
			</div>
		</div>
	</div>
</div>