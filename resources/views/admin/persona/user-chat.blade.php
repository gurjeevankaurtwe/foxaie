@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Users List</h2>
</div>

<!-- For Alert -->
<br>
@if (\Session::has('success'))
<div class="alert alert-success">
	<p>{{ \Session::get('success') }}</p>
</div>
<br>
@endif
<!-- End For Alert -->

<?php //echo "<pre>"; print_r($grouped);die; ?>

<div class="block">
	<div class="block-title">
		<h2><strong>Users List</strong></h2>
	</div>
	<div class="table-responsive">
		<table class="table table-striped table_design" style="margin-bottom:0px;">
			<thead>
			  <tr>
				<th>ID</th>
				<th>Email</th>
				<th>Ip</th>
				<th>Action</th>
			  </tr>
			</thead>
			<tbody>   
			   @php $a=1; @endphp 
			  @foreach($grouped as $user)
			  <tr>
				<td>{{$a}}</td>
				<td>{{$user['email']}}</td>
				<td>{{$user['user_ip_address']}}</td>
				<td><a href="{{action('PersonaController@user_chat_sess', $user['email'])}}" class="btn btn-info btn-sm edit-btn" >Chat</a></td> 
				</td>
			  </tr>
				@php $a++; @endphp
			  @endforeach
			</tbody>
	  </table>
	</div>
</div>

@endsection
