@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Add New Persona</h2>
</div>	 

<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="block">
			
			<div class="block-title">
				<h2><strong>Add New Persona</strong></h2>
			</div>
			
			<form class="form-horizontal form-bordered" method="post" action="{{url('backoffice/persona/add_persona')}}" enctype="multipart/form-data">
			@csrf
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Persona Industry</label>
				<div class="col-sm-9">
					<select class="form-control" name="industry" id="persona_industries" required>
						<option value="">Select</option>
						@foreach($industries as $industry)	
						  <option value="{{$industry->id}}" data-attr="{{$industry->industry}}">{{$industry->industry}}</option>
						  @endforeach
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Persona Brand</label>
				<div class="col-sm-9">
					<select class="form-control" name="brand" id="persona_brands" required>
						<option value="">Select</option>
						@foreach($brands as $brand)  
						<option value="{{$brand->id}}" data-attr="{{$brand->brand}}">{{$brand->brand}}</option>
						@endforeach
					</select>
				</div>
			</div>
			
			<div class="form-group brand-compaigns" style="display:none">
				<label class="col-sm-3 control-label">Persona Brand Compaign</label>
				<div class="col-sm-9">
					<select class="form-control" name="compaign" id="persona_compaigns" required></select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Channel</label>
				<div class="col-sm-9">
					<select class="form-control" name="channel" required>
						<option value="">Select Channel...</option>
						@if(count($facebookPersona) == 0)
							<option value="Facebook">Facebook</option>
						@endif
						@if(count($twitterPersona) == 0)
							<option value="Twitter">Twitter</option>
						@endif
						<option value="Website">Website</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Persona</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" required name="persona_title" id="persona_title">
				</div>
			</div>
			
			<input type="hidden" name="persona_url" id="set_persona_url">
			<div class="form-group">
				<label class="col-sm-3 control-label">URL</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="persona_url" name="url" value="{{url('/')}}/chat" readonly required>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Tags</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="tags" required>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Score</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="checkMinScore" placeholder="Score" name="score" min="1" required>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Login Require</label>
				<div class="col-sm-9">
					<input name='require_login' id="require_login" type='checkbox' value='1'>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Image</label>
				<div class="col-sm-9">
					<input type="file" class="form-control" name="persona_image" required>
				</div>
			</div>
			
			<div class="text-center" style="margin-bottom:20px;">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
			
		  </form>
		  
		</div>
	</div>
</div>
     
@endsection
