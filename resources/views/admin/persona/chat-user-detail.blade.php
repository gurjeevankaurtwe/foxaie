@extends('layouts.admin')

@section('content')

  <div class="container">

    <h2>Chat</h2>

    <br />

    @if (\Session::has('success'))

      <div class="alert alert-success">

        <p>{{ \Session::get('success') }}</p>

      </div><br />

     @endif
	 
	 
	 
	 <div class="block full">
		
		<div class="block-title">
			<ul class="nav nav-tabs" id="myTab" role="tablist">

			<?php
			$i=1;
			foreach($usrc as $userc){
				echo '<li class="nav-item">
					<a href="#chat'.$i.'" class="nav-link active" data-toggle="tab">Chat '.$i.'</a>
				</li>';
				$i++;
			}
			?>
				
			</ul>
		</div>
		
		<div class="tab-content" id="myTabContent">
		
			<?php
			$i=1;
			foreach($usrc as $userc){
				echo '<div class="tab-pane fade show active in" id="chat'.$i.'">
				<div class="chat_tabbox">';
				
				echo html_entity_decode($userc->user_msg);
				echo '</div>
			</div>';
				$i++;
			}
			?>
			
		</div>
		
	</div>
	 
	 
	
	 

    <table class="table table-striped" style="display:none;">

    <thead>

      <tr>
        <th>User Message</th>
		<th>Persona Message</th>
      </tr>

    </thead>

    <tbody>

      

      @foreach($usrc as $userc)

      <tr>
		<td>{{$userc->user_msg}}</td>
		<td>{{$userc->persona_msg}}</td>
        </td>

      </tr>

      @endforeach

    </tbody>

  </table>

  </div>

@endsection

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script>
	/*jQuery(document).ready(function(){
		var tabId =  $('ul#myTab').find('li').find('a.active').attr('href');
		alert(jQuery(tabId).find('form').attr('id'));
	});*/
	/*jQuery(document).on('click','.nav-link',function(){
		var tabId =  $(this).attr('href');
		alert(jQuery(tabId).find('form').attr('id'));
	});*/
</script>


