@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Manage Admin</h2>
</div>

<?php $userData = Auth::user(); ?>

<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="block">
			
			<div class="block-title">
				<h2><strong>Manage Admin</strong></h2>
			</div>
			
			<form class="form-horizontal form-bordered" action="{{url('backoffice/persona/update-profile')}}" method="Post" enctype="multipart/form-data">
				@csrf
			  
				<input type="hidden" name="id" value="{{$userData->id}}">

				<div class="form-group">
					<label class="col-sm-3 control-label">Insight Banner Image</label>
					<div class="col-sm-6">
						<input type="hidden" name="check-banner-image" value="<?php if(!isset($userData->banner_image)){ echo $userData->banner_image;} ?>">
						<input type="file" class="form-control" name="banner_image" id="banner_image" <?php if(!isset($userData->banner_image)){ echo 'required';} ?>>
					</div>
					<div class="col-sm-3">
						<div id="show_banner_image_div" <?php if(!isset($userData->banner_image)){ echo 'style="display:none"';} ?>>
							<img src="{{asset('uploads/user-images/'.$userData->banner_image)}}" id="show_banner_image" width="50" height="50" class="img-circle">
						</div>
					</div>	
				</div>
			  
				<div class="form-group">
					<label class="col-sm-3 control-label">Profile Image</label>
					<div class="col-sm-6">
						<input type="hidden" class="check-profile-image" name="check-profile-image" value="<?php if(!isset($userData->profile_image)){ echo $userData->profile_image;} ?>">
						<input type="file" class="form-control" name="profile_image" id="profile_image" <?php if(!isset($userData->profile_image)){ echo 'required';} ?>>
					</div>
					<div class="col-sm-3">
						<div id="show_profile_image_div" <?php if(!isset($userData->profile_image)){ echo 'style="display:none"';} ?>>
							<img src="{{asset('uploads/user-images/'.$userData->profile_image)}}" id="show_profile_image" width="50" height="50" class="img-circle">
						</div>
					</div>	
				</div>
			  
				<div class="form-group">
					<label class="col-sm-3 control-label">Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="name" name="name" value="{{$userData->name}}" placeholder="Enter Name" required>
					</div>				
				</div>
			  
				<div class="form-group">
					<label class="col-sm-3 control-label">Email</label>
					<div class="col-sm-9">
						<input type="email" class="form-control" id="email" name="email" value="{{$userData->email}}" placeholder="Enter Email" required>
					</div>				
				</div>	

				<div class="form-group">
					<label class="col-sm-3 control-label">Current Password</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="current_pwd" placeholder="Current Password">
						<span class="current-pwd-msg"></span>
					</div>				
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">New Password</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="new_pwd" placeholder="New Password">
					</div>				
				</div>
			  	
				<div class="form-group">
					<label class="col-sm-3 control-label">Confirm Password</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="confirm_pwd" placeholder="Confirm Password">
						<span class="conf-pwd-msg"></span>
					</div>				
				</div>
				
				<div class="text-center" style="margin-top:20px;margin-bottom:20px;">
					<button type="submit" class="btn btn-info btn-md">Submit</button>
				</div>
			  
			</form>
			
		</div>
	</div>
</div>

@endsection


