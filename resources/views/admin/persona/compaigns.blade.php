@extends('layouts.admin')
@section('content')

<div class="text-left outer_title">
	<h2>Campaign List</h2>
	<a href="javascript:void();" class="btn btn-info" data-toggle="modal" data-target="#add-compaign" style="float:right;">Add New Campaign</a>
</div>

<!-- For Alert -->
<br>
@if (\Session::has('success'))
<div class="alert alert-success">
	<p>{{ \Session::get('success') }}</p>
</div>
<br>
@endif
<!-- End For Alert -->

<div class="block">
	
	<div class="block-title">
		<h2><strong>{{ucfirst($brand_name)}}</strong></h2>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table_design" style="margin-bottom:0px;">
			<thead>
				<tr>
					<th>ID</th>
					<th>Campaign</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>

			<tbody>
			  @php $i = 1; @endphp
			  @foreach($compaign as $val)
				<tr>
				  <td>{{$i}}</td>
				  <td>{{ucfirst($val->compaign)}}</td>
				  <td>
					@if($val->status == 'I')
						{{'In-active'}}
					@else
					  {{'Active'}}
					@endif
				  </td>
				  <td>
					<a href="javascript:void(0)" class="btn btn-info btn-sm edit-compaign" data-toggle="modal" data-target="#edit-compaign" data-id="{{$val->_id}}" data-iname="{{$val->compaign}}" data-istatus="{{$val->status}}">Edit</a>
					@if($val->status == 'I')
						<button class="btn btn-info btn-sm deleteFun" data-value="A" data-table="Compaigns" data-target="{{$val->id}}" data-attr="Are you sure you want to Active this compaign?" data-href="Active Compaign" type="button">In-active</button>
					@else
					  <button class="btn btn-info btn-sm deleteFun" data-value="I" data-table="Compaigns" data-target="{{$val->id}}" data-attr="Are you sure you want to In-active this compaign?" data-href="In-active Compaign" type="button">Active</button>
					@endif
				  </td>				  
				</tr>
				@php $i++; @endphp
			  @endforeach
			</tbody>
		</table>
	</div>
	
</div>

@endsection

<!-- Add Campaign Modal -->
<div class="modal fade" id="add-compaign">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Campaign</h4>
			</div>
			<div class="modal-body">
				<div class="modal_box">
					<form method="post" action="{{url('backoffice/persona/add_compaign')}}">
						@csrf
						<input type="hidden" name="brandId" value="{{$brandId}}">
						<div class="form-group">
							<label class="control-label">Campaign</label>
							<input type="text" class="form-control" placeholder="Enter Campaign" required name="compaign">
						</div>
						<div class="form-group">
							<label class="control-label">Status</label>
							<select class="form-control" name="status" required>
								<option value="A">Active</option>
								<option value="I">In-Active</option>
							</select>
						</div>
						<div class="text-center" style="margin-top:30px;">
							<button type="submit" class="btn btn-info btn-lg">Submit</button>
						</div>						
					</form>
				</div>			
			</div>
		</div>
	</div>
</div>

<!-- Edit Campaign Modal -->
<div class="modal fade" id="edit-compaign">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Campaign</h4>
			</div>
			  <div class="modal-body">
				<form method="post" action="{{url('backoffice/persona/update_compaign/')}}">
					@csrf
					<input type="hidden" id="compaign-id" required name="compaign_id">
					<div class="form-group">
						<label class="control-label">Campaign</label>
						<input type="text" class="form-control" id="compaign-name" required name="compaign">
					</div>
					<div class="form-group">
						<label class="control-label">Status</label>
						<select class="form-control" name="status" id="compaign-status" required>
							<option value="A">Active</option>
							<option value="I">In-Active</option>
						</select>
					</div>
					<div class="text-center" style="margin-top:30px;">
						<button type="submit" class="btn btn-info btn-lg">Submit</button>
					</div>						
				</form>
			  </div>
		</div>
	</div>
</div>


