@extends('layouts.admin')
@section('content')
<?php
  use App\Brands;
  use App\Industries;
  use App\Compaigns;
?>

<div class="text-left outer_title">
	<h2>Persona List</h2>
	<a href="{{ url('backoffice/persona/add_persona') }}" class="btn btn-warning" style="float:right;">Add New Persona</a>
</div>

<br />
@if (\Session::has('success'))
  <div class="alert alert-success">
	<p>{{ \Session::get('success') }}</p>
  </div><br />
@endif

<div class="row" style="margin-top:30px;">
	@foreach($personas as $persona)
	<?php 
	  $brands = Brands::where('_id',$persona->brand)->first();
	  $compaigns = Compaigns::where('_id',$persona->compaign)->first();
	  $industries = Industries::where('_id',$persona->industry)->first();
	?>
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<input type="button" value="Assign Channel" class="btn btn-primary assign-persona-channel" data-attr="{{$persona->_id}}">
	  <div class="card persona_card">

		<h3 class="text-left">Persona : {{ucfirst($persona->persona_title)}}</h3>

		<table class="table">
			<tbody>
				<tr>
					<td class="text-left" style="width:30%;"><b>Brand</b></td>
					<td class="text-left">: {{ucfirst($brands->brand)}}</td>
				</tr>
				<tr>
						<td class="text-left" style="width:30%;"><b>Campaign</b></td>
						<td class="text-left">: {{ucfirst($compaigns->compaign)}}</td>
					</tr>
					<tr>
						<td class="text-left" style="width:30%;"><b>Industry</b></td>
						<td class="text-left">: {{ucfirst($industries->industry)}}</td>
					</tr>
					<tr>
						<td class="text-left" style="width:30%;"><b>Date of Creation</b></td>
						<td class="text-left">: {{date('m/d/Y',strtotime($persona->created_at))}}</td>
					</tr>
					<tr>
						<td class="text-left" style="width:30%;"><b>Actions</b></td>
						<td class="text-left">
							<a href="{{action('PersonaController@edit', $persona->id)}}" class="btn btn-info" title="Edit"><i class="fa fa-pencil"></i></a>
							<a href="{{action('PersonaController@content', $persona->id)}}" class="btn btn-info" title="Content"><i class="fa fa-info-circle"></i></a>
							<!-- <a href="{{$persona->url.'/test'}}" class="btn btn-info" title="Frontend"><i class="fa fa-street-view"></i></a> -->
							<a href="javascript:void(0)" data-attr="{{$persona->_id}}" class="btn btn-info show-frontend-url" title="Frontend"><i class="fa fa-street-view"></i></a>
							<a href="javascript:void(0);" class="btn btn-info duplicate-persona" title="Duplicate" data-attr="{{$persona->id}}"><i class="fa fa-files-o"></i></a>
							@if($persona->deleteStatus == 0)
							<button class="btn btn-info deleteFun" data-value="1" data-table="Personas" data-target="{{$persona->id}}" data-attr="Are you sure you want to Activate this Persona?" data-href="Activate Persona" type="button" title="Deactivate"><i class="fa fa-toggle-off"></i></button>
							@else
							<button class="btn btn-info deleteFun" data-value="0" data-table="Personas" data-target="{{$persona->id}}" data-attr="Are you sure you want to Deactivate this Persona?" data-href="Deactivate Persona" type="button" title="Activate"><i class="fa fa-toggle-on"></i></button>
							@endif							
						</td>
					</tr>
			</tbody>
		</table>		
	  </div>
	</div>
	@endforeach
 </div>

 <!----Frontend url Modal ---->

<div class="modal fade" id="frontend_url_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Frontend Url</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      	<form method="POST" action="{{url('backoffice/persona/persona_assign_channel')}}">
      		@csrf
	      	<div class="modal-body url-modal-body">
	      		
	        	
		   	</div>
	      	<div class="modal-footer">
	        	<button type="submit" class="btn btn-primary">Submit</button>
	      	</div>
	    </form>
    </div>
  </div>
</div>

@endsection