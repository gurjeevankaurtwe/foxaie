@extends('layouts.admin')
@section('content')
  <div class="container">
    <h2>Business Logic</h2>
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
	<div class="col-md-8">
        <!--<a href="#" class="btn btn-warning" >Add Response</a>-->
        <form name="logic-add" id="logic-add" method="post" enctype="multipart/form-data" action="{{action('BusinessLogicController@save_logic', $id)}}">
            @csrf 
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="type">Select User Input type</label>
                    <select class="form-control" id="input-type" name="input-type" required >
                        <option value="">Select</option>
                        <option value="Text">Text</option>
                        <option value="Radio">Radio</option>
                        <option value="Checkbox">Checkbox</option>
                        <option value="File">File</option>
                        <option value="Link">Link</option>
                        <option value="Form">Form</option>
                    </select>
                </div>
            </div>
			<div id="content-field"></div>
			<div class="row">
				<div class="form-group col-md-6">
					<label for="Answer">Select Response Type:</label>
					<select class="form-control" id="reponse-type" name="reponse-type" required >
						<option value="">Select</option>
						<option value="Text">Text</option>
						<option value="Radio">Radio</option>
						<option value="Checkbox">Checkbox</option>
						<option value="File">File</option>
						<option value="Link">Link</option>
						<option value="Form">Form</option>
					</select>
				</div>
			</div>
            <input name="persona-id" id="persona-id" type="hidden" value="{{$id}}" >
            <div id="content-btn-sub" class="row">
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
  </div>
@endsection