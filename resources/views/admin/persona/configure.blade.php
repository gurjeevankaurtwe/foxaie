<form method="post" action="{{url('backoffice/persona/save_content_configuration')}}">

	@csrf



	<?php 
		if($persona->channel == 'Facebook')
		{
			$data = $content->content; 
			if(array_key_exists('attachment',$data['message']))
			{
				$type =  'File';
			}
			else if(array_key_exists('quick_replies',$data['message']))
			{
				//echo 'Radio';
				$type =  'Radio';
			}
			else
			{
				$type =  ucfirst(key($data['message']));
			}

			if($type == 'Text')
			{
				$query = $data['message']['text'];
			}
			else if($type == 'File')
			{
				$query = $data['message']['attachment']['payload']['url'];
			}
			else if($type == 'Radio')
			{
				$query = $data['message']['text'];
			}
		} 
		else if($persona->channel == 'Twitter')
		{
			$data = $content->content; 
			if(array_key_exists('quick_reply',$data['event']['message_create']['message_data']))
			{
				$type =  'Radio';
			}
			else
			{
				$type =  ucfirst(key($data['event']['message_create']['message_data']));
			}
			if($type == 'Text')
			{
				$query = $data['event']['message_create']['message_data']['text'];
			}
			else if($type == 'Radio')
			{
				$query = $data['event']['message_create']['message_data']['text'];
			}
		}
		else
		{
			$data = json_decode($content->content);
			$type =  $data->type;
			$query = $data->query;
			
		}
	?>

	@if($type == "Radio")

		<p>{{$query}}</p>

		

		<div class="row">

			<div class="form-group">

				<label for="industry">Response For: </label>

				<select class="form-control" id="query-option-conf" name="query-option-conf" required>

					<option value="">Select</option>

					@if($persona->channel == 'Facebook')
						@foreach($data['message']['quick_replies'] as $val)
						
							<option value="{{$val['title']}}">{{$val['title']}}</option>
							
						@endforeach
					@elseif($persona->channel == 'Twitter')
						@foreach($data['event']['message_create']['message_data']['quick_reply']['options'] as $val)
						
							<option value="{{$val['label']}}">{{$val['label']}}</option>
							
						@endforeach
					@else
						@foreach($data->answer as $radio_option)
							<option value="{{$radio_option}}">{{$radio_option}}</option>
						@endforeach
					@endif
				</select>
			</div>

		</div>

		<div class="row">

			<div class="form-group">

				<label for="industry">Select Response Type: </label>

				@if($persona->channel == 'Facebook' || $persona->channel == 'Twitter')
	

					<select class="form-control response-type-conf" id="response-type-conf" name="response-type-conf" required>

						<option value="">Select</option>

						<option value="Text">Text</option>

						<option value="Radio">Quick Reply</option>

						<option value="File">File</option>

					</select>

				@else

					<select class="form-control response-type-conf" id="response-type-conf" name="response-type-conf" required>

						<option value="">Select</option>

						<option value="Text">Text</option>

						<option value="Textarea">Textarea</option>

						<option value="Button">Button</option>

						<option value="Radio">Radio</option>

						<option value="Checkbox">Checkbox</option>

						<option value="File">Image</option>

						<option value="Docs">File</option>

						<option value="Link">Link</option>

						<option value="Form">Form</option>

						<option value="Embed">Embed</option>

					</select>

				@endif

			</div>

		</div>

		<div class="row">

			<div class="form-group">

				<label for="industry">Select Response: </label>

				<select class="form-control" id="query-type-conf" name="query-type-conf" required>

				</select>

			</div>

		</div>

	@else

		<div class="row">

			<div class="form-group">

				@if($type == 'Embed' || $type == 'Textarea')
					<p>Response For: {!!$query!!}</p>
				@else
					<p>Response For: {{$query}}</p>
				@endif

				

			</div>

		</div>

		<div class="row">

			<div class="form-group">

				<label for="industry">Select Response Type: </label>

				@if($persona->channel == 'Facebook' || $persona->channel == 'Twitter')
	

					<select class="form-control response-type-conf" id="response-type-conf" name="response-type-conf" required>

						<option value="">Select</option>

						<option value="Text">Text</option>

						<option value="Radio">Quick Reply</option>

						<option value="File">File</option>

					</select>

				@elseif($persona->channel == 'Website')

					<select class="form-control response-type-conf" id="response-type-conf" name="response-type-conf" required>

						<option value="">Select</option>

						<option value="Text">Text</option>

						<option value="Textarea">Textarea</option>

						<option value="Button">Button</option>

						<option value="Radio">Radio</option>

						<option value="Checkbox">Checkbox</option>

						<option value="File">Image</option>

						<option value="Docs">File</option>

						<option value="Link">Link</option>

						<option value="Form">Form</option>

						<option value="Embed">Embed</option>

					</select>

				@endif


			</div>

		</div>

		<div class="row">

			<div class="form-group">

				<label for="industry">Select Response: </label>

				<select class="form-control" id="query-type-conf" name="query-type-conf" required>

					<option value="">Select</option>

				</select>

			</div>

		</div>		

	@endif

	<input type="hidden" name="type" id="type" value="{{$type}}">

	<input type="hidden" name="content_id" id="conf_content_id" value="{{$content->_id}}">

	<input type="hidden" name="persona_id" id="conf_persona_id" value="{{$content->persona_id}}">

	<div id="content-btn-sub" class="row">

		<div class="form-group col-md-4">

			<button type="submit" class="btn btn-success">Submit</button>

		</div>

	</div>

</form>



<script>

$("#response-type-conf").change(function(){

	var newValue = $(this).children("option:selected").val();

	var perId = $('#conf_persona_id').val();

	$.ajax( {

		method: "GET",

		url:"{{url('backoffice/persona/get_configure_content')}}",

		data: { type: newValue, pers_id: perId },

		success:function(data) {

			$('#query-type-conf').html(data);

		}

	});

});	



</script>