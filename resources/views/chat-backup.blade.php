<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Chat Bot</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="{{asset('admin/chat-img/logo/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/plugins.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/chat.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/themes.css')}}">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="{{asset('admin/js/vendor/modernizr-respond.min.js')}}"></script>
        <style>
            body {
            background: #f5f5f5 !important;
            font-family: 'Poppins', sans-serif;

            }
            .card-body
            {
            	background-color: white!important;
            }
        </style>
    </head>
    <body>
    <?php
    	if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';

	    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
	    preg_match("/[^\/]+$/", $actual_link, $matches);
		$last_word = $matches[0];
		$videoLink = asset('admin/chat-videos/FoxWebpageWhiteBackground.mp4');
		$imgLogo = asset('admin/chat-img/logo/logo.png');
		$userImg = asset('admin/chat-img/other/user.png');


	?>
	
	<!-- <div style="position: fixed; bottom: 0; z-index: -99; width: 100%; height: 100%"> -->
		
		
	<!-- </div> -->
	<!-- <iframe height="100%" width="100%"  src="https://www.youtube.com/embed/X_PSckP7BKo?autoplay=1&loop=1&playlist=X_PSckP7BKo&mute=1&enablejsapi=1&controls=0&showinfo=0&autohide=1" loop="true" frameborder="0" allow="autoplay" allowfullscreen></iframe> -->

	<div class="page_wrapper">
		<video  id="myvideo"   style="position: fixed;z-index: -1; right: 0; bottom: 0; max-width: 100%; min-height: 100%;" autoplay muted>
		  <source class="active" src="{{asset('admin/chat-videos/Fox Webpage 1.mp4')}}" type="video/mp4">
		  <source id="first_video" src="{{asset('admin/chat-videos/FoxWebpageWhiteBackground.mp4')}}" type="video/mp4">
		</video>
		<!-- Header -->
		<div class="header">
		  <a href="javascript:void();" id="chat_logo">
		  	<img src="{{asset('admin/chat-img/logo/Logo white.png')}}" id="imagLogo" alt="logo">
		  </a>
		  <a href="https://www.foxaie.com/" target="_blank"><button type="button" class="btn bg_orange pull-right">About Us</button></a>
		</div>
		<!-- End Header -->
		
		<!-- Chat Panel -->
		<div class="chat_panel mt-3" style="display:none; ">
			<div class="card" style="
    margin-top: 100px; ">
				
				<div class="card-header" style="border-top-left-radius: 5px; border-top-right-radius: 5px;"><img src="{{asset('admin/chat-img/logo/logo-white.png')}}">
					<!-- <button type="button" class="btn btn-secondary pull-right btn-sm">Logout</button> --></div>
				
				<div class="card-body chat_box_wrapper">					
					<div id="chat-messege">
						<div class="chat_content">
							<div class="media">
							  <div class="media-left"></div>					
							  <div class="media-body"></div>
							</div>
						</div>
					</div>										
				</div>
				
				<div class="card-footer chat_footer" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
					<div class="field"><input type="text" name="try-chat-inputbox" id="try-chat-inputboxes" placeholder="Type Your Message"></div>
										
					<!-- <div class="paperclip mr-2">
					<form enctype="multipart/form-data" id="upload_form" name="upload_form">
					<label for="file-upload"><img src="{{asset('admin/chat-img/icon/paperclip.png')}}"></label>
					<input id="file-upload" name="file-upload" type="file" onchange="file_upload()" style="display: none;">
					</form>
					</div>
					
					<div class="microphone"><img src="{{asset('admin/chat-img/icon/microphone.png')}}"></div> -->
				</div>
				
			</div>
		</div>
		<!-- End Chat Panel -->
		
		<!-- Chat Bottom icon -->
		<div class="bottom_icon">
			<div class="icon_card">
				<div class="media">
					<div class="media-left"><img src="{{asset('admin/chat-img/icon/fox-white-icon.png')}}"></div>
					<div class="media-body">
						<h5>FoxAie</h5>
					</div>
					<span class="close_btn"><i class="fa fa-times"></i></span>
				</div>
				<p class="chat_btn" onclick="start_try_chat_backup();">Interact Now</p>
			</div>
			<div class="icon_img">
				<a class="chat_btn" href="javascript:void();" onclick="start_try_chat_backup();">
					<img src="{{asset('admin/chat-img/icon/fox-white-icon.png')}}">
				</a>
				<i class="fa fa-times" style="display:none;"></i>
			</div>
		</div>
	</div>


	<!-- Start new chat modal -->
	<div class="modal fade" id="chatModalB" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Chat</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        You want to start with previous chat?
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg_orange no-previous-chat">No</button>
	        <button type="button" class="btn bg_orange yes-previous-chat">Yes</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>
	<script src="{{asset('admin/js/vendor/bootstrap.min.js')}}"></script>
	<script src="{{asset('admin/js/plugins.js')}}"></script>
	<script src="//maps.google.com/maps/api/js?sensor=true"></script>
	<script src="{{asset('admin/js/helpers/gmaps.min.js')}}"></script>
	<script src="{{asset('admin/js/helpers/ckeditor/ckeditor.js')}}"></script>
	<script src="//www.youtube.com/iframe_api"></script>


	<?php 
		if(isset($persona->_id))
		{
			$perID = $persona->_id;
		}
		else
		{
			$perID = '';
		}
	?>
	
<script>
	var videolink = '<?php echo $videoLink;?>';
	var imgLogo = '<?php echo $imgLogo;?>';
	var myvid = document.getElementById('myvideo');
	var userImg = '<?php echo $userImg;?>'
	myvid.addEventListener('ended', function(e) 
	{

		var activesource = document.querySelector("#myvideo source.active");
	 	var nextsource = document.getElementById("first_video");
	 	if(videolink == nextsource.src)
		{
			$('#imagLogo').attr('src',imgLogo);
			activesource.className = "";
	  		nextsource.className = "active";
	  		myvid.src = nextsource.src;
	  		myvid.play();
	  	}
	});

	/*========Start Embed you tube video with loop========*/

	
        var videoIDs = [
	        'o4D7b3XVq-o',
	        'X_PSckP7BKo'
	    ];

	    var player, currentVideoId = 0;

	    function onYouTubeIframeAPIReady() {

	        player = new YT.Player('player', {
	        	height: '100%',
	            width: '100%',
	            videoId: 'X_PSckP7BKo',
	            playerVars: {autoplay:1,loop:1},

	            events: {
	                'onReady': onPlayerReady,
	                'onStateChange': onPlayerStateChange
	            }
	        });
	    }

	    function onPlayerReady(event) {
	        event.target.loadVideoById(videoIDs[currentVideoId]);
	    }

	    function onPlayerStateChange(event) {
	    	
	        if (event.data == YT.PlayerState.ENDED) {
	        	currentVideoId++;
	           // if (currentVideoId < videoIDs.length) {
	                player.loadVideoById('X_PSckP7BKo');
	           // }
	        }
	    }

	/*=====End======*/

   

	
    /**
     * Put your video IDs in this array
     */
   
       
    $(document).ready(function (e) {

      	$('.bottom_icon .icon_img a').click(function () {
	        $('.bottom_icon .icon_img a').hide();
	        $('.bottom_icon .icon_img i').show();
	        $('.chat_panel').show();
			$('.icon_card').hide();
	  	});

	  	$('.bottom_icon .icon_img i').click(function () {
	        $('.bottom_icon .icon_img a').show();
	        $('.bottom_icon .icon_img i').hide();
	        $('.chat_panel').hide();
			$('.icon_card').show();
	  	});

	  	

	  	$('.bottom_icon .icon_card .close_btn').click(function () {
	        $('.icon_card').hide();
	  	});
	});

/*======submit marketing form when user chat=======*/

	function submitForm(id)
	{
		var formData = new FormData(id);
		jQuery.ajax({
            type: 'post',
            cache: false,
			processData: false,
			contentType: false,
			headers: {
				'X-CSRF-TOKEN':"{{csrf_token()}}",
			},
			url: "{{url('backoffice/persona/add_form_data')}}",
			data:formData,
			dataType : "json",
            success: function (data) {
            	if(data.success == true)
            	{
            		chatBackupTry();
            	}
            }
        });
		
	}

	function file_upload()
	{
		var formData = new FormData($("#upload_form")[0]);
		$.ajax({
            type: 'post',
			processData: false,
			contentType: false,
			enctype: 'multipart/form-data',
			headers: {
				'X-CSRF-TOKEN':"{{csrf_token()}}",
			},
			url: "{{url('backoffice/persona/upload_file')}}",
			data:formData,
            success: function (data) {
              var messege = $('#try-chat-inputbox').val(data);
			  chatTry();
            }
        });
	}
		
	var chat_id="";
	var return_first;
	function callback(response) {
		return_first = response;
	}
	var rowNum = 0; 
	var user_id='' ;

	jQuery(document).on('click','.response-btntype',function(){
		jQuery(this).attr('disabled',true);
		var btnthis = jQuery(this);
		var btnVal = jQuery(this).val();
		btnChatBackupTry(btnthis,btnVal);
	});

	function btnChatBackupTry(btnthis,btnVal)
	{
		btnthis.val('Processing...');
		rowNum++;
		var last_msg_det = $("#chat-messege .chat_content").last().attr("id");
		var lastmsgtype = last_msg_det.split('_');
		if(lastmsgtype[2] == "Text")
		{
			var messege = $('#try-chat-inputboxes').val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
		}
		else if(lastmsgtype[2] == "Radio")
		{
			var messege = $("input[name='"+lastmsgtype[3]+"']:checked").val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
		}
		else if(lastmsgtype[2] == "Form")
		{
			var messege = 'Submit successfully';
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
		}
		else
		{
			var messege = $('#try-chat-inputboxes').val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
		}
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(messege))
		{
			user_id = reg_user(messege,personaId)	;
		}
		$('#try-chat-inputboxes').val('');
		$('.chat_box_wrapper').animate({scrollTop: $('.chat_box_wrapper').prop("scrollHeight")}, 500);
		var urlType = '<?php echo $last_word;?>';
		$.ajax({
			method: "GET", 
			url:"{{url('backoffice/persona/try_chat_backup_response')}}",
			data: { urlType:urlType,user_msg:messege, msg_pos:rowNum, persona_id:personaId, msg_det:last_msg_det, user_id:user_id},
			dataType : "json",
			success:function(data) 
			{
				/*$('.embed_content').animate({ scrollTop: 0 }, "slow");
				if(data.type == 'Embed')
				{
					console.log($('.embed_content').offset());
					$('body, html').animate({
				        scrollTop: $('.embed_content').offset().top - $(window).height() - 100
				    }, 1000);
					
				}*/
				jQuery('.response-btntype').val(btnVal);
				if(data.type == 'Radio' || data.type == 'Checkbox' || data.type == 'Button' || data.type == 'Embed')
				{

					$('#try-chat-inputboxes').prop('disabled',true);
				}
				else
				{
					$('#try-chat-inputboxes').prop('disabled',false);
				}
				$('#chat-messege').append(data.html);
				$('#try-chat-inputboxes').val('');
				$('.chat_box_wrapper').animate({scrollTop: $('.chat_box_wrapper').prop("scrollHeight")}, 300);
			}
		});
	}

		
	function chatBackupTry()
	{
		rowNum++;
		var last_msg_det = $("#chat-messege .chat_content").last().attr("id");
		var lastmsgtype = last_msg_det.split('_');
		console.log(lastmsgtype);
		if(lastmsgtype[2] == "Text")
		{
			var messege = $('#try-chat-inputboxes').val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
			
			var messageHTML = '<div id="msg-text-'+rowNum+'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="'+userImg+'"></div><div class="media-body"><small>'+cr_time+'</small><div class="chat_txt bg_brown">'+messege+'</div></div></div></div>'; 
			$('#chat-messege').append(messageHTML);
		}
		else if(lastmsgtype[2] == "Radio")
		{
			$("input[name='"+lastmsgtype[3]+"']").attr('disabled',true);
			var messege = $("input[name='"+lastmsgtype[3]+"']:checked").val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
			var messageHTML = '<div id="msg-text-'+rowNum+'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="'+userImg+'"></div><div class="media-body"><small>'+cr_time+'</small><div class="chat_txt bg_brown">'+messege+'</div></div></div></div>';
			$('#chat-messege').append(messageHTML);
		}
		else if(lastmsgtype[2] == "Form")
		{
			var messege = 'Submit successfully';
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
			var messageHTML = '<div id="msg-text-'+rowNum+'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="'+userImg+'"></div><div class="media-body"><small>'+cr_time+'</small><div class="chat_txt bg_brown">Submit successfully</div></div></div></div>';
			$('#chat-messege').append(messageHTML);
		}
		else
		{
			var messege = $('#try-chat-inputboxes').val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
			var messageHTML = '<div id="msg-text-'+rowNum+'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="'+userImg+'"></div><div class="media-body"><small>'+cr_time+'</small><div class="chat_txt bg_brown">'+messege+'</div></div></div></div>';
			$('#chat-messege').append(messageHTML);
		}
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(messege))
		{
			user_id = reg_user(messege,personaId)	;
		}
		$('#try-chat-inputboxes').val('');
		$('.chat_box_wrapper').animate({scrollTop: $('.chat_box_wrapper').prop("scrollHeight")}, 500);
		var urlType = '<?php echo $last_word;?>';
		$.ajax({
			method: "GET", 
			url:"{{url('backoffice/persona/try_chat_backup_response')}}",
			data: { urlType:urlType,user_msg:messege, msg_pos:rowNum, persona_id:personaId, msg_det:last_msg_det, user_id:user_id},
			dataType : "json",
			success:function(data) 
			{
				if(data.type == 'Radio' || data.type == 'Checkbox' || data.type == 'Button' || data.type == 'File')
				{
					
					$('#try-chat-inputboxes').prop('disabled',true);
				}
				else
				{
					$('#try-chat-inputboxes').prop('disabled',false);
				}

				$('#chat-messege').append(data.html);
				
				
				$('#try-chat-inputboxes').val('');
				$('.chat_box_wrapper').animate({scrollTop: $('.chat_box_wrapper').prop("scrollHeight")}, 500);
			}
		});
	}
	function reg_user(eid,pid)
	{
		var retvar;
		$.ajax({
			method: "GET", 
			async: false ,
			url:"{{url('backoffice/persona/register_chat_user')}}",
			data: { user_msg:eid,personaId:pid,ip:'<?php echo $ipaddress;?>'}, 
			success:function(data) 
			{
				var obj = jQuery.parseJSON(data);
				retvar = obj.email;
			}
		});
		return retvar ;	
	}
	function chat_backup()
	{
		var chat=document.getElementById("chat-messege").innerHTML;
		$.ajax({
			method: "POST",			
			headers: {
				'X-CSRF-TOKEN':"{{csrf_token()}}",
			},
			url:"{{url('backoffice/persona/chat_backup')}}",
			data: {chat_id:chat_id,user_email:user_id,chat:chat}, 
			success:function(data) 
			{
				chat_id = data;
			}
		});
	}
	function start_try_chat_backup()
	{
		checkUserIp()
		
	}

/*=========check user ip exist or not for chat========*/

	function checkUserIp()
	{
		jQuery('.chat_btn').html('Processing...');
		var ip = '<?php echo $ipaddress;?>';
		var personaId = '<?php echo $perID;?>';
		var urlType = '<?php echo $last_word;?>';
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/check_user_ip')}}",
			data: {ip:ip,personaId:personaId,urlType:urlType},
			success:function(data){
				jQuery('.chat_btn').html('Interact Now');
				if(data == 1)
				{
					jQuery('#chatModalB').modal('show');
				}
				else
				{
					startNewChat();
				}
			}
		});
	}

/*==========Start new chat if no same ip chat found on databse========*/

	function startNewChat()
	{
		//$('.bottom_icon .icon_card .chat_btn').click(function () {
	        $('.bottom_icon .icon_img a').hide();
	        $('.bottom_icon .icon_img i').show();
	        $('.chat_panel').show();
			$('.icon_card').hide();
	  	//});
		var personaId = '{{$perID}}';
		var urlType = '<?php echo $last_word;?>';
		jQuery('.chat_btn').html('Processing...');
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_chat_backup_response')}}",
			data: {persona_id:personaId,urlType:urlType},
			dataType : "json",
			success:function(data)
			{
				jQuery('.chat_btn').html('Interact Now');
				if(data.type == 'Radio' || data.type == 'Checkbox' || data.type == 'Button' || data.type == 'File')
				{
					
					$('#try-chat-inputboxes').prop('disabled',true);
				}
				else
				{
					$('#try-chat-inputboxes').prop('disabled',false);
				}
				$('#chat-messege').append(data.html);
			}
		});
	}

/*==========On click yes user previous chat shows=========*/

	jQuery(document).on('click','.yes-previous-chat',function(){
		jQuery('#chatModalB').modal('hide');
		startNewChat();
		countPreviousChat();
	});

/*==========On click no delete user previous chat =========*/

	jQuery(document).on('click','.no-previous-chat',function(){
		jQuery('#chatModalB').modal('hide');
		countPreviousChat();
		deletePreviousChat();
		startNewChat();
	});

	function deletePreviousChat()
	{
		var ip = '<?php echo $ipaddress;?>';
		var personaId = '<?php echo $perID;?>';
		var urlType = '<?php echo $last_word;?>';
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/delete_previous_chat')}}",
			data: {ip:ip,personaId:personaId,urlType:urlType},
			success:function(data){
				
			}
		});
	}

/*=======update chat count if user chat multiple time of same persona========*/

	function countPreviousChat()
	{
		var ip = '<?php echo $ipaddress;?>';
		var personaId = '<?php echo $perID;?>';
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/update_previous_chat_count')}}",
			data: {ip:ip,personaId:personaId},
			success:function(data){
				
			}
		});
	}
	
	$('#try-chat-inputboxes').keypress(function(event)
	{
		var keycode = (event.keyCode ? event.keyCode : event.which);
	  	if(keycode == '13')
	  	{
		  chatBackupTry();
	  	}
		event.stopPropagation();
	});
</script>
</body>
</html>