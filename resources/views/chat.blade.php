<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Chat Bot</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="{{asset('admin/chat-img/logo/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/plugins.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/chat.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/themes.css')}}">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="{{asset('admin/js/vendor/modernizr-respond.min.js')}}"></script>
        <style>
            body {
            background: #f5f5f5 !important;
            font-family: 'Poppins', sans-serif;
            }
        </style>
    </head>
    <body>
	<div class="page_wrapper">
		<!-- Header -->
			<div class="header">
			  <a href="javascript:void();"><img src="{{asset('admin/chat-img/logo/logo.png')}}" alt="logo"></a>
			</div>
		<!-- End Header -->
		
		<!-- Chat Panel -->
		<div class="chat_panel mt-3" style="display:none;">
			<div class="card">
				
				<div class="card-header"><img src="{{asset('admin/chat-img/logo/logo-white.png')}}"></div>
				
				<div class="card-body chat_box_wrapper">					
					<div id="chat-messege">
						<div class="chat_content">
							<div class="media">
							  <div class="media-left"></div>					
							  <div class="media-body"></div>
							</div>
						</div>
					</div>										
				</div>
				
				<div class="card-footer chat_footer">
					<div class="field"><input type="text" name="try-chat-inputbox" id="try-chat-inputbox" placeholder="Type Your Message"></div>
										
					<div class="paperclip mr-2">
					<form enctype="multipart/form-data" id="upload_form" name="upload_form">
					<label for="file-upload"><img src="{{asset('admin/chat-img/icon/paperclip.png')}}"></label>
					<input id="file-upload" name="file-upload" type="file" onchange="file_upload()" style="display: none;">
					</form>
					</div>
					
					<div class="microphone"><img src="{{asset('admin/chat-img/icon/microphone.png')}}"></div>
				</div>
				
			</div>
		</div>
		<!-- End Chat Panel -->
		
		<!-- Chat Bottom icon -->
		<div class="bottom_icon">
			<div class="icon_card">
				<div class="media">
					<div class="media-left"><img src="{{asset('admin/chat-img/icon/fox-white-icon.png')}}"></div>
					<div class="media-body">
						<h5>Foxaie</h5>
						<p>How can We help You ?</p>
					</div>
					<span class="close_btn"><i class="fa fa-times"></i></span>
				</div>
				<p class="chat_btn" onclick="start_try_chat();">Chat Now</p>
			</div>
			<div class="icon_img">
				<a href="javascript:void();" onclick="start_try_chat();"><img src="{{asset('admin/chat-img/icon/fox-white-icon.png')}}"></a>
				<i class="fa fa-times" style="display:none;"></i>
			</div>
		</div>
	</div>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>
	<script src="{{asset('admin/js/vendor/bootstrap.min.js')}}"></script>
	<script src="{{asset('admin/js/plugins.js')}}"></script>
	<script src="//maps.google.com/maps/api/js?sensor=true"></script>
	<script src="{{asset('admin/js/helpers/gmaps.min.js')}}"></script>
	<script src="{{asset('admin/js/helpers/ckeditor/ckeditor.js')}}"></script>
	<?php 
		if(isset($persona->_id))
		{
			$perID = $persona->_id;
		}
		else
		{
			$perID = '';
		}
	?>
	
<script>
    $(document).ready(function (e) {

      	$('.bottom_icon .icon_img a').click(function () {
	        $('.bottom_icon .icon_img a').hide();
	        $('.bottom_icon .icon_img i').show();
	        $('.chat_panel').show();
			$('.icon_card').hide();
	  	});

	  	$('.bottom_icon .icon_img i').click(function () {
	        $('.bottom_icon .icon_img a').show();
	        $('.bottom_icon .icon_img i').hide();
	        $('.chat_panel').hide();
			$('.icon_card').show();
	  	});

	  	$('.bottom_icon .icon_card .chat_btn').click(function () {
	        $('.bottom_icon .icon_img a').hide();
	        $('.bottom_icon .icon_img i').show();
	        $('.chat_panel').show();
			$('.icon_card').hide();
	  	});

	  	$('.bottom_icon .icon_card .close_btn').click(function () {
	        $('.icon_card').hide();
	  	});
	});

/*======submit marketing form when user chat=======*/

	function submitForm(id)
	{
		var formData = new FormData(id);
		jQuery.ajax({
            type: 'post',
            cache: false,
			processData: false,
			contentType: false,
			headers: {
				'X-CSRF-TOKEN':"{{csrf_token()}}",
			},
			url: "{{url('backoffice/persona/add_form_data')}}",
			data:formData,
			dataType : "json",
            success: function (data) {
            	if(data.success == true)
            	{
            		chatTry();
            	}
            }
        });
		
	}

	function file_upload()
	{
		var formData = new FormData($("#upload_form")[0]);
		$.ajax({
            type: 'post',
			processData: false,
			contentType: false,
			enctype: 'multipart/form-data',
			headers: {
				'X-CSRF-TOKEN':"{{csrf_token()}}",
			},
			url: "{{url('backoffice/persona/upload_file')}}",
			data:formData,
            success: function (data) {
              var messege = $('#try-chat-inputbox').val(data);
			  chatTry();
            }
        });
	}
		
	var chat_id="";
	var return_first;
	function callback(response) {
		return_first = response;
	}
	var rowNum = 0; 
	var user_id='' ;
		
	function chatTry()
	{
		rowNum++;
		var last_msg_det = $("#chat-messege .chat_content").last().attr("id");
		var lastmsgtype = last_msg_det.split('_');
		if(lastmsgtype[2] == "Text")
		{
			var messege = $('#try-chat-inputbox').val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
			
			var messageHTML = '<div id="msg-text-'+rowNum+'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="media-body"><small>'+cr_time+'</small><div class="chat_txt bg_brown">'+messege+'</div></div></div></div>'; 
			$('#chat-messege').append(messageHTML);
		}
		else if(lastmsgtype[2] == "Radio")
		{
			var messege = $("input[name='"+lastmsgtype[3]+"']:checked").val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
			var messageHTML = '<div id="msg-text-'+rowNum+'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="media-body"><small>'+cr_time+'</small><div class="chat_txt bg_brown">'+messege+'</div></div></div></div>';
			$('#chat-messege').append(messageHTML);
		}
		else if(lastmsgtype[2] == "Form")
		{
			var messege = $("#form_"+lastmsgtype[3]).serialize();;
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
			var messageHTML = '<div id="msg-text-'+rowNum+'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="media-body"><small>'+cr_time+'</small><div class="chat_txt bg_brown">Sumit successfully</div></div></div></div>';
			$('#chat-messege').append(messageHTML);
		}
		else
		{
			var messege = $('#try-chat-inputbox').val();
			var personaId = '{{$perID}}';
			var dt = new Date();
			var cr_time = dt.getHours() + ":" + dt.getMinutes();
			var messageHTML = '<div id="msg-text-'+rowNum+'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="media-body"><small>'+cr_time+'</small><div class="chat_txt bg_brown">'+messege+'</div></div></div></div>';
			$('#chat-messege').append(messageHTML);
		}
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(messege))
		{
			user_id = reg_user(messege)	;
		}
		$('#try-chat-inputbox').val('');
		$('.chat_box_wrapper').animate({scrollTop: $('.chat_box_wrapper').prop("scrollHeight")}, 500);
		$.ajax({
			method: "GET", 
			url:"{{url('backoffice/persona/try_chat_response')}}",
			data: { user_msg:messege, msg_pos:rowNum, persona_id:personaId, msg_det:last_msg_det, user_id:user_id},
			success:function(data) 
			{
				console.log('fds');
				$('#chat-messege').append(data);
				$('#try-chat-inputbox').val('');
				$('.chat_box_wrapper').animate({scrollTop: $('.chat_box_wrapper').prop("scrollHeight")}, 500);
				chat_backup();
			}
		});
	}
	function reg_user(eid)
	{
		var retvar;
		$.ajax({
			method: "GET", 
			async: false ,
			url:"{{url('backoffice/persona/register_chat_user')}}",
			data: { user_msg:eid}, 
			success:function(data) 
			{
				var obj = jQuery.parseJSON(data);
				retvar = obj.email;
			}
		});
		return retvar ;	
	}
	function chat_backup()
	{
		var chat=document.getElementById("chat-messege").innerHTML;
		$.ajax({
			method: "POST",			
			headers: {
				'X-CSRF-TOKEN':"{{csrf_token()}}",
			},
			url:"{{url('backoffice/persona/chat_backup')}}",
			data: { chat_id:chat_id,user_email:user_id,chat:chat}, 
			success:function(data) 
			{
				chat_id = data;
			}
		});
	}
	function start_try_chat()
	{
		var personaId = '{{$perID}}';
		$.ajax({
			method: "GET",
			url:"{{url('backoffice/persona/get_chat_response')}}",
			data: {persona_id:personaId},
			success:function(data){
				$('#chat-messege').append(data);
			}
		});
	}
	$('#try-chat-inputbox').keypress(function(event)
	{
		var keycode = (event.keyCode ? event.keyCode : event.which);
	  	if(keycode == '13')
	  	{
		  chatTry();
	  	}
		event.stopPropagation();
	});
</script>
</body>
</html>