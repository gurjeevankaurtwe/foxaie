<?php
	namespace App;
	use Illuminate\Notifications\Notifiable;
	use Illuminate\Contracts\Auth\MustVerifyEmail;
	use Jenssegers\Mongodb\Auth\User as Authenticatable ;
	class UserChatBackup extends Authenticatable
	{
		public function userChatMsg()
	    {
	        return $this->hasMany('App\UserChatBackupMsg','chatId','_id');
	    }
	    public function personaDetail()
	    {
	        return $this->belongsTo('App\Personas','persona_id','_id');
	    }
	    public function chatMsg()
	    {
	        return $this->hasMany('App\UserChatBackupMsg','chatId','_id')->orderBy('_id','DESC');
	    }
	}

