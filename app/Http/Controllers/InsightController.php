<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Builder;
use App\FormSubmissions;
use App\Brands;
use App\Personas;
use App\Industries;
use App\UserChatBackup;
use App\Compaigns;
use DB;
use Carbon\Carbon;

class InsightController extends Controller
{

	public function index()
	{
		$brands = Brands::where('status','A')->get();
		return view('admin/insight/insight',compact('brands'));
	}
	
	public function compaignForm()
	{
		$brands = Brands::where('status','A')->get();
		$industry = Industries::where('status','A')->get();
		return view('admin/insight/compaign-form',compact('brands','industry'));
	}
	
	/*======Get persona with brand,industry and compaign===========*/

    public function getPersonaTitle()
    {
        $brandId = $_GET['brandId'];
        $industry = $_GET['industryId'];
        $compaignId = $_GET['compaignId'];
        $persona = Personas::where('brand', $brandId)->where('industry',$industry)->where('compaign',$compaignId)->get();
        $html = '<option value="">Select Compaign Detail...</option>';
        foreach($persona as $val)
        {
            $html .= '<option value="'.$val->_id.'">'.$val->persona_title.'</option>';
        }
        echo $html;
    }

    public function personasDetail()
    {
    	$brandId = $_GET['brandId'];
        $industry = $_GET['industryId'];
        $compaignId = $_GET['compaignId'];
        $persona = Personas::where('brand', $brandId)->where('industry',$industry)->where('compaign',$compaignId)->get();
        $html = '';
        foreach($persona as $val)
        {
            $html .= '<div class="col-sm-4">
						<a href="javascript:void();" data-attr="'.$val->_id.'" data-target="'.ucfirst($val->persona_title).'" class="camptype_link personasDetails">
							<div class="camptype_box">
								<div class="brand_logo">
									<img src="'.asset("/uploads/user-images/".$val->persona_image).'" alt="logo" width="50px">
								</div>
								<div class="camptype_txt">
									<p>'.$val->persona_title.'</p>
								</div>
							</div>
						</a>
					</div>';
        }
        echo $html;
    }

    /*=============On submit compaign form redirect to compaign overview======*/

    public function getCompaignOverview(Request $request)
    {
    	return redirect('backoffice/insights/compaign_overview/'.$request->input('personaId'));
    }

    /*=============Compaign overview=======*/

    public function compaignOverview($personaId=NULL,$type=NULL)
	{
		$web = '';
		if(isset($_GET['type']))
		{
			$web = $_GET['type'];
		}
		$pId = $personaId;
		$persona = Personas::where('_id', $pId)->first();
		$brand = Brands::where('_id',$persona->brand)->first();
		$industry = Industries::where('_id',$persona->industry)->first();
		$compaignType = Compaigns::where('_id',$persona->compaign)->first();
		$lifetimeVisitors = UserChatBackup::where('persona_id',$pId)
							->where(function($query) use ($type)  {
					            if($type != '') 
					            {
					                $query->where('url_type', $type);
					            }
					         })
							->count();
		$monthlyVisitors = UserChatBackup::where('persona_id',$pId)
							
							->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
							->where(function($query) use ($type)  {
					            if($type != '') 
					            {
					                $query->where('url_type', $type);
					            }
					         })
							->count();
		$weeklyVisitors = UserChatBackup::where('persona_id',$pId)
							
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->where(function($query) use ($type)  {
					            if($type != '') 
					            {
					                $query->where('url_type', $type);
					            }
					         })
							->count();
		/*$yearlyRecord = UserChatBackup::where('persona_id',$pId)->get()
							
            				->groupBy(function($val) {
            					return Carbon::parse($val->created_at)->format('Y');
							});*/
		$monthlyRecord =  UserChatBackup::where('persona_id',$pId)
							->where(function($query) use ($type)  {
					            if($type != '') 
					            {
					                $query->where('url_type', $type);
					            }
					         })
							->get()
							
							->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])

            				->groupBy(function($d) {
            					return Carbon::parse($d->created_at)->format('m');
							});
		/* $weeklyRecord =  UserChatBackup::where('persona_id',$pId)->get()
		 					
    						->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            				->groupBy(function($date) {
            					return Carbon::parse($date->created_at)->format('D');
							});*/
		return view('admin/insight/compaign-overview',compact('pId','persona','brand','compaignType','industry','lifetimeVisitors','monthlyVisitors','weeklyVisitors','monthlyRecord'));
	}

	public function getMonthlyGraph()
	{
		$startDate = date('Y-m-d',strtotime($_GET['startDate']));
		$endDate = date('Y-m-d',strtotime($_GET['endDate']));
		$pId = $_GET['personaId'];
		$type = 'web';
		$monthlyRecord 	=  	UserChatBackup::where('persona_id',$pId)->whereBetween(
				             	'created_at', array(
				                 	Carbon::createFromFormat('Y-m-d',$startDate),
				                 	Carbon::createFromFormat('Y-m-d',$endDate)
				             	)
         					)->get();
		return view('admin/insight/monthly-graph',compact('monthlyRecord'));
    }

    public function getMonthlyParGraph()
    {
    	$startDate = date('Y-m-d',strtotime($_GET['startDate']));
		$endDate = date('Y-m-d',strtotime($_GET['endDate']));
		$pId = $_GET['personaId'];
		$type = 'web';
		$monthlyRecord 	=  	UserChatBackup::where('persona_id',$pId)->whereBetween(
				             	'created_at', array(
				                 	Carbon::createFromFormat('Y-m-d',$startDate),
				                 	Carbon::createFromFormat('Y-m-d',$endDate)
				             	)
         					)->get();
		return view('admin/insight/monthly-par-graph',compact('monthlyRecord'));
    }

	/*=============Compaign participants============*/

	public function compaignParticipants($personaId)
	{
		$pId = $personaId;
		$persona = Personas::where('_id', $pId)->first();
		$compaign = Compaigns::where('_id',$persona->compaign)->first();
		$brand = Brands::where('_id',$persona->brand)->first();
		$personaChat = UserChatBackup::where('persona_id',$pId)->with(['userChatMsg' => function($query){
						   return $query->sum('score');
						},'personaDetail','chatMsg'])->get();
		return view('admin/insight/compaign-participants',compact('pId','persona','compaign','brand','personaChat'));
	}

	/*============Compaign participation=======*/

	public function compaignParticipation($personaId=NULL,$type=NULL)
	{
		$web = '';
		$brand = array();
		if(isset($_GET['type']))
		{
			$web = $_GET['type'];
		}
		$pId = $personaId;
		$persona = Personas::where('_id', $pId)->first();
		$brand = Brands::where('_id',$persona->brand)->first();
		$industry = Industries::where('_id',$persona->industry)->first();
		$compaignType = Compaigns::where('_id',$persona->compaign)->first();
		if($persona != '')
		{
			$brand = Brands::where('_id',$persona->brand)->first();
		}
		$lifetimeVisitors = UserChatBackup::where('persona_id',$pId)
							->where(function($query) use ($type)  {
					            if($type != '') 
					            {
					                $query->where('url_type', $type);
					            }
					         })
							
							->get();
		$monthlyVisitors = UserChatBackup::where('persona_id',$pId)
							->where(function($query) use ($type)  {
					            if($type != '') 
					            {
					                $query->where('url_type', $type);
					            }
					         })
							
							->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
							->get();
		$weeklyVisitors = UserChatBackup::where('persona_id',$pId)
							->where(function($query) use ($type)  {
					            if($type != '') 
					            {
					                $query->where('url_type', $type);
					            }
					         })
							
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->get();
		/*$yearlyRecord = UserChatBackup::where('persona_id',$pId)->get()
							
            				->groupBy(function($val) {
            					return Carbon::parse($val->created_at)->format('Y');
							});*/
		$monthlyRecord =  UserChatBackup::where('persona_id',$pId)
							->where(function($query) use ($type)  {
					            if($type != '') 
					            {
					                $query->where('url_type', $type);
					            }
					         })
							->get()
							->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
            				->groupBy(function($d) {
            					return Carbon::parse($d->created_at)->format('m');
							});
        /*$weeklyRecord =  UserChatBackup::where('persona_id',$pId)->get()
        					
    						->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            				->groupBy(function($date) {
            					return Carbon::parse($date->created_at)->format('D');
							});*/

		return view('admin/insight/compaign-participation',compact('brand','industry','compaignType','monthlyRecord','pId','persona','brand','lifetimeVisitors','monthlyVisitors','weeklyVisitors'));
	}

	/*=========Insight comparison=========*/

	public function insightComparison($brand=NULL,$persona1=NULL,$persona2=NULL,$persona3=NULL,$persona4=NULL)
	{

		$brandId = $brand;
		$compaign = Compaigns::where('brandId',$brand)->get();
		$brands = Brands::where('status','A')->get();
		$industry = Industries::where('status','A')->get();

		/*==Persona1 detail==*/
		$persona1Detail = Personas::where('_id',$persona1)->first();

		$personas1 = array();

		$per1LifetimeVisitors = '';
		$per1MonthlyVisitors = '';
		$per1WeeklyVisitors = '';

		$per1YearlyRecord = array();
		$per1MonthlyRecord = array();
		$per1WeeklyRecord = array();

		if($persona1Detail != '')
		{
			$personas1 = Personas::where('brand',$brand)->where('industry',$persona1Detail->industry)->where('compaign',$persona1Detail->compaign)->get();
			$per1LifetimeVisitors = UserChatBackup::where('persona_id',$persona1Detail->_id)
							->count();
			$per1MonthlyVisitors = UserChatBackup::where('persona_id',$persona1Detail->_id)
								->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
								->count();
			$per1WeeklyVisitors = UserChatBackup::where('persona_id',$persona1Detail->_id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->count();
			$per1YearlyRecord = UserChatBackup::select(' COUNT(*) AS count','created_at')
    						->where('persona_id',$persona1Detail->_id)->get()
            				->groupBy(function($val) {
            					return Carbon::parse($val->created_at)->format('Y');
							});
			$per1MonthlyRecord =  UserChatBackup::select('count(id) as totalRecords','created_at')
    						->where('persona_id',$persona1Detail->_id)->get()
            				->groupBy(function($d) {
            					return Carbon::parse($d->created_at)->format('m');
							});
			$per1WeeklyRecord =  UserChatBackup::select('count(id) as totalRecords','created_at')
    						->where('persona_id',$persona1Detail->_id)->get()
            				->groupBy(function($date) {
            					return Carbon::parse($date->created_at)->format('W');
							});
		}

		/*==Persona2 detail==*/
		$persona2Detail = Personas::where('_id',$persona2)->first();

		$personas2 = array();

		$per2LifetimeVisitors = '';
		$per2MonthlyVisitors = '';
		$per2WeeklyVisitors = '';

		$per2YearlyRecord = array();
		$per2MonthlyRecord = array();
		$per2WeeklyRecord = array();

		if($persona2Detail != '')
		{
			$personas2 = Personas::where('brand',$brand)->where('industry',$persona2Detail->industry)->where('compaign',$persona2Detail->compaign)->get();
			$per2LifetimeVisitors = UserChatBackup::where('persona_id',$persona2Detail->_id)
							->count();
			$per2MonthlyVisitors = UserChatBackup::where('persona_id',$persona2Detail->_id)
								->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
								->count();
			$per2WeeklyVisitors = UserChatBackup::where('persona_id',$persona2Detail->_id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->count();
			$per2YearlyRecord = UserChatBackup::select(' COUNT(*) AS count','created_at')
    						->where('persona_id',$persona2Detail->_id)->get()
            				->groupBy(function($val) {
            					return Carbon::parse($val->created_at)->format('Y');
							});
			$per2MonthlyRecord =  UserChatBackup::select('count(id) as totalRecords','created_at')
    						->where('persona_id',$persona2Detail->_id)->get()
            				->groupBy(function($d) {
            					return Carbon::parse($d->created_at)->format('m');
							});
			$per2WeeklyRecord =  UserChatBackup::select('count(id) as totalRecords','created_at')
    						->where('persona_id',$persona2Detail->_id)->get()
            				->groupBy(function($date) {
            					return Carbon::parse($date->created_at)->format('W');
							});
		}

		/*==Persona3 detail==*/
		$persona3Detail = Personas::where('_id',$persona3)->first();

		$personas3 = array();

		$per3LifetimeVisitors = '';
		$per3MonthlyVisitors = '';
		$per3WeeklyVisitors = '';

		$per3YearlyRecord = array();
		$per3MonthlyRecord = array();
		$per3WeeklyRecord = array();

		if($persona3Detail != '')
		{
			$personas3 = Personas::where('brand',$brand)->where('industry',$persona3Detail->industry)->where('compaign',$persona3Detail->compaign)->get();
			$per3LifetimeVisitors = UserChatBackup::where('persona_id',$persona3Detail->_id)
							->count();
			$per3MonthlyVisitors = UserChatBackup::where('persona_id',$persona3Detail->_id)
								->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
								->count();
			$per3WeeklyVisitors = UserChatBackup::where('persona_id',$persona3Detail->_id)
								->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->count();
			$per3YearlyRecord = UserChatBackup::select(' COUNT(*) AS count','created_at')
    						->where('persona_id',$persona3Detail->_id)->get()
            				->groupBy(function($val) {
            					return Carbon::parse($val->created_at)->format('Y');
							});
			$per3MonthlyRecord =  UserChatBackup::select('count(id) as totalRecords','created_at')
    						->where('persona_id',$persona3Detail->_id)->get()
            				->groupBy(function($d) {
            					return Carbon::parse($d->created_at)->format('m');
							});
			$per3WeeklyRecord =  UserChatBackup::select('count(id) as totalRecords','created_at')
    						->where('persona_id',$persona3Detail->_id)->get()
            				->groupBy(function($date) {
            					return Carbon::parse($date->created_at)->format('W');
							});
		}

		/*==Persona4 detail==*/
		$persona4Detail = Personas::where('_id',$persona4)->first();

		$personas4 = array();

		$per4LifetimeVisitors = '';
		$per4MonthlyVisitors = '';
		$per4WeeklyVisitors = '';

		$per4YearlyRecord = array();
		$per4MonthlyRecord = array();
		$per4WeeklyRecord = array();

		if($persona4Detail != '')
		{
			$personas4 = Personas::where('brand',$brand)->where('industry',$persona4Detail->industry)->where('compaign',$persona4Detail->compaign)->get();
			
			$per4LifetimeVisitors = UserChatBackup::where('persona_id',$persona4Detail->_id)
							->count();
			$per4MonthlyVisitors = UserChatBackup::where('persona_id',$persona4Detail->_id)
								->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
								->count();
			$per4WeeklyVisitors = UserChatBackup::where('persona_id',$persona4Detail->_id)
								->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
								->count();
			$per4YearlyRecord = UserChatBackup::select(' COUNT(*) AS count','created_at')
    						->where('persona_id',$persona4Detail->_id)->get()
            				->groupBy(function($val) {
            					return Carbon::parse($val->created_at)->format('Y');
							});
			$per4MonthlyRecord =  UserChatBackup::select('count(id) as totalRecords','created_at')
    						->where('persona_id',$persona4Detail->_id)->get()
            				->groupBy(function($d) {
            					return Carbon::parse($d->created_at)->format('m');
							});
			$per4WeeklyRecord =  UserChatBackup::select('count(id) as totalRecords','created_at')
    						->where('persona_id',$persona4Detail->_id)->get()
            				->groupBy(function($date) {
            					return Carbon::parse($date->created_at)->format('W');
							});
		}



		return view('admin/insight/insight-comparison',compact('per1YearlyRecord','per1MonthlyRecord','per1WeeklyRecord','per2YearlyRecord','per2MonthlyRecord','per2WeeklyRecord','per3YearlyRecord','per3MonthlyRecord','per3WeeklyRecord','per4YearlyRecord','per4MonthlyRecord','per4WeeklyRecord','per1WeeklyVisitors','per1LifetimeVisitors','per1MonthlyVisitors','per2WeeklyVisitors','per2LifetimeVisitors','per2MonthlyVisitors','per3WeeklyVisitors','per3LifetimeVisitors','per3MonthlyVisitors','per4WeeklyVisitors','per4LifetimeVisitors','per4MonthlyVisitors','brands','brandId','industry','persona1Detail','compaign','personas1','personas2','personas3','personas4','persona2Detail','persona3Detail','persona4Detail'));
	}

	/*==========User persona chat detail========*/

	public function userPersonaChatDetail($chatId)
	{
		$chatDetail = UserChatBackup::where('_id',$chatId)->with(['userChatMsg','personaDetail'])->first();
		return view('admin.insight.persona-chat-detail',compact('chatDetail'));
	}

	public function getBrandList()
	{
		$keyword = $_GET['keyword'];
		if($keyword != '')
		{
			$brands = Brands::where('status','A')->where('brand', 'like', '%' . $keyword . '%')->get();
		}
		else
		{
			$brands = Brands::where('status','A')->get();
		}
		
		$html = '';
		if(count($brands) > 0)
		{
			foreach($brands as $val)
			{
				$html .= '<div class="col-sm-3">
							<a href="javascript:void();" data-attr="'.$val->_id.'" data-target="'.ucfirst($val->brand).'" class="brand_link choose_brand">
								<div class="brand_box">
									<div class="brand_logo">
										<img src="'.asset('/admin/img/uberlogo.png').'" alt="logo">
									</div>
									<div class="brand_txt">
										<p>'.ucfirst($val->brand).'</p>
									</div>
								</div>
							</a>
						</div>';
			}
		}
		else
		{
			$html .= '<div class="col-sm-3">No matching brand found...</div>';
		}
		echo $html;
	}
	
}