<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class DashboardController extends Controller
{
    //
	public function index()
    {
        return view('admin.login');
    }

    public function admin()
    {
		$id = Auth::user()->id;
		$currentuser = User::find($id)->toArray();
		
        return view('admin.index', ['admin' => $currentuser]);
    }

}
