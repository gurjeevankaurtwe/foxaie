<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\FormSubmissions;

class FormController extends Controller
{

	public function index(Request $request)
	{
		$data = $request->all();
		unset($data['chatId']);
		if($request->file())
		{

			foreach($request->file() as $key=>$multipleFiles)
			{

				foreach($multipleFiles as $val)
				{
					$ext = '.'.$val->getClientOriginalExtension();
	           		$fileName = str_replace($ext, '_'.time() . $ext, $val->getClientOriginalName());
	           		$destinationPath = 'uploads/formFiles/';
	      			$val->move($destinationPath,$fileName);
	      			$data['upload-'.$key][] = $fileName;
	      			unset ($data[$key]);
				}
			}
		}
		$chatId = $request->input('chatId');
		$formData = json_encode($data);
		$formSubmissions = New FormSubmissions();
		$formSubmissions->chatId = $chatId;
		$formSubmissions->contentId = $request->input('contentId');
		$formSubmissions->formId = $data['formId'];
		$formSubmissions->formData = $formData;
		$formSubmissions->save();
		if($formSubmissions->id != '')
		{
			$result['success'] = true;
		}
		else
		{
			$result['success'] = false;
		}
		echo json_encode($result);exit;
	}
}