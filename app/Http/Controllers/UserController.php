<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{

/*========Show manage admin view========*/

	public function index()
	{
		return view('admin.persona.manage-admin');
	}

/*===========Update profile===========*/

	public function updateProfile(Request $request)
	{

		$user = User::find($request->input('id'));
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		if($request->input('new_password') != '')
		{
			$user->password = Hash::make($request->input('new_password'));
		}
		if($request->hasFile('profile_image'))
        {
        	$ext = '.'.$request->file('profile_image')->getClientOriginalExtension();
        	$fileName = str_replace($ext, '_'.time() . $ext, $request->file('profile_image')->getClientOriginalName());
        	$destinationPath = 'uploads/user-images/';
  			$request->file('profile_image')->move($destinationPath,$fileName);
  			$user->profile_image = $fileName;
  		}
  		if($request->hasFile('banner_image'))
        {
        	$bannerExt = '.'.$request->file('banner_image')->getClientOriginalExtension();
        	$bannerFileName = str_replace($bannerExt, '_'.time() . $bannerExt, $request->file('banner_image')->getClientOriginalName());
        	$bannerDestinationPath = 'uploads/user-images/';
  			$request->file('banner_image')->move($bannerDestinationPath,$bannerFileName);
  			$user->banner_image = $bannerFileName;
  		}
  		$user->save();
  		return redirect('backoffice/persona/manage-admin')->with('success', 'Profile updated successfully');
	}

/*===========Check current password=========*/

	public function checkCurrentPwd()
	{
		$userId = Auth::user()->id;
		$user = User::find($userId);
		if (Hash::check($_GET['currentPwd'], Auth::user()->password)) 
		{
		 	$result['success'] = true;
		}
		else
		{
			$result['success'] = false;
		}
		echo json_encode($result);exit;
	}

/*==========Show login view========*/

	public function userLogin(Request $request)
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$user_data = array(
		      	'email'  => $request->input('login-email'),
		      	'password' => $request->input('login-password')
	     	);
	     	if(Auth::attempt($user_data))
	     	{
	     		return redirect('backoffice')->with('success', 'Persona has been successfully added');
	     	}
	     	else
	     	{
	     		return back()->with('error', 'Wrong Login Details');
	     	}
		}
		return view('admin.login');
	}
}