<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;

class BusinessLogicController extends Controller
{
    public function index(Request $request, $id)
    {
		$content = Content::where('persona_id', $id)->get();
        return view('admin.persona.businesslogic', compact('id', 'content'));
    }
	
	public function save_logic(){
		//
	}
}
