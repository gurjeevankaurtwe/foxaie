<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\Personas;
use App\Industries;
use App\UserChat;
use App\UserChatBackup;
use App\UserChatBackupMsg;
use Session;
use PDF;


class ChatController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
		$cr_url = url()->current() ;
		$persona = Personas::where('url', $cr_url)->first();
		return view('chat', compact('persona'));
    }
	
	public function start_chat()
	{
		$industries=Industries::all();
        return view('welcome', compact('industries'));
	}
	
	public function get_persona(){
		$pesona = Personas::where('industry', $_REQUEST['industry'])->get();
		$ret = '<option value="">Select</option>';
		foreach($pesona as $cont){
			
			$ret .= '<option value="'.$cont->_id . '">'.$cont->persona_title .'</option>';
			
		}
		echo $ret;
	}
	
	public function get_url(){
		$persona = Personas::find($_REQUEST['pers']);
		echo $persona->url;
	}
	
	
	public function upload_file(){
		
		echo "done";
		die;
		$file = $request->file('file-upload');
			$fileName= time()."-".$file->getClientOriginalName();
			$fileName = str_replace(' ', '_', $fileName);
		   
			//File Extension
			$fileExt = $file->getClientOriginalExtension();
			//File Real Path
			$realPath = $file->getRealPath();
			//Display File Size
			$fileSize = $file->getSize();
			//Display File Mime Type
			$filetype = $file->getMimeType();
			$destinationPath = 'uploads';
			$file->move($destinationPath,$fileName);
			$fileurl= asset($destinationPath."/".$fileName);
			echo '<img src="'.asset($destinationPath."/".$fileName).">";
	}

/*===========Chat backup module==========*/

	public function chatBackup()
	{
		$cr_url = url()->current() ;
		$url = explode('/', $cr_url);
		array_pop($url);
		$persona = Personas::where('url', implode("/",$url))->first();
		return view('chat-backup', compact('persona'));
	}

/*=============Show user chat listing with ip or email==========*/

	public function userChat()
    {
    	$chat = UserChatBackup::select('uniqueId','user_ip_address')->where('chat_type', 'web')->groupBy('email')->get();
    	return view('admin.chat.user-chat',compact('chat')); 
    }

/*===============Show user chat detail on click chat button===========*/

	public function getUserChatDetail($ip=NULL,$email=NULL)
	{
		if($email != '')
		{
			$chatDetail = UserChatBackup::where('email',Crypt::decryptString($email))->with(['userChatMsg','personaDetail'])->get();
		}
		else
		{
			$chatDetail = UserChatBackup::where('uniqueId',$ip)->where('email','')->with(['userChatMsg','personaDetail'])->get();
		}
		return view('admin.chat.user-chat-detail',compact('chatDetail'));
	} 

	public function userChatPdf($personaId,$ip)
	{
		$chatDetail = UserChatBackup::where('persona_id',$personaId)->where('user_ip_address',$ip)->with(['userChatMsg','personaDetail'])->get();
		$data = ['chatDetail' => $chatDetail];

        $pdf = PDF::loadView('admin.chat.user-chat-pdf', $data);



        return $pdf->download('hdtuto.pdf');

    }

	public function checkUserIp()
	{
		$personaId = $_GET['personaId'];
		$ip = $_GET['ip'];
		$type = $_GET['urlType'];
		if(Session::get('uniqueId') != '')
		{
			$chatDetail = UserChatBackup::where('user_ip_address',$ip)->where('uniqueId',Session::get('uniqueId'))->where('persona_id',$personaId)->where('url_type',$type)->get();
			if(count($chatDetail) > 0)
			{
				echo 1;
			}
			else
			{
				echo 0;
			}
		}
		else
		{
			echo 0;
		}
		
	}

	public function updatePreviousChatCount()
	{
		$personaId = $_GET['personaId'];
		$ip = $_GET['ip'];
		$chatDetail = UserChatBackup::where('user_ip_address',$ip)->where('persona_id',$personaId)->first();
		$clicks = $chatDetail->clicks+1;
		$chat = UserChatBackup::find($chatDetail->_id);
		$chat->clicks = $clicks;
		$chat->save();
	}

	public function deletePreviousChat()
	{
		$personaId = $_GET['personaId'];
		$ip = $_GET['ip'];
		$type= $_GET['urlType'];
		$chatDetail = UserChatBackup::where('user_ip_address',$ip)->where('uniqueId',SESSION::get('uniqueId'))->where('persona_id',$personaId)->where('url_type',$type)->first();
		$ChatMsgs = UserChatBackupMsg::where('chatId',$chatDetail->_id);
        $ChatMsgs->delete();
        $chatDetail->delete();
	}

	
}

