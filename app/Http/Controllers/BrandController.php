<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Brands;
use App\Compaigns;

class BrandController extends Controller
{
	/*====listing of brands====*/

	public function index()
    {
		$brands=brands::all();
		return view('admin.persona.brands',['brands'=> $brands]);
    }

    /*======add brand=====*/

    public function addBrand(Request $request)
    {
    	$brand = new Brands();
        $brand->brand = $request->get('brand');
        $brand->status = $request->get('status');
        if($request->hasFile('brand_image'))
        {
            $ext = '.'.$request->file('brand_image')->getClientOriginalExtension();
            $fileName = str_replace($ext, '_'.time() . $ext, $request->file('brand_image')->getClientOriginalName());
            $destinationPath = 'uploads/user-images/';
            $request->file('brand_image')->move($destinationPath,$fileName);
            $brand->brand_image = $fileName;
        }
        $brand->save();
        return redirect('backoffice/persona/brands')->with('success', 'Brand has been successfully added');
    }

    /*======delete brand======*/

    public function deleteBrand($id)
    {
    	$brand = Brands::find($id);
        $brand->delete();
        return redirect('backoffice/persona/brands')->with('success','Brand has been  deleted');
    }

    /*=====edit brand=====*/

    public function editBrand($id)
    {
    	$persona = Brands::find($id);
        return view('admin.persona.edit',compact('persona','id'));
    }

    /*====update brand=====*/

    public function updateBrand(Request $request)
    {
		$brandId = $request->get('brand-id') ; 
        $brand= Brands::find($brandId);
        $brand->brand = $request->get('brand');
        $brand->status = $request->get('status');
        if($request->hasFile('brand_image'))
        {
            $ext = '.'.$request->file('brand_image')->getClientOriginalExtension();
            $fileName = str_replace($ext, '_'.time() . $ext, $request->file('brand_image')->getClientOriginalName());
            $destinationPath = 'uploads/user-images/';
            $request->file('brand_image')->move($destinationPath,$fileName);
            $brand->brand_image = $fileName;
        }
        $brand->save();
        return redirect('backoffice/persona/brands')->with('success', 'Brand has been successfully update');
    }

    /*=======show brand compaign list======*/

    public function brandCompaign($id)
    {
        $brandId = $id;
        $compaign = Compaigns::where('brandId', $brandId)->get();
        $brand = Brands::where('_id',$brandId)->first();
        $brand_name = $brand->brand;
        return view('admin.persona.compaigns',compact('compaign','brandId','brand_name'));
        
    }

    /*=====Add brand compaign===*/

    public function addCompaign(Request $request)
    {
        $compaign= new Compaigns();
        $compaign->brandId = $request->get('brandId');
        $compaign->compaign = $request->get('compaign');
        $compaign->status = $request->get('status');
        $compaign->save();
        return redirect('backoffice/persona/brand_compaign/'.$request->get('brandId'))->with('success', 'Compaign has been successfully added');
    }

    /*=========Update brand compaign=====*/

    public function updateCompaign(Request $request)
    {
        $compaign= Compaigns::find($request->get('compaign_id'));
        $compaign->compaign = $request->get('compaign');
        $compaign->status = $request->get('status');
        $compaign->save();
        return redirect('backoffice/persona/brand_compaign/'.$compaign->brandId)->with('success', 'Compaign has been successfully update');
    }

    /*======Get brand compaign with brand id=====*/

    public function getBrandCompaign()
    {
        $brandId = $_GET['brandId'];
        $compaign = Compaigns::where('brandId', $brandId)->where('status','A')->get();
        $html = '<option value="">Select Compaign...</option>';
        foreach($compaign as $val)
        {
            $html .= '<option value="'.$val->id.'" data-attr="'.$val->compaign.'">'.$val->compaign.'</option>';
        }
        echo $html;
    }

    public function getBrandsCompaign()
    {
        $brandId = $_GET['brandId'];
        $compaign = Compaigns::where('brandId', $brandId)->where('status','A')->get();
        $html = '';
        foreach($compaign as $val)
        {
            $html .= '<div class="col-sm-4">
                        <a href="javascript:void();" data-attr="'.$val->_id.'" data-target="'.ucfirst($val->compaign).'" class="camptype_link brandsComp">
                            <div class="camptype_box">
                                <div class="camptype_logo">
                                    <span><i class="fa fa-folder"></i></span>
                                </div>
                                <div class="camptype_txt">
                                    <p>'.ucfirst($val->compaign).'</p>
                                </div>
                            </div>
                        </a>
                    </div>';
        }
        echo $html;
    }

   
}

?>
