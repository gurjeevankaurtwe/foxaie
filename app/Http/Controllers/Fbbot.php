<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Response;

use App\Compaigns;

use App\FormSubmissions;

use App\Personas;

use App\Brands;

use App\Industries;

use App\Content;

use App\FbChat;

use View;

use Auth;

use App\User;

use App\ContentConfigure;

use App\UserChatBackup;

use App\UserChatBackupMsg;

use Google\Cloud\Dialogflow\V2\IntentsClient;

use Google\ApiCore\ApiException;

use Google\ApiCore\CredentialsWrapper;

use Google\ApiCore\LongRunning\OperationsClient;

use Google\ApiCore\Testing\GeneratedTest;

use Google\ApiCore\Testing\MockTransport;

use Google\Cloud\Dialogflow\V2\BatchUpdateIntentsResponse;

use Google\Cloud\Dialogflow\V2\Intent;

use Google\Cloud\Dialogflow\V2\ListIntentsResponse;

use Google\LongRunning\GetOperationRequest;

use Google\LongRunning\Operation;

use Google\Protobuf\Any;

use Google\Protobuf\GPBEmpty;

use Google\Rpc\Code;

use stdClass;

use PHPUnit\Framework\TestCase;

class Fbbot extends Controller
{
    public function __construct()
    {
    }

    

    

    public function verify(Request $request)
    {

        //die("Hi");

        // parameters

        $hubVerifyToken = 'TGffLOvR4RuepA3JbmdmDItQXgx2BbDx5GJF4YkNHO';

        

        // check token at setup

        if ($_REQUEST['hub_verify_token'] === $hubVerifyToken) {
            echo $_REQUEST['hub_challenge'];
        } else {
            return response('', 201)

                  ->header('Content-Type', 'text/plain');
        }
    }

    

    public function message(Request $request)
    {

        

        //die("HI Singh");

        // handle fb call

        //$input = json_decode(file_get_contents('php://input'), true);

        $fbmsg = $request->json()->all();

        //var_dump($fbmsg);die;

            

        //get reply for fb msg

        $res=$this->msg_reply($fbmsg);

        $result=$this->send_msg($res);
    }

    

    

    public function msg_reply($input)
    {
        $senderId = $input['entry'][0]['messaging'][0]['sender']['id'];

        $messageText = $input['entry'][0]['messaging'][0]['message']['text'];
        //$messageText = 'fds';

        /*$message = array();





        if (!empty($input['entry'][0]['messaging'][0]['message']['text'])) {

            $message['text']=$input['entry'][0]['messaging'][0]['message']['text'];

        }

        if (!empty($input['entry'][0]['messaging'][0]['message']['attachments'])) {

            $message['attachments']=$input['entry'][0]['messaging'][0]['message']['text'];

        }

        if (!empty($input['entry'][0]['messaging'][0]['message']['quick_reply'])) {

            $message['quick_reply']=$input['entry'][0]['messaging'][0]['message']['text'];

        }*/

        //Register new user if not exist

        $user = new User();
        $checkUser = User::where('fb_sender_id', $senderId)->first();
        if ($checkUser == '') {
            $user->name = '';
            $user->email = '';
            $user->fb_sender_id = $senderId;
            $user->save();
        }

        //get response to reply to user
        $fbPersona = Personas::where('channel', 'Facebook')->first();

        $first_content = Content::where('persona_id', $fbPersona->_id)->orderBy('content_cust_id', 'ASC')->first();

        $res_tb=array();
        $i=$first_content;
        while ($i!='') {
            $configureContent = ContentConfigure::where('content_id', $i)->first();
            if ($configureContent == '') {
                break;
            } else {
                array_push($res_tb, $configureContent->response_content);
                $contentDec = json_decode($configureContent->response_content);
                $i=$contentDec->response_cont_id;
            }
        }
        

        //backup user chat



        //return reply response

        return $res = [

            'recipient' => [ 'id' => $senderId ],

            'message' => $first_content->content['message']

        ];
    }

    public function recursive($contentId)
    {
        $configureContent = ContentConfigure::where('content_id', $contentId)->first();
        if ($configureContent == '') {
            return 1;
        } else {
            $contentDec = json_decode($configureContent->response_content);
            return $this->recursive($contentDec->response_cont_id);
        }
    }

    public function send_msg($res)
    {
        $accessToken =   "EAAFDvBvCVEMBACfmhCaCRq4uSQ0oW3ngamrEImpxBZA4vWKkVtqomiBW70y065LYIW81j4IC9wbgJSMBNmHvp78c9RdGNjctfkBBQ4eKGU4O4GnRQFss1LvZCfAdSZCPI8Es2JeeVlBL2ERvyhzgNBg42hPypdfDc89ux7eDK2p6Qf7F7pygZCuZBws1I3IsZD";

        

        $ch = curl_init('https://graph.facebook.com/v4.0/me/messages?access_token='.$accessToken);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($res));

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

        

        $result = curl_exec($ch);

        

        

        //Get error if any

        if (curl_errno($ch)) {
            $e = curl_errno($ch);

            curl_close($ch);

            //var_dump($e);die;

            return array("success" => 0, "result" => $e);
        } else {
            curl_close($ch);

            return array("success" => 1, 'result' => $result);
        }
    }

    

    public function msgtest(Request $request)
    {
        echo "hello";
    }
}
