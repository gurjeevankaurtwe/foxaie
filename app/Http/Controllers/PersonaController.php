<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Mail;
use App\Compaigns;
use App\FormSubmissions;
use App\Personas;
use App\Brands;
use App\Industries;
use App\Content;
use View;
use Auth;
use App\User;
use App\ContentConfigure;
use App\UserChat;
use App\UserChatBackup;
use App\UserChatBackupMsg;
use Google\Cloud\Dialogflow\V2\IntentsClient;
use Google\ApiCore\ApiException;
use Google\ApiCore\CredentialsWrapper;
use Google\ApiCore\LongRunning\OperationsClient;
use Google\ApiCore\Testing\GeneratedTest;
use Google\ApiCore\Testing\MockTransport;
use Google\Cloud\Dialogflow\V2\BatchUpdateIntentsResponse;
use Google\Cloud\Dialogflow\V2\Intent;
use Google\Cloud\Dialogflow\V2\ListIntentsResponse;
use Google\LongRunning\GetOperationRequest;
use Google\LongRunning\Operation;
use Google\Protobuf\Any;
use Google\Protobuf\GPBEmpty;
use Google\Rpc\Code;
use stdClass;
use PHPUnit\Framework\TestCase;
use Config;
use Cookie;
use Session;


class PersonaController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {
			$admin_u = auth()->user();
			return $next($request);
		});
	}
	
    public function add_persona()
    {
		$industries = Industries::where('status', 'A')->get();
		$brands = Brands::where('status', 'A')->get();
		$facebookPersona = Personas::where('channel','Facebook')->get();
		$twitterPersona = Personas::where('channel','Twitter')->get();
		return view('admin.persona.add', compact('industries','brands','facebookPersona','twitterPersona'));
    }
	
	public function index(Request $request)
    {
    	$personas=Personas::all();
        return view('admin.persona.index',compact('personas'));
    }
    
	
	public function save(Request $request)
    {
		$persona = new Personas();
        if($request->hasFile('persona_image'))
        {
        	$ext = '.'.$request->file('persona_image')->getClientOriginalExtension();
        	$fileName = str_replace($ext, '_'.time() . $ext, $request->file('persona_image')->getClientOriginalName());
        	$destinationPath = 'uploads/user-images/';
  			$request->file('persona_image')->move($destinationPath,$fileName);
  			$persona->persona_image = $fileName;
  			
        }
        $persona->persona_title = $request->get('persona_title');
        $persona->industry = $request->get('industry');
        $persona->brand = $request->get('brand');
        $persona->channel = $request->get('channel');
		$persona->url = $request->get('url');
		$persona->persona_url = $request->get('persona_url');
		$persona->tags = $request->get('tags');	
		$persona->score = $request->get('score');	
		$persona->compaign = $request->get('compaign');	
		$persona->require_login = $request->get('require_login');	
        $persona->save();
        return redirect('backoffice/persona')->with('success', 'Persona has been successfully added');
    }
	
	public function edit($id)
    {
    	$persona = Personas::find($id);
    	$brands = Brands::all();
    	$industries = Industries::all();
    	$compaign = Compaigns::where('brandId', $persona->brand)->get();
    	//$facebookPersona = Personas::where('channel','Facebook')->get();
		//$twitterPersona = Personas::where('channel','Twitter')->get();
		return view('admin.persona.edit',compact('persona','id','brands','industries','compaign'));
    }
	
	public function update(Request $request, $id)
    {
        $persona= Personas::find($id);
        if($request->get('check_image') == '')
        {
        	if($request->hasFile('persona_image'))
	        {
	        	$ext = '.'.$request->file('persona_image')->getClientOriginalExtension();
	        	$fileName = str_replace($ext, '_'.time() . $ext, $request->file('persona_image')->getClientOriginalName());
	        	$destinationPath = 'uploads/user-images/';
	  			$request->file('persona_image')->move($destinationPath,$fileName);
	  			$persona->persona_image = $fileName;
	  		}
        }
       	$persona->persona_title = $request->get('persona_title');
        $persona->industry = $request->get('industry');
        $persona->brand = $request->get('brand');
        $persona->channel = $request->get('channel');
		$persona->url = $request->get('url');
		$persona->tags = $request->get('tags');	
		$persona->score = $request->get('score');	
		$persona->compaign = $request->get('compaign');
		$persona->require_login = $request->get('require_login');		
        $persona->save();
        return redirect('backoffice/persona')->with('success', 'Persona has been successfully update');
    }
	
	public function destroy($id)
    {
        $persona = Personas::find($id);
        $persona->delete();
        return redirect('backoffice/persona')->with('success','Persona has been  deleted');
    }

	public function industries(){
		$industries=Industries::all();
        return view('admin.persona.industries',compact('industries'));
	}

	public function save_industry(Request $request)
    {
        $industry = new Industries();
        $industry->industry = $request->get('industry');
        $industry->status = $request->get('status');
        $industry->save();
        return redirect('backoffice/persona/industries')->with('success', 'Industry has been successfully added');
    }
	
	public function update_industries(Request $request)
    {
		//print_r($_REQUEST);
		$ind_ID = $request->get('industry-id') ; 
        $industry= Industries::find($ind_ID);
        $industry->industry = $request->get('industry');
        $industry->status = $request->get('status');
        $industry->save();
        return redirect('backoffice/persona/industries')->with('success', 'Industry has been successfully update');
    }

	public function delete_ind($id)
    {
        $industry = Industries::find($id);
        $industry->delete();
        return redirect('backoffice/persona/industries')->with('success','Industry has been  deleted');
    }
	
	public function content(Request $request, $id){
		$content = Content::where('persona_id', $id)->get();
		$persona = Personas::where('_id',$id)->first();
		$reponse_content = ContentConfigure::where('persona_id', $id)->get();
		return view('admin.persona.content',compact('id', 'persona','content', 'reponse_content'));
	}
	
	public function save_content(Request $request, $id)
	{

		$persona = Personas::where('_id',$id)->first();
		if($_REQUEST['type'] == "Text")
		{
			if($persona->channel == 'Facebook')
			{
				$arr['messaging_type'] = "";
				$arr['recipient'] = array('id'=>"");
				$arr['message'] = array('text'=>$_REQUEST['query']);
				$content_data = $arr;
			}
			else if($persona->channel == 'Twitter')
			{
				$arr['event'] = array(
						'type'=>'message_create',
						'message_create' => array(
							'target' => array(
								'recipient_id' => ''
							),
							'message_data'=> array(
								'text' => $_REQUEST['query'],
							)
						)
					);
				$content_data = $arr;
			}
			else
			{
				if(isset($_REQUEST['email']))
				{
					$arr['email'] = $_REQUEST['email'];
				}
				$arr['type'] = $_REQUEST['type'];
				$arr['query'] = $_REQUEST['query'];
				$content_data = json_encode($arr);
			}
			if($request->input('content_id') == '')
			{
				$content = New Content();
			}
			else
			{
				$content = Content::find($request->input('content_id'));
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score'];
			$content->save();
		}
		else if($_REQUEST['type'] == "Button")
		{
			$arr['type'] = $_REQUEST['type'];
			$arr['query'] = $_REQUEST['query'];
			if(isset($_REQUEST['answer']))
			{
				$arr['answer'] = $_REQUEST['answer'];
			}
			$content_data = json_encode($arr);
			if($request->input('content_id') == '')
			{
				$content = New Content();
			}
			else
			{
				$content = Content::find($request->input('content_id'));
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score'];
			$content->save();
		}
		else if($_REQUEST['type'] == "Embed")
		{
			$arr['type'] = $_REQUEST['type'];
			$arr['query'] = $_REQUEST['query'];
			$arr['btnlabel'] = $_REQUEST['btn-label'];
			if(isset($_REQUEST['answer']))
			{
				$arr['answer'] = $_REQUEST['answer'];
			}
			
			$content_data = json_encode($arr);
			if($request->input('content_id') == '')
			{
				$content = New Content();
			}
			else
			{
				$content = Content::find($request->input('content_id'));
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score']; 
			if(isset($_REQUEST['require_login']))
			{ 
				$req_log = $_REQUEST['require_login']; 
			} 
			else 
			{
				$req_log = 0;
			} 
			$content->require_login = $req_log ;
			$content->save();

		}
		else if($_REQUEST['type'] == "Radio")
		{
			if($persona->channel == 'Facebook')
			{
				foreach($_REQUEST['ques'] as $val)
				{
					$contents[] = array('content_type'=>'text','title'=>$val,'payload'=>'','image_url'=>'');
				}
				$arr['recipient'] = array('id'=>"");
				$arr['messaging_type'] = 'RESPONSE';
				$arr['message'] = array(
						'text'=>$_REQUEST['answer'],
						'quick_replies' => $contents
					);
				$content_data = $arr;
			}
			else if($persona->channel == 'Twitter')
			{
				foreach($_REQUEST['ques'] as $val)
				{
					$contents[] = array('label'=>$val,'description'=>$val,'metadata'=>'');
				}
				$arr['event'] = array(
						'type'=>'message_create',
						'message_create' => array(
							'target' => array(
								'recipient_id' => ''
							),
							'message_data'=> array(
								'text' => $_REQUEST['answer'],
								'quick_reply' => array(
									'type' => 'options',
									'options' => $contents
								)
							)
						)
					);
				$content_data = $arr;
			}
			else
			{
				$arr['type'] = $_REQUEST['type'];
				$arr['query'] = $_REQUEST['answer'];
				$arr['answer'] = $_REQUEST['ques'];
				$content_data = json_encode($arr);
			}
			if($request->input('content_id') == '')
			{
				$content = New Content();
			}
			else
			{
				$content = Content::find($request->input('content_id'));
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score'];
			$content->save();
		}else if($_REQUEST['type'] == "Checkbox"){
			
			$arr['type'] = $_REQUEST['type'];
			$arr['query'] = $_REQUEST['answer'];
			$arr['answer'] = $_REQUEST['ques'];
			$content_data = json_encode($arr); 
			if($request->input('content_id') == '')
			{
				$content = New Content();
			}
			else
			{
				$content = Content::find($request->input('content_id'));
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score'];
			if(isset($_REQUEST['require_login'])){ $req_log = $_REQUEST['require_login']; } else {$req_log = 0;} 
			$content->require_login = $req_log ;
			$content->save();
		}
		else if($_REQUEST['type'] == "File")
		{
			
			if($request->input('content_id') == '')
			{
				$file = $request->file('file-input');
				$fileName= time()."-".$file->getClientOriginalName();
			   	$fileExt = $file->getClientOriginalExtension();
				$realPath = $file->getRealPath();
				$fileSize = $file->getSize();
				$filetype = $file->getMimeType();
				$destinationPath = 'uploads';
				$file->move($destinationPath,$fileName);
				if($persona->channel == 'Facebook')
				{
					$type = explode("/",$filetype);
					if($type[0] == 'application')
					{
						$filetype = 'file';
					}
					else
					{
						$filetype = $type[0];
					}
					$arr['recipient'] = array('id'=>"");
					$arr['message'] = array('attachment'=>array('type'=>$filetype,'payload'=>array('url'=>$destinationPath."/".$fileName,'is_reusable'=>true)));
				}
				else
				{
					$arr['type'] = $_REQUEST['type'];
					$arr['query'] = $destinationPath."/".$fileName;
					$arr['btnlabel'] = $_REQUEST['btn-label'];
					$arr['filetype'] = $filetype;
				}
				$content = New Content();
			}
			else
			{
				if($request->input('check_image') == '')
				{
					$file = $request->file('file-input');
					$fileName= time()."-".$file->getClientOriginalName();
				   	$fileExt = $file->getClientOriginalExtension();
					$realPath = $file->getRealPath();
					$fileSize = $file->getSize();
					$filetype = $file->getMimeType();
					$destinationPath = 'uploads';
					$file->move($destinationPath,$fileName);
					$arr['filetype'] = $filetype;
					$arr['query'] = $destinationPath."/".$fileName;
				}
				else
				{
					$arr['filetype'] = $request->input('check_image');
					$arr['query'] = $request->input('check_image');
				}
				$arr['type'] = $_REQUEST['type'];
				$arr['btnlabel'] = $_REQUEST['btn-label'];
				$content = Content::find($request->input('content_id'));
			}
			if(isset($_REQUEST['answer']))
			{
				$arr['answer'] = $_REQUEST['answer'];
			}
			if($persona->channel == 'Facebook')
			{	
				$content_data = $arr; 
			}
			else
			{
				$content_data = json_encode($arr); 
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score'];
			$content->save();
		}
		else if($_REQUEST['type'] == "Docs")
		{
			
			if($request->input('content_id') == '')
			{
				$file = $request->file('file-input');
				$fileName= time()."-".$file->getClientOriginalName();
			   	$fileExt = $file->getClientOriginalExtension();
				$realPath = $file->getRealPath();
				$fileSize = $file->getSize();
				$filetype = $file->getMimeType();
				$destinationPath = 'uploads';
				$file->move($destinationPath,$fileName);
				
				$arr['type'] = $_REQUEST['type'];
				$arr['query'] = $destinationPath."/".$fileName;
				$arr['btnlabel'] = $_REQUEST['btn-label'];
				$arr['filetype'] = $filetype;
				$content = New Content();
			}
			else
			{
				if($request->input('check_image') == '')
				{
					$file = $request->file('file-input');
					$fileName= time()."-".$file->getClientOriginalName();
				   	$fileExt = $file->getClientOriginalExtension();
					$realPath = $file->getRealPath();
					$fileSize = $file->getSize();
					$filetype = $file->getMimeType();
					$destinationPath = 'uploads';
					$file->move($destinationPath,$fileName);
					$arr['filetype'] = $filetype;
					$arr['query'] = $destinationPath."/".$fileName;
				}
				else
				{
					$arr['filetype'] = $request->input('check_image');
					$arr['query'] = $request->input('check_image');
				}
				$arr['type'] = $_REQUEST['type'];
				$arr['btnlabel'] = $_REQUEST['btn-label'];
				$content = Content::find($request->input('content_id'));
			}
			if(isset($_REQUEST['answer']))
			{
				$arr['answer'] = $_REQUEST['answer'];
			}
			if($persona->channel == 'Facebook')
			{	
				$content_data = $arr; 
			}
			else
			{
				$content_data = json_encode($arr); 
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score'];
			$content->save();
		}
		else if($_REQUEST['type'] == "Link")
		{
			
			$arr['type'] = $_REQUEST['type'];
			$arr['query'] = $_REQUEST['link-input'];
			if(isset($_REQUEST['answer']))
			{
				$arr['answer'] = $_REQUEST['answer'];
			}
			$content_data = json_encode($arr); 
			if($request->input('content_id') == '')
			{
				$content = New Content();
			}
			else
			{
				$content = Content::find($request->input('content_id'));
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score'];
			if(isset($_REQUEST['require_login'])){ $req_log = $_REQUEST['require_login']; } else {$req_log = 0;} 
			$content->require_login = $req_log ;
			$content->save();
		}
		else if($_REQUEST['type'] == "Textarea")
		{
			
			$arr['type'] = $_REQUEST['type'];
			$arr['query'] = $_REQUEST['query'];
			if(isset($_REQUEST['answer']))
			{
				$arr['answer'] = $_REQUEST['answer'];
			}
			$content_data = json_encode($arr); 
			if($request->input('content_id') == '')
			{
				$content = New Content();
			}
			else
			{
				$content = Content::find($request->input('content_id'));
			}
			$content->persona_id = $id;
			$content->content_cust_id = $_REQUEST['content_cust_id'] ; 
			$content->content = $content_data;
			$content->score = $_REQUEST['score'];
			$content->save();
		}
		return redirect('backoffice/persona/content/'.$id)->with('success', 'Content has been successfully added');
	}
	
	public function delete_cont($id)
    {
        $content = Content::find($id);
        $content->delete();
		$contentconf = ContentConfigure::where('content_id', $id);
        $contentconf->delete();
        return redirect('backoffice/persona/content/'.$_REQUEST['cont-id'])->with('success','Content has been deleted');
    }

	public function content_configure(){
		$content = Content::find($_REQUEST['cont_id']);
		$persona = Personas::find($_REQUEST['per_id']);
		return view('admin.persona.configure', compact('content', 'persona'));
	}
	
	public function get_configure_content()
	{
		$content = Content::where('persona_id', $_REQUEST['pers_id'])->get();
		$persona = Personas::where('_id',$_REQUEST['pers_id'])->first();
		$ret = '<option value="">Select Next Query</option>';
		foreach($content as $cont)
		{
			if($persona->channel == 'Facebook')
			{
				$data = $cont->content;
				if(array_key_exists('attachment',$data['message']))
				{
					$type =  'File';
					$query = $data['message']['attachment']['payload']['url'];
				}
				else if(array_key_exists('quick_replies',$data['message']))
				{
					$type =  'Radio';
					$query = $data['message']['text'];
				}
				else
				{
					$type =  ucfirst(key($data['message']));
					$query = $data['message']['text'];
				}
			}
			else if($persona->channel == 'Twitter')
			{
				$data = $cont->content;
				if(array_key_exists('quick_reply',$data['event']['message_create']['message_data']))
				{
					$type =  'Radio';
					$query = $data['event']['message_create']['message_data']['text'];
				}
				else
				{
					$type =  ucfirst(key($data['event']['message_create']['message_data']));
					$query = $data['event']['message_create']['message_data']['text'];
				}
			}
			else
			{
				$data = json_decode($cont->content);
				$type = $data->type;
				$query = $data->query;
			}
			
			if($type == $_REQUEST['type'])
			{
				if($type == 'Embed' || $type == 'Textarea')
				{
					$query = $data->answer;
				}
				$ret .= '<option value="'.$cont->_id . '">'.$query .'</option>';
			}
		}
		echo $ret;
	}
	
	public function save_content_configuration()
	{
		if($_REQUEST['type'] == "Radio")
		{
			$get_det = ContentConfigure::where('content_id', $_REQUEST['content_id'])->get();
			
			if($get_det->count() > 0){
				foreach($get_det as $entry)
				{
					$res_data = json_decode($entry->response_content);
					if($res_data->response_option == $_REQUEST['query-option-conf'] ){
						$cID = ContentConfigure::find($entry->_id);
						$cID->delete();
					}
				}
			}
			$arr['response_option'] = $_REQUEST['query-option-conf'];
			$arr['response_type'] = $_REQUEST['response-type-conf'];
			$arr['response_cont_id'] = $_REQUEST['query-type-conf'];
			$content_data = json_encode($arr);
			$content_conf = New ContentConfigure();
			$content_conf->persona_id = $_REQUEST['persona_id'] ;
			$content_conf->content_id = $_REQUEST['content_id'] ; 
			$content_conf->response_content = $content_data;
			$content_conf->save();
		}
		else
		{
			
			$get_det = ContentConfigure::where('content_id', $_REQUEST['content_id'])->get();
			if($get_det->count()>0)
			{
				foreach($get_det as $entry)
				{
					$cID = ContentConfigure::find($entry->_id);
					$cID->delete();
				}
				//$res_data = json_decode($entry->response_content);
				
			}
			
			$arr['response_type'] = $_REQUEST['response-type-conf'];
			$arr['response_cont_id'] = $_REQUEST['query-type-conf'];
			//$arr['answer'] = $_REQUEST['answer'];
			
			$content_data = json_encode($arr);
			//die('test');
			$content_conf = New ContentConfigure();
			$content_conf->persona_id = $_REQUEST['persona_id'];
			$content_conf->content_id = $_REQUEST['content_id']; 
			$content_conf->response_content = $content_data;
			$content_conf->save();
		}
		return redirect('backoffice/persona/content/'.$_REQUEST['persona_id'])->with('success', 'Content has been successfully added');
	}
	
	public function get_chat_response()
	{
		date_default_timezone_set('Asia/Kolkata');
		$msg_pos=0;
		/* *****/
		$first_contentccc = Content::where('persona_id', $_REQUEST['persona_id'])->orderBy('content_cust_id', 'ASC')->first();
		/******/
		$first_content = Content::where('persona_id', $_REQUEST['persona_id'])->where('content_cust_id', $first_contentccc->content_cust_id )->first();
		$cont = json_decode($first_content->content) ;

		$personaDetail = Personas::find($_REQUEST['persona_id']);
		if($cont->type == "Text")
		{
			$contmsg = $cont->query ;
			
			$msg_pos++;
			
			echo '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
		}
		else if($cont->type == "Radio")
		{
			$contmsg = $cont->query ;
			$msg_pos++;
			
			$retStr = '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div><ul>';
			foreach($cont->answer as $options){
				$retStr .= '<li><input type="radio" name="'.$first_content->_id .'" value="'.$options.'">'.$options.'</li>';
			}
			 
			$retStr .= '</ul></div></div></div>';
			echo $retStr;
		}
		else if($cont->type == "File")
		{
			$contmsg = asset($cont->query) ;
			echo '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><p><img src="'.$contmsg.'" style="height:150px;width:150px" /></p></div></div></div>';
		}
		else if($cont->type == "Form")
		{
			//echo '<li><div class="chat_box chat_box_right"><div class="avatar"><img class="img-circle" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="txt"><p><img src="'.$contmsg.'" style="hieght:150px;width:150px" /></p><p><small>'.date('H:i').'</small></p></div></div></li>';
		}
		die;	
	}

	public function get_chat_backup_response()
	{
		date_default_timezone_set('Asia/Kolkata');
		$msg_pos=0;
		/* *****/
		$first_contentccc = Content::where('persona_id', $_REQUEST['persona_id'])->orderBy('_id', 'ASC')->first();
		
		//dd($first_contentccc);

		/******/
		$first_content = Content::where('persona_id', $_REQUEST['persona_id'])->where('content_cust_id', $first_contentccc->content_cust_id )->first();
		$cont = json_decode($first_content->content) ;

		$personaDetail = Personas::find($_REQUEST['persona_id']);

		/*====Start add chat with ip and email=====*/

		if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';

	    $type = $_GET['urlType'];
	    $userChat = new UserChatBackupMsg();
		$chatId = UserChatBackup::where('user_ip_address', $ipaddress)->where('uniqueId',SESSION::get('uniqueId'))->where('persona_id',$personaDetail->id)->where('url_type',$type)->first();

		$userChatMsg = array();
		if($chatId == '')
	    {
	    	$length = 7;
			$lifetime = time() + 60 * 60 * 24 * 365;
	    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    		$charactersLength = strlen($characters);
    		$randomString = '';
		    for ($i = 0; $i < $length; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
			SESSION::put('uniqueId', $randomString);
			SESSION::save();
	    	
	    	$personaMsg['msg'] = $cont->query;
	    	$personaMsg['type'] = $cont->type;
	    	$personaMsg['content_id'] = $first_content->_id;
	    	if(isset($cont->answer))
	    	{
	    		$personaMsg['answer'] = $cont->answer;
	    	}
	    	if(isset($cont->btnlabel))
	    	{
	    		$personaMsg['btnlabel'] = $cont->btnlabel;
	    	}
	    	$chatBackup = new UserChatBackup();
	    	$chatBackup->user_ip_address = $ipaddress;
	    	$chatBackup->uniqueId = $randomString;
	    	$chatBackup->email = '';
	    	$chatBackup->persona_id = $personaDetail->id;
	    	$chatBackup->industry_id = $personaDetail->industry;
	    	$chatBackup->brand_id = $personaDetail->brand;
	    	$chatBackup->compaign_id = $personaDetail->compaign;
	    	$chatBackup->chat_type = 'web';
	    	$chatBackup->url_type = $type;
	    	$chatBackup->clicks = '1';
	    	$chatBackup->save();
	    	$chat_id = $chatBackup->id;
	    	$userChat->chatId = $chat_id;
	    	$userChat->response = json_encode($personaMsg);
	    	$userChat->score = $first_contentccc->score;
	    	$userChat->user_msg = '';
	    	$userChat->save();
	    }
	    else
	    {
	    	$userChatMsg = UserChatBackupMsg::where('chatId',$chatId->id)->get();

	    }

	    $html = '';
	    if(count($userChatMsg) != 0)
	    {
	    	$i =1;

	    	/*if($personaDetail->require_login == 1)
	    	{
	    		$html .= '<h3>Please login to interact with Foxaie</h3><input type="button" value="Login with Facebook" class="btn btn-primary">  <input type="button" value="Login with Google" class="btn btn-warning">';

	    	}
	    	else
	    	{*/
	    	
		    	foreach($userChatMsg as $val)
		    	{
		    		if($val->response != '')
		    		{
		    			$personaContent = json_decode($val->response);
		    		}
		    		
		    		$userContent = json_decode($val->user_msg);
		    		
	    			if(!empty($userContent->msg))
		    		{
		    			$html .= '<div id="msg-text-'.$i.'" class="chat_content"><div class="media float-right"><div class="media-left"><img class="fox_icon" src="'.asset('admin/chat-img/other/user.png').'"></div><div class="media-body"><small>'.date('H:i',strtotime($val->updated_at)).'</small><div class="chat_txt bg_brown">'.$userContent->msg.'</div></div></div></div>';
		    		}
			    	

		    		if($val->response != '')
		    		{
		    			/*====If type radio===*/


		    			if($personaContent->type == 'Radio')
			    		{
			    			$answers = json_decode($personaContent->answer);
							$retStr = '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i',strtotime($val->updated_at)).'</small><div class="chat_txt bg_brown_light">'.$personaContent->msg.'</div><div class="chat_btn">';
							
							$i = 1;
							foreach($answers as $options)
							{
								if(isset($personaContent->selected))
								{
									if($options == $personaContent->selected)
									{
										$sel = "checked='checked'";
									}
									else
									{
										$sel = '';
									}
								}
								else
								{
									$sel = '';
								}
								$retStr .= '<a href="javascript:void();" class="bg_orange btn_radio"><input type="radio" id="test'.$i.'" name="'.$personaContent->content_id .'" onclick="chatBackupTry()" value="'.$options.'" '.$sel.'>'.$options.'<span class="checkmark"></span></a><p></p>';
								$i++;
							}
							$retStr .= '</div></div></div></div>';
							$html .=  $retStr;
						}

			    		/*====If type file====*/

			    		else if($personaContent->type == 'File')
			    		{

								$imagePath = asset($personaContent->msg);
								$html .= '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$personaContent->answer.'</div><div class=""><img src="'.$imagePath.'" style="height:150px;width:150px" /></div><p></p><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$personaContent->btnlabel.'"></div></div></div></div>';
						}
						
						else if($personaContent->type == "Docs")
						{
							$answer = '';
							if(isset($personaContent->answer))
							{
								$answer = $personaContent->answer;
							}
							$contmsg = $personaContent->msg;
							$imagePath = asset($contmsg);
							$docname = explode("/",$contmsg);
							$html .= '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_txt bg_brown_light">'.$docname[1].'</div><div class="chat_btn"><a href="'.$imagePath.'" download><input type="button" class="btn bg_orange" value="Download reading material"></a><input type="button" class="btn bg_orange response-btntype" value="'.$personaContent->btnlabel.'"></div></div></div></div>';
						}

						else if($personaContent->type == "Button")
						{
							$answer = '';
							if(isset($personaContent->answer))
							{
								$answer = $personaContent->answer;
							}
							$html .=  '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$personaContent->msg.'"></div></div></div></div>';
						}
						else if($personaContent->type == "Textarea")
						{
							$answer = '';
							if(isset($personaContent->answer))
							{
								$answer = $personaContent->answer;
							}
							$html .= '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light">'.$personaContent->msg.'</iframe></div></div></div></div>';
						}

			    		else if($personaContent->type == "Embed")
						{
							$answer = '';
							if(isset($personaContent->answer))
							{
								$answer = $personaContent->answer;
							}
							$html .= '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="embed_att">'.$personaContent->msg.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$personaContent->btnlabel.'"></div></div></div></div>';
						}
						

			    		/*====If type form====*/

			    		else if($personaContent->type == 'Form')
			    		{
			    			$formData = FormSubmissions::where('contentId',$personaContent->content_id)->first();
							$formItems = json_decode($personaContent->answer);
			    			$htmlstr = '';
			    		
			    			$htmlstr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$personaContent->content_id.'">';
			    			$htmlstr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$personaDetail->id.'">';
							$htmlstr .= '<input type="hidden" name="formId" value="form_'.$personaContent->content_id.'">';
							foreach($formItems as $formItem)
							{
								$getVal = '';
								$readonly = '';
								$disabled = '';
								if($formData != '')
								{
									$decodeFormVal = json_decode($formData->formData);
			    					$formVal = (array)$decodeFormVal;
			    				}
								
			    				if($formItem->type =='textarea')
								{ 
									if($formData != '')
									{
										$getVal =  $formVal[$formItem->name];
										$readonly = 'readonly';
									}
									$htmlstr.= '<div class="col-lg-6">
										<textarea name="'.$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" '.$readonly.'>'.$getVal.'</textarea>
										</div>';
								}
								else if($formItem->type =='checkbox-group')
								{
									if($formData != '')
									{
										$getVal =  $formVal[$formItem->name];
										$disabled = 'disabled';
									} 
									$htmlstr.= '<div class="col-lg-6">';
									foreach($formItem->values as $val)
									{
										
										if($val->value == $getVal)
										{
											$sel = "selected='selected'";
										}
										else
										{
											$sel ='';
										}
										
										$htmlstr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'" '.$sel.'>'.$val->value;
									}
									'</div>';
								}
								else if($formItem->type =='radio-group')
								{ 
									if($formData != '')
									{
										$getVal =  $formVal[$formItem->name];
										$disabled = 'disabled';
									}
									$htmlstr.= '<div class="col-lg-6">';
									foreach($formItem->values as $val)
									{
										if($val->value == $getVal[0])
										{
											$sel = "checked='checked'";
										}
										else
										{
											$sel ='';
										}
										$htmlstr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'" '.$sel.'>'.$val->value;
									}
									'</div>';
								}
								else if($formItem->type =='select')
								{ 
									if($formData != '')
									{
										$getVal =  $formVal[$formItem->name];
										$disabled = 'disabled';
									}
									$htmlstr.= '<div class="col-lg-6">
										<select class="form-control mr-3 '.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required '.$disabled.'>';
									foreach($formItem->values as $slectOptions)
									{
										if($slectOptions->value == $getVal)
										{
											$sel = "selected='selected'";
										}
										else
										{
											$sel ='';
										}
										
										$htmlstr.= '<option value="'.$slectOptions->value .'" '.$sel.'>'.$slectOptions->label .'</option>';
									}
									$htmlstr.='</select></div>';
								}
								else if($formItem->type =='text')
								{
									if($formData != '')
									{
										$getVal =  $formVal[$formItem->name];
										$readonly = 'readonly';
									} 
									$htmlstr.= '<div class="col-lg-6">
										<input type="text" name="' .$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" value="'.$getVal.'" '.$readonly.' placeholder="Text">
									</div>';
									
								}
								else if($formItem->type =='number')
								{ 
									if($formData != '')
									{
										$getVal =  $formVal[$formItem->name];
										$readonly = 'readonly';
									}
									$htmlstr.= '<div class="col-lg-6">
										<input type="number" name="'. $formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" value="'.$getVal.'" '.$readonly.'>
									</div>';
								}
								else if($formItem->type =='date')
								{
									if($formData != '')
									{
										$getVal =  $formVal[$formItem->name];
										$readonly = 'readonly';
									}
									$htmlstr.= '<div class="col-lg-6">
										<input type="date" name="'.$formItem->name .'" id="'.$formItem->name.'" class="'. $formItem->className .'" value="'.$getVal.'" '.$readonly.'>
									</div>';
								}
								else if($formItem->type =='file')
								{
									//print_r($formVal['upload-'.$formItem->name]);die;
									/*if($formData != '')
									{
										$getVal =  $formVal[$formItem->name].'[]';
									} */
									$htmlstr.= '<div class="col-lg-12"><div class="file_upload"><span id="result_name"></span> <i class="fa fa-cloud-upload"></i>
										<input type="file" id="upload_input" name="'.$formItem->name .'[]" id="'.$formItem->name.'" class="'.$formItem->className .'" multiple>
									</div>';

									if($formData != '')
									{

										$htmlstr .= '<div class="attachment">';
											foreach($formVal['upload-'.$formItem->name] as $val)
											{
												$htmlstr .= '<img src="'.asset('uploads/formFiles/'.$val).'" class="file_img">';
											}
										'</div>';
										
									}
									$htmlstr .= '</div></div>';
								}
								else if($formItem->type =='button')
								{ 
									if($formData != '')
									{
										$disabled = 'disabled';
									}
									$formId = 'form_'.$personaContent->content_id;
									$htmlstr.= '<div class="chat_btn mt-2 text-right">
										<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="bg_orange '. $formItem->className .'" onclick="submitForm('.$formId.')" '.$disabled.'>
										</div>';
										
								}
							}
							$html .= '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light"></div><div class="form mt-2"><div class="row">'.$htmlstr.'</div></div></div></div>';
			
			    		}
			    		else
			    		{
			    			$html .= '<div id="bot_response_'.$personaContent->type .'_'.$personaContent->content_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i',strtotime($val->updated_at)).'</small><div class="chat_txt bg_brown_light">'.$personaContent->msg.'</div></div></div></div></div></div>';
			    		}
			    		
				
		    		}
		    	}
		    //}
			$i++;
	    }
	    else
	    {
			if($cont->type == "Text")
			{
				$contmsg = $cont->query ;
				
				$msg_pos++;
				
				$html .= '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div></div></div></div>';
			}
			else if($cont->type == "Embed")
			{
				$answer = '';
				if(isset($cont->answer))
				{
					$answer = $cont->answer;
				}
				$contmsg = $cont->query ;
				
				$msg_pos++;
				
				$html .= '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="embed_att">'.$contmsg.'</div></div></div>';
			}
			else if($cont->type == "Textarea")
			{
				$answer = '';
				if(isset($cont->answer))
				{
					$answer = $cont->answer;
				}
				$contmsg = $cont->query ;
				$html .= '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light">'.$contmsg.'</iframe></div></div></div></div>';
			}
			else if($cont->type == "Button")
			{
				$answer = '';
				if(isset($cont->answer))
				{
					$answer = $cont->answer;
				}
				$contmsg = $cont->query ;
				$html .=  '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contmsg.'"></div></div></div></div>';
			}
			else if($cont->type == "Radio")
			{
				$contmsg = $cont->query ;
				$retStr = '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div><div class="chat_btn">';
				
				$i = 1;
				foreach($cont->answer as $options)
				{
					$retStr .= '<a href="javascript:void();" class="bg_orange btn_radio"><input type="radio" id="test'.$i.'" name="'.$first_content->_id .'" onclick="chatBackupTry()" value="'.$options.'">'.$options.'<span class="checkmark"></span></a><p></p>';
					$i++;
				}
				$retStr .= '</div></div></div></div>';
				$html .=  $retStr;
			}
			else if($cont->type == "File")
			{
				$contmsg = asset($cont->query) ;
				$html .= '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$cont->answer.'</div><div class=""><img src="'.$contmsg.'" style="height:150px;width:150px" /></div><p></p><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$cont->btnlabel.'"></div></div></div></div>';
			}
			else if($cont->type == "Docs")
			{
				$answer = '';
				if(isset($cont->answer))
				{
					$answer = $cont->answer;
				}
				$contmsg = $cont->query;
				$imagePath = asset($contmsg);
				$docname = explode("/",$contmsg);
				$html .= '<div id="bot_response_'.$cont->type .'_'.$first_content->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_brown_light">'.$docname[1].'</div><div class="chat_btn"><a href="'.$imagePath.'" download><input type="button" class="btn bg_orange" value="Download reading material"></a><input type="button" class="btn bg_orange response-btntype" value="'.$cont->btnlabel.'"></div></div></div></div>';
			}
			else if($cont->type == "Form")
			{
				//echo '<li><div class="chat_box chat_box_right"><div class="avatar"><img class="img-circle" src="https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48"></div><div class="txt"><p><img src="'.$contmsg.'" style="hieght:150px;width:150px" /></p><p><small>'.date('H:i').'</small></p></div></div></li>';
			}
		}
		$arr['html'] = $html;
		$arr['type'] = $cont->type;
		echo json_encode($arr);exit;
	  
	}

/*=========Test chat backup response when user chat on foxai========*/

	public function try_chat_backup_response()
	{
		date_default_timezone_set('Asia/Kolkata'); 
		$persona_id = $_REQUEST['persona_id'];
		$last_bot_msg_det = $_REQUEST['msg_det'];
		$detAr = explode("_", $last_bot_msg_det);
		$lastMsgType= $detAr[2];
		$personaDetail = Personas::find($_REQUEST['persona_id']);
		$html = '';
		$contType = '';
		$response = ContentConfigure::where('persona_id', $_REQUEST['persona_id'])->where('content_id', $detAr[3])->first();
		$response_det = json_decode($response->response_content);
		$response_id = $response_det->response_cont_id ; 
		$get_cont = Content::find($response_id);
		$contDet = json_decode($get_cont->content) ;
		$contType = $contDet->type;

		/*====if last message text====*/

		if($lastMsgType == 'Text')
		{
			
			
			if($contDet->type == "Text")
			{
				$contmsg = $contDet->query ;
				$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div></div></div></div>';
			}
			else if($contDet->type == "Button")
			{
				$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$contmsg = $contDet->query ;
				$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contmsg.'"></div></div></div></div>';
			}
			else if($contDet->type == "Embed")
			{
				$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$contmsg = $contDet->query ;
				$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content embed_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="embed_att">'.$contmsg.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
				
			}
			else if($contDet->type == "Textarea")
			{
				$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$contmsg = $contDet->query ;
				$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light"><a href="'.$contmsg.'" target="_blank">'.$contmsg.'</a></iframe></div></div></div></div>';
			}
			else if($contDet->type == "Link")
			{
				$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$contmsg = $contDet->query ;
				$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
				
			}
			else if($contDet->type == "Radio")
			{
				$contmsg = $contDet->query ;
				$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div><div class="chat_btn">';
				
				$i = 1;
				foreach($contDet->answer as $options)
				{
					$retStr .= '<a href="javascript:void();" class="bg_orange btn_radio"><input type="radio" id="test'.$i.'" name="'.$get_cont->_id .'" onclick="chatBackupTry()" value="'.$options.'">'.$options.'<span class="checkmark"></span></a><p></p>';
					$i++;
				}
				$retStr .= '</div></div></div></div>';
				$html =  $retStr;  
			}
			else if($contDet->type == "Checkbox")
			{
				$contmsg = $contDet->query ;
				
				$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div><p>';
				foreach($contDet->answer as $options)
				{
					$retStr .= '<input type="checkbox" name="'.$get_cont->_id .'" onclick="chatBackupTry()" value="'.$options.'">'.$options.'&nbsp;&nbsp;&nbsp;';
				}
				$retStr .= '</p></div></div></div>';
				$html = $retStr;  
			}
			else if($contDet->type == "File")
			{
				$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$contmsg = $contDet->query;
				$imagePath = asset($contmsg);
				$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class=""><img src="'.$imagePath.'" style="hieght:150px;width:150px" /></div><p></p><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
			}

			else if($contDet->type == "Docs")
			{
				$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$contmsg = $contDet->query;
				$imagePath = asset($contmsg);
				$docname = explode("/",$contmsg);
				$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_txt bg_brown_light">'.$docname[1].'</div><div class="chat_btn"><a href="'.$imagePath.'" download><input type="button" class="btn bg_orange" value="Download reading material"></a><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
			}
			else if($contDet->type == "Form")
			{
				$htmlStr ="";
				$oparr =$contDet->formcont ;
				$formItems = json_decode($oparr);
				$htmlStr.=$contDet->query;
				$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
				$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
				$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
				$htmlStr .= '<input type="hidden" name="contentId" value="'.$get_cont->_id.'">';
				foreach($formItems as $formItem)
				{
					if($formItem->type =='textarea')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'. $formItem->label .'</label>
							<textarea name="'.$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'"> </textarea>
							</div>';
					}

					else if($formItem->type =='checkbox-group')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'. $formItem->label .'</label>';
						foreach($formItem->values as $val)
						{
							$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
						}
						'</div>';
					}
					else if($formItem->type =='radio-group')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'. $formItem->label .'</label>';
						foreach($formItem->values as $val)
						{
							$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
						}
						'</div>';
					}
					else if($formItem->type =='select')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'.$formItem->label .'</label>
							<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
								foreach($formItem->values as $slectOptions){
									$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
								}
						$htmlStr.='</select></div>';

					}
					else if($formItem->type =='text')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">' .$formItem->label .'</label>
							<input type="text" id="'.$formItem->name.'" name="' .$formItem->name .'" class="'.$formItem->className .'" >
						</div>';
					}
					else if($formItem->type =='number')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'.$formItem->label .'</label>
							<input type="number" id="'.$formItem->name.'" name="'. $formItem->name .'" class="'.$formItem->className .'" >
						</div>';
					}
					else if($formItem->type =='date')
					{
						$htmlStr.= '<div class="form-group">
							<label for="type">'. $formItem->label .'</label>
							<input type="date" id="'.$formItem->name.'" name="'.$formItem->name .'" class="'. $formItem->className .'" >
						</div>';
					}
					else if($formItem->type =='file')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'.$formItem->label .'</label>
							<input type="file" id="'.$formItem->name.'" name="'.$formItem->name .'[]" class="'.$formItem->className .'" multiple>
						</div>';
					} 
					else if($formItem->type =='button')
					{ 
						$formId = 'form_'.$get_cont->_id;
						$htmlStr.= '<div class="form-group">
							<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
							</div>';
					}
				} 
				$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
			}
		}

		/*=======if last message radio======*/

		else if($lastMsgType == "Radio")
		{
			$response = ContentConfigure::where('persona_id', $_REQUEST['persona_id'])->where('content_id', $detAr[3])->get();

			foreach($response as $resp)
			{
				$response_det = json_decode($resp->response_content);
				if(isset($response_det->response_option))
				{
					if($response_det->response_option == $_REQUEST['user_msg'])
					{
						$response_id = $response_det->response_cont_id ; 
					$get_cont = Content::find($response_id);

					$contDet = json_decode($get_cont->content);
					$contType = $contDet->type;

					if($contDet->type == "Text")
					{
						$contmsg = $contDet->query ;
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div></div></div></div>';
					}
					else if($contDet->type == "Button")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query ;
						$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contmsg.'"></div></div></div></div>';
					}
					else if($contDet->type == "Embed")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query ;
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content embed_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="embed_att">'.$contmsg.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
						
					}
					else if($contDet->type == "Textarea")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query ;
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light">'.$contmsg.'</iframe></div></div></div></div>';
						
					}
					else if($contDet->type == "Radio")
					{
						$contmsg = $contDet->query ;
						$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div><div class="chat_btn">';
						
						$i = 1;
						foreach($contDet->answer as $options)
						{
							$retStr .= '<a href="javascript:void();" class="bg_orange btn_radio"><input type="radio" id="test'.$i.'" name="'.$get_cont->_id .'" onclick="chatBackupTry()" value="'.$options.'">'.$options.'<span class="checkmark"></span></a><p></p>';
							$i++;
						}
						$retStr .= '</div></div></div></div>';
						$html =  $retStr;
					}
					else if($contDet->type == "File")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query;
						$imagePath = asset($contmsg);
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class=""><img src="'.$imagePath.'" style="hieght:150px;width:150px" /></div><p></p><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
					}
					else if($contDet->type == "Docs")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query;
						$imagePath = asset($contmsg);
						$docname = explode("/",$contmsg);
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_txt bg_brown_light">'.$docname[1].'</div><div class="chat_btn"><a href="'.$imagePath.'" download><input type="button" class="btn bg_orange" value="Download reading material"></a><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
					}
					else if($contDet->type == "Link")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query ;
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light"><a href="'.$contmsg.'" target="_blank">'.$contmsg.'</a></div></div></div></div>';
						
					}
					else if($contDet->type == "Form")
					{
						$htmlStr ="";
						$oparr =$contDet->formcont ;
						$formItems = json_decode($oparr);
						$htmlStr.=$contDet->query;
						$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
						$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
						$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
						$htmlstr .= '<input type="hidden" name="contentId" value="'.$get_cont->_id.'">';
						foreach($formItems as $formItem)
						{
							if($formItem->type =='textarea')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label.'</label>
									<textarea id="'.$formItem->name.'" name="'.$formItem->name .'" class="'.$formItem->className .'"> </textarea>
									</div>';
							}
							else if($formItem->type =='checkbox-group')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>';
								foreach($formItem->values as $val)
								{
									$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
								}
								'</div>';
							}
							else if($formItem->type =='radio-group')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>';
								foreach($formItem->values as $val)
								{
									$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
								}
								'</div>';
							}
							else if($formItem->type =='select')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
										foreach($formItem->values as $slectOptions)
										{
											$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
										}
									$htmlStr.='</select></div>';
						
							}

							else if($formItem->type =='text')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">' .$formItem->label .'</label>
									<input type="text" id="'.$formItem->name.'" name="' .$formItem->name .'" class="'.$formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='number')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<input type="number" id="'.$formItem->name.'" name="'. $formItem->name .'" class="'.$formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='date')
							{
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>
									<input type="date" id="'.$formItem->name.'" name="'.$formItem->name .'" class="'. $formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='file')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<input type="file" id="'.$formItem->name.'" name="'.$formItem->name .'[]" class="'.$formItem->className .'" multiple>
								</div>';
							}
							else if($formItem->type =='button')
							{ 
								$formId = 'form_'.$get_cont->_id;
								$htmlStr.= '<div class="form-group">
									<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
									</div>';

							}
						} 
						$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
					}
					}
				}
				else
				{
					
					$response_id = $response_det->response_cont_id ; 
					$get_cont = Content::find($response_id);

					$contDet = json_decode($get_cont->content);
					$contType = $contDet->type;

					if($contDet->type == "Text")
					{

						$contmsg = $contDet->query ;
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div></div></div></div>';

					}
					else if($contDet->type == "Button")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query ;
						$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contmsg.'"></div></div></div></div>';
					}
					else if($contDet->type == "Embed")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query ;
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content embed_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="embed_att">'.$contmsg.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
						
					}
					else if($contDet->type == "Textarea")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query ;
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light">'.$contmsg.'</iframe></div></div></div></div>';
						
					}
					else if($contDet->type == "Radio")
					{
						$contmsg = $contDet->query ;
						$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div><div class="chat_btn">';
						
						$i = 1;
						foreach($contDet->answer as $options)
						{
							$retStr .= '<a href="javascript:void();" class="bg_orange btn_radio"><input type="radio" id="test'.$i.'" name="'.$get_cont->_id .'" onclick="chatBackupTry()" value="'.$options.'">'.$options.'<span class="checkmark"></span></a><p></p>';
							$i++;
						}
						$retStr .= '</div></div></div></div>';
						$html =  $retStr;
					}
					else if($contDet->type == "File")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query;
						$imagePath = asset($contmsg);
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class=""><img src="'.$imagePath.'" style="hieght:150px;width:150px" /></div><p></p><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
					}
					else if($contDet->type == "Docs")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query;
						$imagePath = asset($contmsg);
						$docname = explode("/",$contmsg);
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_txt bg_brown_light">'.$docname[1].'</div><div class="chat_btn"><a href="'.$imagePath.'" download><input type="button" class="btn bg_orange" value="Download reading material"></a><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
					}
					else if($contDet->type == "Link")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query ;
						$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light"><a href="'.$contmsg.'" target="_blank">'.$contmsg.'</a></div></div></div></div>';
						
					}
					else if($contDet->type == "Form")
					{
						$htmlStr ="";
						$oparr =$contDet->formcont ;
						$formItems = json_decode($oparr);
						$htmlStr.=$contDet->query;
						$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
						$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
						$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
						$htmlstr .= '<input type="hidden" name="contentId" value="'.$get_cont->_id.'">';
						foreach($formItems as $formItem)
						{
							if($formItem->type =='textarea')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label.'</label>
									<textarea id="'.$formItem->name.'" name="'.$formItem->name .'" class="'.$formItem->className .'"> </textarea>
									</div>';
							}
							else if($formItem->type =='checkbox-group')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>';
								foreach($formItem->values as $val)
								{
									$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
								}
								'</div>';
							}
							else if($formItem->type =='radio-group')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>';
								foreach($formItem->values as $val)
								{
									$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
								}
								'</div>';
							}
							else if($formItem->type =='select')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
										foreach($formItem->values as $slectOptions)
										{
											$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
										}
									$htmlStr.='</select></div>';
						
							}

							else if($formItem->type =='text')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">' .$formItem->label .'</label>
									<input type="text" id="'.$formItem->name.'" name="' .$formItem->name .'" class="'.$formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='number')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<input type="number" id="'.$formItem->name.'" name="'. $formItem->name .'" class="'.$formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='date')
							{
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>
									<input type="date" id="'.$formItem->name.'" name="'.$formItem->name .'" class="'. $formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='file')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<input type="file" id="'.$formItem->name.'" name="'.$formItem->name .'[]" class="'.$formItem->className .'" multiple>
								</div>';
							}
							else if($formItem->type =='button')
							{ 
								$formId = 'form_'.$get_cont->_id;
								$htmlStr.= '<div class="form-group">
									<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
									</div>';

							}
						} 
						$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
					}
				}
			}
		}

		/*=======if last message form========*/

		else if($lastMsgType == "Form")
		{
			$response = ContentConfigure::where('persona_id', $_REQUEST['persona_id'])->where('content_id', $detAr[3])->get();
			foreach($response as $resp)
			{
				$response_det = json_decode($resp->response_content);
				$response_id = $response_det->response_cont_id ; 
				$get_cont = Content::find($response_id);
				$contDet = json_decode($get_cont->content) ;
				$contType = $contDet->type;
				if($contDet->type == "Text")
				{
					$contmsg = $contDet->query ;
					if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !="")
					{
						$chat_u = New UserChat();
						$chat_u->user_email = $_REQUEST['user_id'];
						$chat_u->persona_msg = $contmsg; 
						$chat_u->user_msg = $_REQUEST['user_msg'];
						$chat_u->save();
					}
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div></div></div></div>';
				}
				else if($contDet->type == "Button")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query ;
					$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contmsg.'"></div></div></div></div>';
				}
				else if($contDet->type == "Link")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query ;
					$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light"><a href="'.$contmsg.'" target="_blank">'.$contmsg.'</a></div></div></div></div>';
					
				}
				else if($contDet->type == "Docs")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query;
					$imagePath = asset($contmsg);
					$docname = explode("/",$contmsg);
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_txt bg_brown_light">'.$docname[1].'</div><div class="chat_btn"><a href="'.$imagePath.'" download><input type="button" class="btn bg_orange" value="Download reading material"></a><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
				}
				else if($contDet->type == "Embed")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query ;
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content embed_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="embed_att">'.$contmsg.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
				}
				else if($contDet->type == "Textarea")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query ;
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light">'.$contmsg.'</iframe></div></div></div></div>';
					
				}
				else if($contDet->type == "Radio")
				{
					$contmsg = $contDet->query ;
					$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div><div class="chat_btn">';
					
					$i = 1;
					foreach($contDet->answer as $options)
					{
						$retStr .= '<a href="javascript:void();" class="bg_orange btn_radio"><input type="radio" id="test'.$i.'" name="'.$get_cont->_id .'" onclick="chatBackupTry()" value="'.$options.'">'.$options.'<span class="checkmark"></span></a><p></p>';
						$i++;
					}
					$retStr .= '</div></div></div></div>';
					$html =  $retStr;
				}
				else if($contDet->type == "File")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query;
					$imagePath = asset($contmsg);
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class=""><img src="'.$imagePath.'" style="hieght:150px;width:150px" /></div><p></p><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
				}
				else if($contDet->type == "Form")
				{
					$htmlStr ="";
					$oparr =$contDet->formcont ;
					$formItems = json_decode($oparr);
					$htmlStr.=$contDet->query;
					$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
					$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
					$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
					$htmlStr .= '<input type="hidden" name="contentId" value="'.$get_cont->_id.'">';
					foreach($formItems as $formItem)
					{
						if($formItem->type =='textarea')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label.'</label>
								<textarea name="'.$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'"> </textarea>
								</div>';
						}
						else if($formItem->type =='select')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
							foreach($formItem->values as $slectOptions)
							{
								$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
							}
							$htmlStr.='</select></div>';
						}
						else if($formItem->type =='checkbox-group')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>';
							foreach($formItem->values as $val)
							{
								$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
							}
							'</div>';
						}
						else if($formItem->type =='radio-group')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>';
							foreach($formItem->values as $val)
							{
								$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
							}
							'</div>';
						}
						else if($formItem->type =='text')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">' .$formItem->label .'</label>
								<input type="text" id="'.$formItem->name.'" name="' .$formItem->name .'" class="'.$formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='number')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<input type="number" id="'.$formItem->name.'" name="'. $formItem->name .'" class="'.$formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='date')
						{
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>
								<input type="date" id="'.$formItem->name.'" name="'.$formItem->name .'" class="'. $formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='file')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<input type="file" id="'.$formItem->name.'" name="'.$formItem->name .'[]" class="'.$formItem->className .'" multiple>
							</div>';
						}
						else if($formItem->type =='button')
						{ 
							$formId = 'form_'.$get_cont->_id;
							$htmlStr.= '<div class="form-group">
								<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
								</div>';
						}
					} 
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
				}
			}
		}
		else 
		{
			$response = ContentConfigure::where('persona_id', $_REQUEST['persona_id'])->where('content_id', $detAr[3])->get();
			foreach($response as $resp)
			{
				$response_det = json_decode($resp->response_content);
				$response_id = $response_det->response_cont_id ; 
				$get_cont = Content::find($response_id);
				$contDet = json_decode($get_cont->content);
				$contType = $contDet->type;
				
				if($contDet->type == "Text")
				{
					$contmsg = $contDet->query ;
					if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !="")
					{
						$chat_u = New UserChat();
						$chat_u->user_email = $_REQUEST['user_id'];
						$chat_u->persona_msg = $contmsg; 
						$chat_u->user_msg = $_REQUEST['user_msg'];
						$chat_u->save();
					}
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div></div></div></div>';
				}
				else if($contDet->type == "Button")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query ;
					$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contmsg.'"></div></div></div></div>';
				}
				else if($contDet->type == "Embed")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query ;
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content embed_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="embed_att">'.$contmsg.'</div><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
					
				}
				else if($contDet->type == "Textarea")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query ;
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light">'.$contmsg.'</iframe></div></div></div></div>';
					
				}
				else if($contDet->type == "Link")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query ;
					$html =  '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<div class="chat_txt bg_light"><a href="'.$contmsg.'" target="_blank">'.$contmsg.'</a></div></div></div></div>';
					
				}
				else if($contDet->type == "Radio")
				{
					$contmsg = $contDet->query ;
					$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$contmsg.'</div><div class="chat_btn">';
					
					$i = 1;
					foreach($contDet->answer as $options)
					{
						$retStr .= '<a href="javascript:void();" class="bg_orange btn_radio"><input type="radio" id="test'.$i.'" name="'.$get_cont->_id .'" onclick="chatBackupTry()" value="'.$options.'">'.$options.'<span class="checkmark"></span></a><p></p>';
						$i++;
					}
					$retStr .= '</div></div></div></div>';
					$html =  $retStr;
				}
				else if($contDet->type == "File")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query;
					$imagePath = asset($contmsg);
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class=""><img src="'.$imagePath.'" style="hieght:150px;width:150px" /></div><p></p><div class="chat_btn"><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
				}
				else if($contDet->type == "Docs")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query;
					$imagePath = asset($contmsg);
					$docname = explode("/",$contmsg);
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><img class="fox_icon mr-3" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_brown_light">'.$answer.'</div><div class="chat_txt bg_brown_light">'.$docname[1].'</div><div class="chat_btn"><a href="'.$imagePath.'" download><input type="button" class="btn bg_orange" value="Download reading material"></a><input type="button" class="btn bg_orange response-btntype" value="'.$contDet->btnlabel.'"></div></div></div></div>';
				}
				else if($contDet->type == "Form")
				{
					$htmlStr ="";
					$oparr =$contDet->formcont ;
					$formItems = json_decode($oparr);
					$htmlStr.=$contDet->query;
					$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
					$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
					$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
					$htmlStr .= '<input type="hidden" name="contentId" value="'.$get_cont->_id.'">';
					
					foreach($formItems as $formItem)
					{
						if($formItem->type =='textarea')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label.'</label>
								<textarea name="'.$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'"> </textarea>
								</div>';
						}
						/*if($formItem->type =='autocomplete')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label.'</label>';
								foreach($formItem->values as $val)
								{
									$htmlStr.= '<input type="checkbox" name="'.$formItem->label.'[]" class="" value="'.$val->value.'">'.$val->value;
								}
								'</div>';
						}*/
						else if($formItem->type =='checkbox-group')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>';
							foreach($formItem->values as $val)
							{
								$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
							}
							'</div>';
						}
						else if($formItem->type =='radio-group')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>';
							foreach($formItem->values as $val)
							{
								$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
							}
							'</div>';
						}
						else if($formItem->type =='select')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
							foreach($formItem->values as $slectOptions)
							{
								$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
							}
							$htmlStr.='</select></div>';
						}
						else if($formItem->type =='text')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">' .$formItem->label .'</label>
								<input type="text" name="' .$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='number')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<input type="number" name="'. $formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='date')
						{
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>
								<input type="date" name="'.$formItem->name .'" id="'.$formItem->name.'" class="'. $formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='file')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<input type="file" name="'.$formItem->name .'[]" id="'.$formItem->name.'" class="'.$formItem->className .'" multiple>
							</div>';
						}
						else if($formItem->type =='button')
						{ 
							$formId = 'form_'.$get_cont->_id;
							$htmlStr.= '<div class="form-group">
								<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
								</div>';
						}
					} 
					$html = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
				}
			}
		}

		/*======Start add user and persona chat======*/

		if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    $urltype = $_GET['urlType'];
	   	$chat = UserChatBackup::where('user_ip_address', $ipaddress)->where('uniqueId',SESSION::get('uniqueId'))->where('persona_id',$persona_id)->where('url_type',$urltype)->first();
		$chat_id = $chat->id;
    	$previousChatMsg = UserChatBackUpMsg::where('chatId',$chat_id)->orderBy('_id','desc')->first();
    	$preResp = json_decode($previousChatMsg->response);
    	if($preResp->type == 'Radio')
    	{
    		$preResp->selected = $_REQUEST['user_msg'];
    		$previousChatMsg->response = json_encode($preResp);
    		$previousChatMsg->save();
    	}
		$userMsg['msg'] = $_REQUEST['user_msg'];
	    $chatMsg = new UserChatBackUpMsg();
	    $chatMsg->chatId = $chat_id;
	    $chatMsg->user_msg = json_encode($userMsg);
	    $chatMsg->score = $get_cont->score;
	    if(isset($contDet))
	    {

	    	$personaMsg['msg'] = '';
	    	if($contDet->type != 'Form')
	    	{
	    		$personaMsg['msg'] = $contmsg;
	    	}
	    	$personaMsg['type'] = $contDet->type;
		    if($contDet->type == 'Radio')
		    {
		    	$personaMsg['answer'] = json_encode($contDet->answer);
		    }

		    else if($contDet->type == "File")
		    {
		    	$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$personaMsg['answer'] = $answer;
				$personaMsg['btnlabel'] = $contDet->btnlabel;

		    }
		    else if($contDet->type == "Docs")
		    {
		    	$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$personaMsg['answer'] = $answer;
				$personaMsg['btnlabel'] = $contDet->btnlabel;

		    }
		    else if($contDet->type == "Button" || $contDet->type == "Embed")
			{
				$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$personaMsg['answer'] = $answer;
				if($contDet->type == 'Embed')
				{
					$personaMsg['btnlabel'] = $contDet->btnlabel;
				}
				
			}
			else if($contDet->type == "Form")
			{
				$personaMsg['answer'] = $contDet->formcont;
			}
		    $personaMsg['content_id'] = $get_cont->_id;
		    $chatMsg->response = json_encode($personaMsg);
		    //print_r($contDet);die;
	    }
	    else
	    {
	    	$chatMsg->response = '';
	    }
	   	$chatMsg->save();

	   	$htmlResponse['html'] = $html;
	   	$htmlResponse['type'] = $contType;
	   	if(isset($contDet->email))
	   	{
	   		$htmlResponse['email'] = 'email';
	   	}
	   	echo json_encode($htmlResponse);exit;

		/*======End add user and persona chat======*/
	}

/*============Chat response when user chat on Foxai=============*/
	
	public function try_chat_response()
	{
		date_default_timezone_set('Asia/Kolkata'); 
		$persona_id = $_REQUEST['persona_id'];
		$last_bot_msg_det = $_REQUEST['msg_det'];
		$detAr = explode("_", $last_bot_msg_det);
		$lastMsgType= $detAr[2];
		$personaDetail = Personas::find($_REQUEST['persona_id']);
		if($lastMsgType == 'Text')
		{
			$response = ContentConfigure::where('persona_id', $_REQUEST['persona_id'])->where('content_id', $detAr[3])->first();
			$response_det = json_decode($response->response_content);
			$response_id = $response_det->response_cont_id ; 
			$get_cont = Content::find($response_id);
			$contDet = json_decode($get_cont->content) ;
			if($contDet->type == "Text")
			{
				$contmsg = $contDet->query ;
				echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
				
			}
			else if($contDet->type == "Link")
			{
				$contmsg = $contDet->query ;
				echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
				
			}
			else if($contDet->type == "Radio")
			{
				$contmsg = $contDet->query ;
				
				$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div><p>';
				foreach($contDet->answer as $options)
				{
					$retStr .= '<input type="radio" name="'.$get_cont->_id .'" onclick="chatTry()" value="'.$options.'">'.$options.'&nbsp;&nbsp;&nbsp;';
				}
				$retStr .= '</p></div></div></div>';
				echo $retStr;  
			}
			else if($contDet->type == "Checkbox")
			{
				$contmsg = $contDet->query ;
				
				$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div><p>';
				foreach($contDet->answer as $options)
				{
					$retStr .= '<input type="checkbox" name="'.$get_cont->_id .'" onclick="chatTry()" value="'.$options.'">'.$options.'&nbsp;&nbsp;&nbsp;';
				}
				$retStr .= '</p></div></div></div>';
				echo $retStr;  
			}
			else if($contDet->type == "File")
			{
				$answer = '';
				if(isset($contDet->answer))
				{
					$answer = $contDet->answer;
				}
				$contmsg = $contDet->query;
				$imagePath = asset($contmsg);
				echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="class="media-left""><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<p>
					<img src="'.$imagePath.'" style="hieght:150px;width:150px" /></p></div></div></div>';
			}
			else if($contDet->type == "Form")
			{
				$htmlStr ="";
				$oparr =$contDet->formcont ;
				$formItems = json_decode($oparr);
				$htmlStr.=$contDet->query;
				$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
				$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
				$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
				foreach($formItems as $formItem)
				{
					if($formItem->type =='textarea')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'. $formItem->label .'</label>
							<textarea name="'.$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'"> </textarea>
							</div>';
					}
					else if($formItem->type =='checkbox-group')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'. $formItem->label .'</label>';
						foreach($formItem->values as $val)
						{
							$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
						}
						'</div>';
					}
					else if($formItem->type =='radio-group')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'. $formItem->label .'</label>';
						foreach($formItem->values as $val)
						{
							$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
						}
						'</div>';
					}
					else if($formItem->type =='select')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'.$formItem->label .'</label>
							<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
								foreach($formItem->values as $slectOptions){
									$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
								}
						$htmlStr.='</select></div>';

					}
					else if($formItem->type =='text')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">' .$formItem->label .'</label>
							<input type="text" id="'.$formItem->name.'" name="' .$formItem->name .'" class="'.$formItem->className .'" >
						</div>';
					}
					else if($formItem->type =='number')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'.$formItem->label .'</label>
							<input type="number" id="'.$formItem->name.'" name="'. $formItem->name .'" class="'.$formItem->className .'" >
						</div>';
					}
					else if($formItem->type =='date')
					{
						$htmlStr.= '<div class="form-group">
							<label for="type">'. $formItem->label .'</label>
							<input type="date" id="'.$formItem->name.'" name="'.$formItem->name .'" class="'. $formItem->className .'" >
						</div>';
					}
					else if($formItem->type =='file')
					{ 
						$htmlStr.= '<div class="form-group">
							<label for="type">'.$formItem->label .'</label>
							<input type="file" id="'.$formItem->name.'" name="'.$formItem->name .'[]" class="'.$formItem->className .'" multiple>
						</div>';
					} 
					else if($formItem->type =='button')
					{ 
						$formId = 'form_'.$get_cont->_id;
						$htmlStr.= '<div class="form-group">
							<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
							</div>';
					}
				} 
				echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
			}
		}
		else if($lastMsgType == "Radio")
		{
			$response = ContentConfigure::where('persona_id', $_REQUEST['persona_id'])->where('content_id', $detAr[3])->get();
			foreach($response as $resp)
			{
				if(strpos($resp->response_content, $_REQUEST['user_msg']) == true)
				{
					$response_det = json_decode($resp->response_content);
					$response_id = $response_det->response_cont_id ; 
					$get_cont = Content::find($response_id);
					$contDet = json_decode($get_cont->content) ;
					if($contDet->type == "Text")
					{
						$contmsg = $contDet->query ;
						echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
					}
					else if($contDet->type == "Radio")
					{
						$contmsg = $contDet->query ;
						
						$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div><p>';
						foreach($contDet->answer as $options)
						{
							$retStr1 .= '<input type="radio" name="'.$get_cont->_id .'" onclick="chatTry()" value="'.$options.'">'.$options.'&nbsp;&nbsp;&nbsp;';
						}
						$retStr .= $retStr1;
						$retStr .= '</p></div></div></div>';
						echo $retStr;
					}
					else if($contDet->type == "File")
					{
						$answer = '';
						if(isset($contDet->answer))
						{
							$answer = $contDet->answer;
						}
						$contmsg = $contDet->query;
						$imagePath = asset($contmsg);
						echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="class="media-left""><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<p>
							<img src="'.$imagePath.'" style="hieght:150px;width:150px" /></p></div></div></div>';
					}
					else if($contDet->type == "Link")
					{
						$contmsg = $contDet->query ;
						echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
						
					}
					else if($contDet->type == "Form")
					{
						$htmlStr ="";
						$oparr =$contDet->formcont ;
						$formItems = json_decode($oparr);
						$htmlStr.=$contDet->query;
						$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
						$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
						$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
						foreach($formItems as $formItem)
						{
							if($formItem->type =='textarea')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label.'</label>
									<textarea id="'.$formItem->name.'" name="'.$formItem->name .'" class="'.$formItem->className .'"> </textarea>
									</div>';
							}
							else if($formItem->type =='checkbox-group')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>';
								foreach($formItem->values as $val)
								{
									$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
								}
								'</div>';
							}
							else if($formItem->type =='radio-group')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>';
								foreach($formItem->values as $val)
								{
									$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
								}
								'</div>';
							}
							else if($formItem->type =='select')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
										foreach($formItem->values as $slectOptions)
										{
											$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
										}
									$htmlStr.='</select></div>';
						
							}

							else if($formItem->type =='text')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">' .$formItem->label .'</label>
									<input type="text" id="'.$formItem->name.'" name="' .$formItem->name .'" class="'.$formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='number')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<input type="number" id="'.$formItem->name.'" name="'. $formItem->name .'" class="'.$formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='date')
							{
								$htmlStr.= '<div class="form-group">
									<label for="type">'. $formItem->label .'</label>
									<input type="date" id="'.$formItem->name.'" name="'.$formItem->name .'" class="'. $formItem->className .'" >
								</div>';
							}
							else if($formItem->type =='file')
							{ 
								$htmlStr.= '<div class="form-group">
									<label for="type">'.$formItem->label .'</label>
									<input type="file" id="'.$formItem->name.'" name="'.$formItem->name .'[]" class="'.$formItem->className .'" multiple>
								</div>';
							}
							else if($formItem->type =='button')
							{ 
								$formId = 'form_'.$get_cont->_id;
								$htmlStr.= '<div class="form-group">
									<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
									</div>';

							}
						} 
						echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
					}
				}
			}
		}
		else if($lastMsgType == "Form")
		{
			$response = ContentConfigure::where('persona_id', $_REQUEST['persona_id'])->where('content_id', $detAr[3])->get();
			foreach($response as $resp)
			{
				$response_det = json_decode($resp->response_content);
				$response_id = $response_det->response_cont_id ; 
				$get_cont = Content::find($response_id);
				$contDet = json_decode($get_cont->content) ;
				if($contDet->type == "Text")
				{
					$contmsg = $contDet->query ;
					if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !="")
					{
						$chat_u = New UserChat();
						$chat_u->user_email = $_REQUEST['user_id'];
						$chat_u->persona_msg = $contmsg; 
						$chat_u->user_msg = $_REQUEST['user_msg'];
						$chat_u->save();
					}
					echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
				}
				else if($contDet->type == "Link")
				{
					$contmsg = $contDet->query ;
					echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
					
				}
				else if($contDet->type == "Radio")
				{
					$contmsg = $contDet->query ;
					
					$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div><p>';
					foreach($contDet->answer as $options)
					{
						$retStr1 .= '<input type="radio" name="'.$get_cont->_id .'" onclick="chatTry()" value="'.$options.'">'.$options.'&nbsp;&nbsp;&nbsp;';
					}
					$retStr .=$retStr1;
					$retStr .= '</p></div></div></div>';
					echo $retStr;
				}
				else if($contDet->type == "File")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query;
					$imagePath = asset($contmsg);
					echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="class="media-left""><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<p>
						<img src="'.$imagePath.'" style="hieght:150px;width:150px" /></p></div></div></div>';
				}
				else if($contDet->type == "Form")
				{
					$htmlStr ="";
					$oparr =$contDet->formcont ;
					$formItems = json_decode($oparr);
					$htmlStr.=$contDet->query;
					$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
					$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
					$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
					foreach($formItems as $formItem)
					{
						if($formItem->type =='textarea')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label.'</label>
								<textarea name="'.$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'"> </textarea>
								</div>';
						}
						else if($formItem->type =='select')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
							foreach($formItem->values as $slectOptions)
							{
								$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
							}
							$htmlStr.='</select></div>';
						}
						else if($formItem->type =='checkbox-group')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>';
							foreach($formItem->values as $val)
							{
								$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
							}
							'</div>';
						}
						else if($formItem->type =='radio-group')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>';
							foreach($formItem->values as $val)
							{
								$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
							}
							'</div>';
						}
						else if($formItem->type =='text')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">' .$formItem->label .'</label>
								<input type="text" id="'.$formItem->name.'" name="' .$formItem->name .'" class="'.$formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='number')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<input type="number" id="'.$formItem->name.'" name="'. $formItem->name .'" class="'.$formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='date')
						{
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>
								<input type="date" id="'.$formItem->name.'" name="'.$formItem->name .'" class="'. $formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='file')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<input type="file" id="'.$formItem->name.'" name="'.$formItem->name .'[]" class="'.$formItem->className .'" multiple>
							</div>';
						}
						else if($formItem->type =='button')
						{ 
							$formId = 'form_'.$get_cont->_id;
							$htmlStr.= '<div class="form-group">
								<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
								</div>';
						}
					} 
					echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
				}
			}
		}
		else 
		{
			$response = ContentConfigure::where('persona_id', $_REQUEST['persona_id'])->where('content_id', $detAr[3])->get();
			foreach($response as $resp)
			{
				$response_det = json_decode($resp->response_content);
				$response_id = $response_det->response_cont_id ; 
				$get_cont = Content::find($response_id);
				$contDet = json_decode($get_cont->content);
				
				if($contDet->type == "Text")
				{
					$contmsg = $contDet->query ;
					echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
				}
				else if($contDet->type == "Link")
				{
					$contmsg = $contDet->query ;
					echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div></div></div></div>';
					
				}
				else if($contDet->type == "Radio")
				{
					$contmsg = $contDet->query ;
					$retStr = '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$contmsg.'</div><p>';
					foreach($contDet->answer as $options)
					{
						$retStr .= '<input type="radio" name="'.$get_cont->_id .'" onclick="chatBackupTry()" value="'.$options.'">'.$options.'&nbsp;&nbsp;&nbsp;';
					}
					$retStr .= '</p></div></div></div>';
					echo $retStr;
				}
				else if($contDet->type == "File")
				{
					$answer = '';
					if(isset($contDet->answer))
					{
						$answer = $contDet->answer;
					}
					$contmsg = $contDet->query;
					$imagePath = asset($contmsg);
					echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="class="media-left""><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small>'.$answer.'<p>
						<img src="'.$imagePath.'" style="hieght:150px;width:150px" /></p></div></div></div>';
				}
				else if($contDet->type == "Form")
				{
					$htmlStr ="";
					$oparr =$contDet->formcont ;
					$formItems = json_decode($oparr);
					$htmlStr.=$contDet->query;
					$htmlStr.='<form action="'.url('backoffice/persona/add_form_data').'" enctype="multipart/form-data" method="post" id="form_'.$get_cont->_id.'">';
					$htmlStr .= '<input type="hidden" name="chatId" class="chat-id" value="'.$persona_id.'">';
					$htmlStr .= '<input type="hidden" name="formId" value="form_'.$get_cont->_id.'">';
					foreach($formItems as $formItem)
					{
						if($formItem->type =='textarea')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label.'</label>
								<textarea name="'.$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'"> </textarea>
								</div>';
						}
						/*if($formItem->type =='autocomplete')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label.'</label>';
								foreach($formItem->values as $val)
								{
									$htmlStr.= '<input type="checkbox" name="'.$formItem->label.'[]" class="" value="'.$val->value.'">'.$val->value;
								}
								'</div>';
						}*/
						else if($formItem->type =='checkbox-group')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>';
							foreach($formItem->values as $val)
							{
								$htmlStr.= '<input type="checkbox" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
							}
							'</div>';
						}
						else if($formItem->type =='radio-group')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>';
							foreach($formItem->values as $val)
							{
								$htmlStr.= '<input type="radio" id="'.$formItem->name.'" name="'.$formItem->name.'[]" class="" value="'.$val->value.'">'.$val->value;
							}
							'</div>';
						}
						else if($formItem->type =='select')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<select class="'.$formItem->className .'" id="response-type" name="'.$formItem->name .'" required>';
							foreach($formItem->values as $slectOptions)
							{
								$htmlStr.= '<option value="'.$slectOptions->value .'">'.$slectOptions->label .'</option>';
							}
							$htmlStr.='</select></div>';
						}
						else if($formItem->type =='text')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">' .$formItem->label .'</label>
								<input type="text" name="' .$formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='number')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<input type="number" name="'. $formItem->name .'" id="'.$formItem->name.'" class="'.$formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='date')
						{
							$htmlStr.= '<div class="form-group">
								<label for="type">'. $formItem->label .'</label>
								<input type="date" name="'.$formItem->name .'" id="'.$formItem->name.'" class="'. $formItem->className .'" >
							</div>';
						}
						else if($formItem->type =='file')
						{ 
							$htmlStr.= '<div class="form-group">
								<label for="type">'.$formItem->label .'</label>
								<input type="file" name="'.$formItem->name .'[]" id="'.$formItem->name.'" class="'.$formItem->className .'" multiple>
							</div>';
						}
						else if($formItem->type =='button')
						{ 
							$formId = 'form_'.$get_cont->_id;
							$htmlStr.= '<div class="form-group">
								<input type="button" name="'. $formItem->name .'" value="'. $formItem->label .'" class="'. $formItem->className .'" onclick="submitForm('.$formId.')">
								</div>';
						}
					} 
					echo '<div id="bot_response_'.$contDet->type .'_'.$get_cont->_id .'" class="chat_content"><div class="media"><div class="media-left"><img class="fox_icon" src="'.asset('uploads/user-images/'.$personaDetail->persona_image).'"></div><div class="media-body"><small>'.date('H:i').'</small><div class="chat_txt bg_light">'.$htmlStr.'</div></div></div></div>';
				}
			}
		}
	}
	
	public function chat_backup(Request $request)
	{
		$id=$request->input('chat_id');
		
		if($id=="")
		{
			$u=UserChat::create([

            'user_email' => $request->input('user_email'),

            'user_msg' => $request->input('chat'),
			
			]);
			
			$id=$u->id;
		}
		else
		{
			$uc= UserChat::find($id);
			$uc->user_email= $request->input('user_email');
			$uc->user_msg = $request->input('chat');
			$uc->save();
		}
		echo $id;
	} 
	
	
	
	public function upload_file(Request $request){
		
		$file = $request->file('file-upload');
		//var_dump($file);die;
			$fileName= time()."-".$file->getClientOriginalName();
			
			
			$fileName = str_replace(' ', '_', $fileName);
		   
			//File Extension
			$fileExt = $file->getClientOriginalExtension();
			//File Real Path
			$realPath = $file->getRealPath();
			//Display File Size
			$fileSize = $file->getSize();
			//Display File Mime Type
			$filetype = $file->getMimeType();
			$destinationPath = 'uploads';
			$file->move($destinationPath,$fileName);
			$fileurl= asset($destinationPath."/".$fileName);
			
			//var_dump($fileExt);die;
			if($fileExt=="jpg" or $fileExt=="jpeg" or $fileExt=="png")
			{				
				echo '<img class="chat_txt bg_brown" src="'.asset($destinationPath."/".$fileName).'">';
			}
			else
			{
				echo '<a href="'.asset($destinationPath."/".$fileName).'" download="ChatFile"><i class="fa fa-paperclip fa-10x" aria-hidden="true"></i> '.$fileName.'</a>';
			}
	}
	
	function detect_intent_texts()
	{
		$intentsClient = new IntentsClient();
		$parent = $intentsClient->projectAgentName("testagent-d160b");
		$intents = $intentsClient->listIntents($parent,array("intentView"=>1));
		$allIntents = array();
		$iterator = $intents->getPage()->getResponseObject()->getIntents()->getIterator();
		while($iterator->valid()){
			$intent = $iterator->current();
			$allIntents[] = array("id"=>$intent->getName(),"name"=>$intent->getDisplayName());
			$iterator->next();
		}
		return Response::json(array('success'=>true,"allIntents"=>$allIntents));
	}
	
	public function createIntentTest()
    {
        $transport = $this->createTransport();
        $client = $this->createClient(['transport' => $transport]);
        $this->assertTrue($transport->isExhausted());
        // Mock response
        $name = 'name3373707';
        $displayName = 'displayName1615086568';
        $priority = 1165461084;
        $isFallback = false;
        $mlDisabled = true;
        $action = 'action-1422950858';
        $resetContexts = true;
        $rootFollowupIntentName = 'rootFollowupIntentName402253784';
        $parentFollowupIntentName = 'parentFollowupIntentName-1131901680';
        $expectedResponse = new Intent();
        $expectedResponse->setName($name);
        $expectedResponse->setDisplayName($displayName);
        $expectedResponse->setPriority($priority);
        $expectedResponse->setIsFallback($isFallback);
        $expectedResponse->setMlDisabled($mlDisabled);
        $expectedResponse->setAction($action);
        $expectedResponse->setResetContexts($resetContexts);
        $expectedResponse->setRootFollowupIntentName($rootFollowupIntentName);
        $expectedResponse->setParentFollowupIntentName($parentFollowupIntentName);
        $transport->addResponse($expectedResponse);
        // Mock request
        $formattedParent = $client->projectAgentName('testagent-d160b');
        $intent = new Intent();
        $response = $client->createIntent($formattedParent, $intent);
        $this->assertEquals($expectedResponse, $response);
        $actualRequests = $transport->popReceivedCalls();
        $this->assertSame(1, count($actualRequests));
        $actualFuncCall = $actualRequests[0]->getFuncCall();
        $actualRequestObject = $actualRequests[0]->getRequestObject();
        $this->assertSame('/google.cloud.dialogflow.v2.Intents/CreateIntent', $actualFuncCall);
        $actualValue = $actualRequestObject->getParent();
        $this->assertProtobufEquals($formattedParent, $actualValue);
        $actualValue = $actualRequestObject->getIntent();
        $this->assertProtobufEquals($intent, $actualValue);
        $this->assertTrue($transport->isExhausted());
    }
	
	private function createTransport($deserialize = null)
    {
        return new MockTransport($deserialize);
    }
    /**
     * @return IntentsClient
     */
    private function createClient(array $options = [])
    {
        $options += [
            'credentials' => $this->getMockBuilder(CredentialsWrapper::class)->disableOriginalConstructor()->getMock(),
        ];
        return new IntentsClient($options);
    }
	
	public function register_chat_user()
	{
		
		$getUserChat = UserChatBackup::where('persona_id',$_REQUEST['personaId'])->where('uniqueId',SESSION::get('uniqueId'))->where('user_ip_address',$_REQUEST['ip'])->first();
		$getUserChat->email = $_REQUEST['user_msg'];
		$getUserChat->save();
		echo User::create([

            'name' => '',

            'email' => $_REQUEST['user_msg'],

        ]);
	} 
	
	
	
	

/*===========Common delete function for admin==========*/

	public function commonDelete(Request $request)
	{
		$tableName = $request->input('table_name');
		if($tableName == 'Personas' )
		{
			$deleteData = Personas::find($request->input('id'));
			$deleteData->deleteStatus = $request->input('deleteStatus');
			$deleteData->save();
        	return redirect('backoffice/persona');
		}
		if($tableName == 'Industries' )
		{
			$deleteData = Industries::find($request->input('id'));
			$deleteData->status = $request->input('deleteStatus');
			$deleteData->save();
        	return redirect('backoffice/persona/industries');
		}
		if($tableName == 'Brands' )
		{
			$deleteData = Brands::find($request->input('id'));
			$deleteData->status = $request->input('deleteStatus');
			$deleteData->save();
        	return redirect('backoffice/persona/brands');
		}
		if($tableName == 'Compaigns' )
		{
			$deleteData = Compaigns::find($request->input('id'));
			$deleteData->status = $request->input('deleteStatus');
			$deleteData->save();
        	return redirect('backoffice/persona/brand_compaign/'.$deleteData->brandId);
		}
	}

/*==========Edit persona content===========*/

	public function editPersonaContent()
	{
		$personaChannel = $_GET['personaChannel'];
		$content = Content::find($_GET['contentId']);
		if($personaChannel == 'Facebook')
		{
			$result['content'] = $content->content;
		}
		else if($personaChannel == 'Twitter')
		{
			$result['content'] = $content->content;
		}
		else
		{
			$result['content'] = json_decode($content->content);
		}
		$result['content_id'] = $content->id;
		$result['content_cust_id'] = $content->content_cust_id;
		$result['score'] = $content->score;
		$result['channel'] = $personaChannel;
		echo json_encode($result);exit;
	}

/*============Add duplicate persona===========*/

	public function addDuplicatePersona()
	{
		$personaDetail = Personas::where('_id', $_GET['personaId'])->first();
		$personas = new Personas();
		$getPersonaTitle = Personas::where('persona_title', $personaDetail->persona_title)->get();
		$personas->persona_title = $personaDetail->persona_title.' '.count($getPersonaTitle);
        $personas->industry = $personaDetail->industry;
        $personas->brand = $personaDetail->brand;
        $personas->channel = $personaDetail->channel;
		$personas->url = $personaDetail->url;
		$personas->persona_url = $personaDetail->persona_url;
		$personas->tags = $personaDetail->tags;	
		$personas->score = $personaDetail->score;	
		$personas->compaign = $personaDetail->compaign;	
		$personas->require_login = $personaDetail->require_login;	
		$personas->save();
		if($personas->id != '')
		{
			$result['url'] = url('backoffice/persona');
		}
		else
		{
			$result['url'] = '';
		}
		echo json_encode($result);exit;
	}

/*===========Get persona score=======*/

	public function getPersonaScore()
	{
		$personaScore = Personas::where('_id',$_GET['personaId'])->first();
		$content = Content::where('persona_id',$_GET['personaId'])->get();
		if(count($content) > 0)
		{
			$score_sum = 0;
			$sumScore = Content::where('persona_id',$_GET['personaId'])->get();

			foreach($sumScore as $val)
			{
				$score_sum += $val->score;
			}

			$data['score_sum'] = $score_sum;
		}
		$data['score'] = $personaScore->score;
		echo json_encode($data);exit;
	}

	public function PersonaAssignChannel(Request $request)
	{
		$personaId = $request->input('persona_id');
		$personaDetail = Personas::where('_id',$personaId)->first();
		$url = $personaDetail->url;
		foreach($request->input('channels') as $val)
		{
			if($val == 'facebook')
			{
				$type = 'facebook_url';
				$chatUrl = $url.'/facebook';
			}
			else if($val == 'twitter')
			{
				$type = 'twitter_url';
				$chatUrl = $url.'/twitter';
			}
			else if($val == 'linkdin')
			{
				$type = 'linkdin_url';
				$chatUrl = $url.'/linkdin';
			}
			else if($val == 'youtube')
			{
				$type = 'youtube_url';
				$chatUrl = $url.'/youtube';
			}
			$checkUrl = Personas::where('_id',$personaId)->where($type,$chatUrl)->first();
			if($checkUrl == '')
			{
				$personaDetail->$type = $chatUrl;
				$personaDetail->save();
			}
		}
		return redirect('backoffice/persona');
	}

	public function getPersonaChannel()
	{
		$personaId = $_GET['perId'];
		$personaDetail = Personas::where('_id',$personaId)->first();
		$fbSel = '';
		$twSel = '';
		$youSel = '';
		$linSel = '';
		if($personaDetail->facebook_url)
		{
			$fbSel = "selected='selected'";
		}
		if($personaDetail->twitter_url)
		{
			$twSel = "selected='selected'";
		}
		if($personaDetail->youtube_url)
		{
			$youSel = "selected='selected'";
		}
		if($personaDetail->linkdin_url)
		{
			$linSel = "selected='selected'";
		}
		$html = '<option value="">Select multiple channels</option>
            		<option value="facebook"'.$fbSel.'>Facebook</option>
            		<option value="twitter"'.$twSel.'>Twitter</option>
            		<option value="linkdin"'.$youSel.'>Linkdin</option>
            		<option value="youtube"'.$linSel.'>Youtube</option>
            	';
        echo $html;
	}


	public function getFrontendUrl()
	{
		$personaId = $_GET['personaId'];
		$persona = Personas::where('_id',$personaId)->first();
		echo json_encode($persona);exit;
	}
} 

