<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BusinessLogicController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.persona.content');
    }
	
	public function add(Request $request)
    {
        return view('admin.persona.addcontent');
    }
}
