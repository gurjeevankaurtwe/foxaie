<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BusinessLogic extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'business_logics';
	protected $primaryKey = '_id';
    
    protected $fillable = [
        'persona_title', 'industry', 'channel', 'tags'
    ];
	
	public function personas()
    {
        return $this->belongsTo('App\Models\Personas');
    }
}
