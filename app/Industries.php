<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Industries extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'industries';
    
	protected $primaryKey = '_id';
    protected $fillable = [
        'industry', 'status'
    ];
	
	/*public function tasks()
    {
        return $this->hasMany('App\Models\BusinessLogic');
    }*/
}
