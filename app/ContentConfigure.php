<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
class ContentConfigure extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'content_configure';
	protected $primaryKey = '_id';
    protected $fillable = [
        'persona_id', 'content_id', 'response_content'
    ];
}
