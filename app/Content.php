<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Content extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'content';
    
	protected $primaryKey = '_id';
    protected $fillable = [
        'persona_id', 'content_cust_id', 'content' , 'require_login'
    ];
	
	/*public function tasks()
    {
        return $this->hasMany('App\Models\BusinessLogic');
    }*/
}
